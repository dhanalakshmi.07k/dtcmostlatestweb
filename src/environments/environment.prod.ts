const base_api = "51.15.254.32";

export const environment = {
  production: true,
  ws_url: 'http://'+base_api+':4500/',
  server_url: 'http://'+base_api+':4500/api/',
  login_url: 'http://'+base_api+':4500',
  iotzen_logo_url: 'assets/iot_zen_logo.png',
  is_menu_collapse: true,
  is_sub_menu_collapse: true
};
