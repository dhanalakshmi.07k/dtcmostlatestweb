// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
const base_api = "localhost";
export const environment = {
  production: false,
 /* ws_url: 'http://localhost:4500/',
  server_url: 'http://localhost:4500/api/',
  login_url: 'http://localhost:4500',*/
  iotzen_logo_url: 'assets/iot_zen_logo.png',
 // production: false,
  ws_url:"http://"+base_api+":4500/",
  server_url:"http://"+base_api+":4500/api/",
  login_url:"http://"+base_api+":4500",
  is_menu_collapse: true,
  is_sub_menu_collapse: true
};
