import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";


import { environment } from '../environments/environment';
@Injectable()
export class InspectionBayService{

  constructor(private http: HttpClient,public configService:ConfigService) { }

  getActive() {
    console.log(environment)
    let url = this.configService.appConfig.appBaseUrl +"inspectionBay/active";
    return this.http.get(url);
  }
  getAll(skip,limit) {
    let url = this.configService.appConfig.appBaseUrl +"inspectionBay?time="+Date.now() + '&skip=' + skip + '&limit=' + limit;
    return this.http.get(url);
  }
  getAllMinifiedReportData(skip, limit) {
    let url = this.configService.appConfig.appBaseUrl + 'inspectionBay?time=' + Date.now() + '&skip=' + skip + '&limit=' + limit + '&min=true' ;
    return this.http.get(url);
  }
  getCount() {
    let url = this.configService.appConfig.appBaseUrl +"inspectionBay/count?date="+Date.now();
    return this.http.get(url);
  }

  stopInspection() {
    let url = this.configService.appConfig.appBaseUrl +"inspectionBay/stop?date="+Date.now();
    return this.http.get(url);
  }

  getSearchedReportPaginationCount(searchText) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'inspectionBay/count?date='+Date.now() + '&search=' + searchText);
  }

  getReportsOnSearch(searchText, skip, limit) {
    let url = this.configService.appConfig.appBaseUrl + 'inspectionBay/?search=' + searchText + '&skip=' + skip + '&limit=' + limit + '&min=true';
    return this.http.get(url);
  }
  getAvddsDetails(inspectedCarDetails) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'inspectionBay/fields/avdds/' + inspectedCarDetails._id);
  }
  getUvisDetails(inspectedCarDetails) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'inspectionBay/fields/uvis/' + inspectedCarDetails._id);
  }
  getEngineDiagnosticDetails(inspectedCarDetails) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'inspectionBay/fields/engineInspection/' + inspectedCarDetails._id);
  }
  getEntireReportDetails(inspectedCarDetails) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'inspectionBay/id/' + inspectedCarDetails._id);
  }
  getReportById(inspectionId) {
    window.open(this.configService.appConfig.appBaseUrl + 'inspectionBay/reports/' + inspectionId, '_blank');
  }
  getAnalyticsData(startDate, endDate, series, status, carRegNo) {
    if (!series) {
      series = 'day';
    }
    return this.http.get(this.configService.appConfig.appBaseUrl +
      'inspectionBay/analytics/counts/' + startDate + '/' + endDate + '?series=' + series + '&status=' + status);
  }
  downloadAnalyticsData(startDate, endDate, series, status, carRegNo) {
    if (!series) {
      series = 'day';
    }
    window.open(this.configService.appConfig.appBaseUrl + 'inspectionBay/analytics/download/' + startDate + '/' + endDate + '?series=' + series + '&status=' + status, '_blank');
  }
}
