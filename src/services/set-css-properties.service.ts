import { Injectable } from '@angular/core';
import {ConfigService} from './config.service';
declare var $: any;

@Injectable()
export class SetCssPropertiesService {

  constructor(private configService: ConfigService) { }

  setCssClasses() {
    if (this.configService.appConfig.isMenuCollapse) {
      $('#content').addClass('active').removeClass('login-header');
      $('#sidebar').addClass('active');
      $('#side-bar-grid').addClass('active').removeClass('login-header');
      $('#header-container').addClass('active').removeClass('login-header');
      $('#main-container').removeClass('login-header');
    } else {
      $('#content').removeClass('active').removeClass('login-header');
      $('#side-bar-grid').removeClass('active').removeClass('login-header');
      $('#sidebar').removeClass('active');
      $('#header-container').removeClass('active').removeClass('login-header');
      $('#main-container').removeClass('login-header');
    }
  }

}
