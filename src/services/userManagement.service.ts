import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ConfigService} from './config.service';

@Injectable()
export class UserManagementService {
  constructor(private http: HttpClient, public configService: ConfigService) {
  }

  getUsers() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'user');
  }

  getUserData(id) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'user/profile/' + id);
  }

  getRoles() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'user/rolesDefined');
  }

  checkIsUsernameExist(username: string) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'user/isUserNameExist/' + username);
  }

  saveUserDetails(userData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'user', userData);
  }

  updateUserDetails(userId, userData) {
    return this.http.put(this.configService.appConfig.appBaseUrl + 'user/profile/' + userId, userData);
  }

  deleteUser(userID) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'user/profile/' + userID);
  }

  isUserNameExist(username: string) {
    return new Promise((resolve, reject) => {
      let strLength: number;
      strLength = username.length;
      this.checkIsUsernameExist(username)
        .subscribe(res => {
          let isUsernameExist: boolean;
          isUsernameExist = res['present'];
          resolve (isUsernameExist);
        });
    });
  }
}
