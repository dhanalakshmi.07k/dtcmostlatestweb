import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class NearableService {
  constructor(private http: HttpClient,public configService:ConfigService) { }
  public defaultNearByDuration = 1;
  getNearby(assetType, registeredStatus, duration) {

    let url = this.configService.appConfig.appBaseUrl +"nearBy";
    if(duration){
      url = url + '?duration='+duration;
    }else{
      url = url + '?duration='+this.defaultNearByDuration;
    }
    if(assetType){
        url = url + '&type='+assetType;
    }
    if(registeredStatus){
        url = url + '&registered='+registeredStatus;
    }
    return this.http.get(url);
  }
  getNearbyAssetsByRfId(duration) {

    let url = this.configService.appConfig.appBaseUrl +"nearBy/assetsByRfId";
    if(duration){
      url=url+"?duration="+duration;
    }else{
      url=url+"?duration="+this.defaultNearByDuration;
    }
    return this.http.get(url);
  }
}
