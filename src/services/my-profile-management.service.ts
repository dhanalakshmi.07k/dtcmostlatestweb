/**
 * Created by chandru on 15/2/19.
 */
import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";
import { Observable, Subject, BehaviorSubject } from 'rxjs/Rx';

@Injectable()
export class MyProfileManagementService {

  // messages: Subject<any>;
  public isPasswordChanged: Subject<boolean> = new BehaviorSubject<boolean>(false);
  public userProfileDetails: Subject<any> = new BehaviorSubject<any>(null);

  // Our constructor calls our wsService connect method
  constructor(private http: HttpClient, public configService: ConfigService) {}

  passwordChanged(isPasswordChanged: boolean) {
    this.isPasswordChanged.next(isPasswordChanged);
  }

  getUserProfile() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'user/self/profile');
  }

  setNewPassword(newPassword) {
    return this.http.put(this.configService.appConfig.appBaseUrl + 'user/self/profile/password', newPassword);
  }

  updateUserDetails(userDetails) {
    return this.http.put(this.configService.appConfig.appBaseUrl + 'user/self/profile', userDetails);
  }

  sendUserProfileDetails(userProfileDetails?) {
    if (userProfileDetails) {
      this.userProfileDetails.next(userProfileDetails);
    } else {
      this.getUserProfile()
        .subscribe(details => {
          this.userProfileDetails.next(details);
        });
    }
  }

}
