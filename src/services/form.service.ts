import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FormService {

  constructor(private http: HttpClient) { }
  formatAssetConfig(assetConfig) {
    let arr = []
    for (let key in assetConfig) {
      if (assetConfig.hasOwnProperty(key)) {
        arr.push(assetConfig[key])
      }
    }
    return arr;
  }

  formatEditAssetConfig(assetConfig,assetDetails) {
    let arr = []
    for (let key in assetConfig) {
      if (assetConfig.hasOwnProperty(key)) {
        assetConfig[key].fieldValue=assetDetails[key];
        arr.push(assetConfig[key])
      }
    }
    return arr;
  }


  formatEditAssetDetails(assetDetails){
    var obj={}
    for (let key in assetDetails) {
      if (assetDetails.hasOwnProperty(key)) {
        obj[assetDetails[key].field]=assetDetails[key].fieldValue
      }
    }
    return obj
  }

  formatAssetSaveDetails(assetDetails){
   let  assetModifiedObj={};
    for (let key in assetDetails) {
      if (assetDetails.hasOwnProperty(key) && typeof  assetDetails[key]==='string') {
        assetModifiedObj[key]=assetDetails[key]
      }
    }
    return assetModifiedObj;
  }
}
