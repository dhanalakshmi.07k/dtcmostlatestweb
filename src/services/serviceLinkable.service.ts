/**
 * Created by chandru on 16/10/18.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ConfigService} from "./config.service";

@Injectable()
export class ServiceLinkableService {
  constructor(private http: HttpClient, public configService: ConfigService) {
  }

  getAllServiceAssets() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'serviceAssets/' );
  }

  getServiceAssetDetailsById(serviceAssetId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'serviceAssets/id/' + serviceAssetId);
  }

  saveServiceAssetDetails(serviceAssetData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'serviceAssets/', serviceAssetData);
  }
  updateServiceById(serviceId, serviceDetails) {
    return this.http.put(this.configService.appConfig.appBaseUrl + 'serviceAssets/' + serviceId, serviceDetails);
  }
  getServiceAssetConfigrationTypes() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'serviceAssetsConfig/types' );
  }

  getServiceAssetConfig(serviceAssetType) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'serviceAssetsConfig?type=' + serviceAssetType);
  }

  deregisterServiceAsset(serviceAssetId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'serviceAssets/id/' + serviceAssetId);
  }
  linkAsset(linkAssetDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'serviceAssets/link/', linkAssetDetails);
  }
  delinkAsset(delinkAssetDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'serviceAssets/deLink/', delinkAssetDetails);
  }
  manageServices(action) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'serviceAssets/manage', action);
  }
  getServicesHealth(start, end, id?: number) {
    if (id) {
      return this.http.get(this.configService.appConfig.appBaseUrl + 'serviceAssets/status/' + start + '/' + end + '/' + id );
    } else {
      return this.http.get(this.configService.appConfig.appBaseUrl + 'serviceAssets/status/' + start + '/' + end );
    }
  }
}
