import { Injectable } from '@angular/core';
import { WebsocketService } from './webSocket.service';
import { Observable, Subject } from 'rxjs/Rx';
import * as io from 'socket.io-client';
import { environment } from '../environments/environment';

@Injectable()
export class SocketService {
  private url: any = environment.ws_url;
  private socket;

  // Our constructor calls our wsService connect method
  constructor() {
    this.socket = io(this.url);
  }

  getActiveInspectionDataObservable() {
    /*let observable = new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on('activeInspectionData', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;*/
    return Observable.create((observer) => {
      this.socket.on('activeInspectionData', (message) => {
        observer.next(message);
      });
    });
  }

  getHealthManagementResponse() {
    return Observable.create((observer) => {
      this.socket.on('healthManagementResponse', (response) => {
        observer.next(response);
      });
    });
  }

}
