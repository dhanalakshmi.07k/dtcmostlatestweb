/**
 * Created by chandru on 29/6/18.
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ConfigService} from "./config.service";

@Injectable()
export class AssetService {
  constructor(private http: HttpClient,public configService:ConfigService) {
  }

  getAssetCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'asset/countByType');
    }
  getAssetPaginationCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl +'asset/count');
  }

  getSearchedAssetPaginationCount(searchText, selectedAssetTypes) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assets/search/count/' + searchText + '?type=' + selectedAssetTypes);
  }

  getAssetConfig(assetName) {
    return this.http.get(this.configService.appConfig.appBaseUrl +'assetConfig?type='+assetName);
  }

  getAllAssets() {
    return this.http.get(this.configService.appConfig.appBaseUrl +'asset');
  }

  getAllAssetTypes() {
    return this.http.get(this.configService.appConfig.appBaseUrl +'assetConfig/types');
  }

  saveAssetDetails(assetData) {
    return this.http.post(this.configService.appConfig.appBaseUrl +'asset',assetData);
  }
  getAssetDetailsByMongodbId(assetId) {
    return this.http.get(this.configService.appConfig.appBaseUrl +'asset/' + assetId);
  }

  updateAssetsById(assetId, assetDetails) {
    return this.http.put(this.configService.appConfig.appBaseUrl +'asset/' + assetId,assetDetails);
  }
  saveAssets(assetDetails) {
    return this.http.put(this.configService.appConfig.appBaseUrl +'asset/',assetDetails);
  }

  searchForAssetFromassetType(searchForAsset, assetTypeForSearch, skip, limit) {
    let queryUrl = this.configService.appConfig.appBaseUrl + 'assets/search/';
    if (searchForAsset && assetTypeForSearch) {
      queryUrl = queryUrl + searchForAsset + '?type=' + assetTypeForSearch + '&skip=' + skip + '&limit=' + limit;
    } else if (searchForAsset) {
      queryUrl = queryUrl + searchForAsset + '?type=' + '&skip=' + skip + '&limit=' + limit;
    }
    return this.http.get(queryUrl);
  }

  delinkAsset(delinkAssetDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl +'assets/deLink', delinkAssetDetails);
  }

  linkAsset(linkAssetDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl +'assets/link', linkAssetDetails);
  }
  getAssetsBasedOnRange(skip, limit) {
    return this.http.get(this.configService.appConfig.appBaseUrl +'asset?skip='+skip+'&limit='+limit);
  }
  saveMultipleAssetDetails(assetArrayData) {
    return this.http.post(this.configService.appConfig.appBaseUrl +'asset/multiple',assetArrayData);
  }

  getAllByType(assetTypes, skip, limit) {
    let url = this.configService.appConfig.appBaseUrl +'asset?skip='+skip+'&limit='+limit;
    if(assetTypes){
      url = url + '&type=' + assetTypes;
    }
    return this.http.get(url);
  }

  getAssetOnSearch(searchText, selectedAssetTypes, skip, limit) {
    let url = this.configService.appConfig.appBaseUrl + 'assets/search/' + searchText + '?type=' + selectedAssetTypes + '&skip=' + skip + '&limit=' + limit;
    return this.http.get(url);
  }

  getCountByTypes(assetTypes) {
    return this.http.get(this.configService.appConfig.appBaseUrl +'asset/count?type=' + assetTypes);
  }

  deregisterAsset(assetId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'asset/' + assetId);
  }

  getAssetGroups() {
    return this.http.get(this.configService.appConfig.appBaseUrl +'assetGroup' );
  }

  getLinkedAssets(assetId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'asset/linkedAssets/' + assetId );
  }

  getServerAnalytics(startTime, endTime, assetId, field?) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'asset/historical/stats/' + startTime + '/' + endTime + '/' + assetId + '?fields=' + field  );
  }
}


