import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class LoginServiceService {

  constructor(private http: HttpClient, public configService: ConfigService) { }

  verifyUserDetails(userDetails) {
    return this.http.post(this.configService.appConfig.loginBaseURL + '/authentication/login', userDetails);
  }

  roleMatch(allowedRoles) {
    let isMatch: boolean;
    isMatch = false;
    let userRoles: string[];
    userRoles = JSON.parse(sessionStorage.getItem('userRoles'));
    allowedRoles.forEach(element => {
      if (userRoles.indexOf(element) > -1) {
         isMatch = true;
         return false;
      }
    });
    return isMatch;
  }

}
