/**
 * Created by chandru on 29/6/18.
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ConfigService} from "./config.service";

@Injectable()
export class AssetGroupService {
  constructor(private http: HttpClient,public configService:ConfigService) {
  }

  get() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assetGroup/');
  }
  update(obj){
    return this.http.put(this.configService.appConfig.appBaseUrl +'assetGroup/',obj);
  }
  add(obj,id){
    return this.http.post(this.configService.appConfig.appBaseUrl +'assetGroup/id/'+id,obj);
  }
  link(assetIdList,groupId){
    let obj = {
      assets:assetIdList,groups:[groupId]
    };
    return this.http.post(this.configService.appConfig.appBaseUrl + 'assetGroup/linkToAsset',obj);
  }
}


