/**
 * Created by chandru on 29/8/18.
 */
import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs/Rx';

@Injectable()
export class SearchService {

 // messages: Subject<any>;
  public searchText: Subject<any> = new BehaviorSubject<any>('');
  public searchPlaceholderValue: Subject<any> = new BehaviorSubject<any>('');
  public resetSearchTextValue: Subject<any> = new BehaviorSubject<any>(0);
  public searchedInfoValue: Subject<any> = new BehaviorSubject<any>('');
  public isSearchBarNeeded: Subject<any> = new BehaviorSubject<any>(false);

  // Our constructor calls our wsService connect method
  constructor() {}

  showSearchBar(isSearchBarNeeded: boolean) {
    this.isSearchBarNeeded.next(isSearchBarNeeded);
  }
  sendSearchText(text) {
    this.searchText.next(text);
  }
  sendSearchPlaceholderValue(placeholder) {
    this.searchPlaceholderValue.next(placeholder);
  }
  sendSearchedInfoValue(value) {
    this.searchedInfoValue.next(value);
  }
  resetSearchTextInSearchBox() {
    this.resetSearchTextValue.next(Math.random());
  }

}
