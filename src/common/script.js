/**
 * Created by chandru on 29/6/18.
 */
$(document).ready(function () {
  let parentEle = $('body');
  let showPass = 0;

  fun = {
    popup: function(btn, popup) {
      parentEle.on('click', btn, function(e) {
        e.preventDefault();
        e.stopPropagation();
        // $('.background').fadeIn(250, function() {
        $('.background').fadeIn(250);
        $('.popup').fadeOut(0);
        $(popup).fadeIn(250);
        // });
        $('body,html').scrollTop(0);
      });
      parentEle.on('click', '.popup .close', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('.popup').fadeOut(250, function() {
          $('.background').fadeOut(250);
        });
      });
    },
    sliderToggle: function () {
      parentEle.on('click', '#sidebarCollapse', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#sidebar').toggleClass('active');
        $('#content').toggleClass('active');
        $('#side-bar-grid').toggleClass('active');
        $('#header-container').toggleClass('active');
      });
    },
    togglePlusBtn: function () {
      parentEle.on('click', '#addAsset', function(e) {
        e.preventDefault();
        e.stopPropagation();
        backgroundToggle();
      });
      /*$('#addAsset').click(function() {
       $('.assetList').toggle('5000');
       $('#addAssetFabIcon').toggleClass("fa-plus fa-times");
       });*/
    },
    closeAddAssetBackground: function () {
      parentEle.on('click', '.addAssetBackground', function(e) {
        e.preventDefault();
        e.stopPropagation();
        backgroundToggle();
      });
      parentEle.on('click', '#assetIcon', function(e) {
        backgroundToggle();
      });
    },
    closeAssetListPopup: function () {
      parentEle.on('click', '#assetListBackBtn', function(e) {
        e.stopPropagation();
        $('#assetListingPopup').hide(300);
      });
    },
    closeServiceListPopup: function () {
      parentEle.on('click', '#serviceListBackBtn', function(e) {
        e.stopPropagation();
        $('#serviceListingPopup').hide(300);
      });
    },
    tooltip: function () {
      $('[data-toggle="tooltip"]').tooltip();
    },
    dropDown: function () {
      parentEle.on('click', 'ul#dropdown-item li a', function(e) {
        e.preventDefault();
        var concept = $(this).html();
        $('.search-panel span#search_concept').html(concept);
      });
    },
    formEditable: function() {
      parentEle.on('click', '#edit-asset-form, #edit-service-form', function(e) {
        e.stopPropagation();
        $(".edit-form-input").attr({'readonly': false, 'onfocus': false });
        $('.selectDropDownOption').prop('disabled',false);
      });
    },
    assetListOpen: function () {
      parentEle.on('click', '#asset-list-plus', function(e) {
        e.stopPropagation();
        $('#assetListingPopup').show(300);
      });
    },
    serviceListOpen: function () {
      parentEle.on('click', '#service-list-plus', function(e) {
        e.stopPropagation();
        $('#serviceListingPopup').show(300);
      });
    },
    showPassword: function () {
      parentEle.on('click', '.btn-show-pass', function(e) {
        e.stopPropagation();
        if(showPass == 0) {
          $(this).next('input').attr('type','text');
          $(this).find('i').removeClass('zmdi-eye');
          $(this).find('i').addClass('zmdi-eye-off');
          showPass = 1;
        }
        else {
          $(this).next('input').attr('type','password');
          $(this).find('i').addClass('zmdi-eye');
          $(this).find('i').removeClass('zmdi-eye-off');
          showPass = 0;
        }
      });
    },
    animateRefreshBtn: function () {
      parentEle.on('click', '#refreshButton', function(e) {
        e.stopPropagation();
        $(this).addClass('arrow-animation');
        setTimeout(function() {
          $('#refreshButton').removeClass('arrow-animation');
        }, 1000);
      });
    },
    userDetailsGridPopup: function () {
      parentEle.on('click', '#userDetailsGrid', function(e) {
        e.stopPropagation();
      });
      parentEle.on('click', '.alphabet-circle', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#userDetailsGrid').toggle();
      });
      parentEle.on('click', '#accountSettingBtn', function() {
        $('#userDetailsGrid').hide();
      });
      parentEle.on('click', function() {
        $('#userDetailsGrid').hide();
      });
    },
    subMenuToggle: function () {
      parentEle.on('click', 'li.main-menu > a', function(e) {
        e.stopPropagation();
        var submenu = $(this).next();
        var arrowIcon =  $(this).find('.arrow-icon');
        if ( $(submenu).is(':hidden') ) {
          $(arrowIcon).removeClass('fa-angle-down');
          $(arrowIcon).addClass('fa-angle-up');
          $(submenu).slideDown(200);
        } else {
          $(arrowIcon).removeClass('fa-angle-up');
          $(arrowIcon).addClass('fa-angle-down');
          $(submenu).slideUp(200);
        }
      });
    }
  };

  function backgroundToggle() {
    $('.assetList').toggle('5000');
    $('#addAssetFabIcon').toggleClass("fa-plus fa-times");
    $('.addAssetBackground').fadeToggle(250);
  }

  fun.popup('.showdDelinkAlertPopupBtn', '.delinkAlertPopup');
  fun.popup('.showdDelinkServicePopupBtn', '.delinkServicePopup');
  fun.popup('.showLinkAlertPopupBtn', '.linkAlertPopup');
  fun.popup('.showServiceLinkPopupBtn', '.linkServicePopup');
  fun.popup('.showDeregisterAssetPopupBtn', '.deregisterAssetPopup');
  fun.popup('.showDeleteUserPopupBtn', '.deleteUserPopup');
  fun.popup('.showRegisterExceptionBeconPopupBtn', '.registerExceptionBeconPopup');
  fun.popup('.showDeregisterExceptionBeaconPopupBtn', '.deregisterExceptionBeaconPopup');
  fun.popup('.showAssetRegisterErrorMsgBtn', '.assetRegisterErrorMsgPopup');
  fun.popup('.showServerActionBtn', '.serverActionPopup');
  fun.popup('.showBulkUploadErrorMsgBtn', '.BulkUploadErrorMsgPopup');
  fun.sliderToggle();
  fun.togglePlusBtn();
  fun.closeAddAssetBackground();
  fun.tooltip();
  fun.closeAssetListPopup();
  fun.closeServiceListPopup();
  fun.dropDown();
  fun.formEditable();
  fun.assetListOpen();
  fun.serviceListOpen();
  fun.showPassword();
  fun.animateRefreshBtn();
  fun.userDetailsGridPopup();
  fun.subMenuToggle();
});
