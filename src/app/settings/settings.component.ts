import { Component, OnInit } from '@angular/core';
import { Subject, Actions} from '../../auth/rules';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  public navLinks: any = [];
  SUBJECT = Subject;
  ACTIONS = Actions;
  constructor() {
    this.navLinks = [];
  }

  ngOnInit() {}

}
