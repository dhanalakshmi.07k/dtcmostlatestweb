import { Component, OnInit, OnDestroy } from '@angular/core';
import {ExceptionService} from '../../services/exception.service';
import {SearchService} from '../../services/search.service';
import { Subscription } from 'rxjs';
import { Subject, Actions} from '../../auth/rules';
declare var $: any;
import * as _ from 'lodash';

@Component({
  selector: 'app-asset-exception-info',
  templateUrl: './asset-exception-info.component.html',
  styleUrls: ['./asset-exception-info.component.scss']
})
export class AssetExceptionInfoComponent implements OnInit, OnDestroy {
  public deregisterBeaconExceptionArray: any;
  public exceptionAssetsList: any;
  public searchForBeacon: string;
  public  selectedAll: any;
  public isSelectAll: boolean;
  public isShowAsset: boolean;
  private searchPlaceholder: string;
  public selectedExceptionAssetsCount: number;
  public collectionOfExceptionAssetIds: any;
  SUBJECT = Subject;
  ACTIONS = Actions;
  private searchTextSub: Subscription;
  constructor(private exceptionService: ExceptionService, private searchService: SearchService) {
    this.exceptionAssetsList = [];
    this.deregisterBeaconExceptionArray = [];
    this.isShowAsset = false;
    this.searchPlaceholder = 'Search Beacon';
  }

  ngOnInit() {
    this.doSearchTextResetAndOtherOperations();
    this.getExceptionBecons();
    this.searchTextSub = this.searchService.searchText.subscribe(event => {
      this.searchForBeacon = event;
    });
  }
  ngOnDestroy() {
    this.searchTextSub.unsubscribe();
  }
  private doSearchTextResetAndOtherOperations() {
    this.searchService.showSearchBar(true);
    this.searchService.resetSearchTextInSearchBox();
    this.searchService.sendSearchText(''); // required when front-end filter is applied on search operation
    this.searchService.sendSearchPlaceholderValue(this.searchPlaceholder);
  }
  getExceptionBecons() {
    this.isShowAsset = true;
    this.exceptionService.getBeaconException()
      .subscribe(expectionAssets => {
        this.isShowAsset = false;
        this.exceptionAssetsList = expectionAssets;
        this.unSelectAllAssets();
      });
  }
  unSelectAllAssets() {
    $('#selectAllCheckbox').prop('checked', false);
    _.forEach(this.exceptionAssetsList, function(value) {
      value.selected = false;
    });
    this.isSelectAll = false;
  }
  selectAll() {
    this.isSelectAll = !this.isSelectAll;
    for (let i = 0; i < this.exceptionAssetsList.length; i++) {
      this.exceptionAssetsList[i].selected = this.selectedAll;
      if (this.isSelectAll) {
        if (this.deregisterBeaconExceptionArray.length > 0) {
          _.remove(this.deregisterBeaconExceptionArray, {'beaconId': this.exceptionAssetsList[i].beaconId});
        }
        this.deregisterBeaconExceptionArray.push(this.exceptionAssetsList[i]);
      } else {
        this.deregisterBeaconExceptionArray = [];
      }
    }
    this.selectedExceptionAssetsCount = this.deregisterBeaconExceptionArray.length;
  }
  deregisterExceptionAssetData(data) {
    if (this.deregisterBeaconExceptionArray.length > 0) {
      _.remove(this.deregisterBeaconExceptionArray, {'beaconId': data.assetData.beaconId});
    }
    if (data.embossedFlag) {
      this.deregisterBeaconExceptionArray.push(data.assetData);
    }
    this.selectedExceptionAssetsCount = this.deregisterBeaconExceptionArray.length;
  }
  confirmedDeregisteringExceptionBeacons(status) {
    if (status === 'OK') {
      let selectedExceptionIdsArray;
      selectedExceptionIdsArray = [];
      _.forEach(this.deregisterBeaconExceptionArray, function(asset) {
        selectedExceptionIdsArray.push(asset.beaconId);
      });
      this.collectionOfExceptionAssetIds = selectedExceptionIdsArray.toString();
      this.exceptionService.deregisterBeconException(this.collectionOfExceptionAssetIds)
        .subscribe(res => {
          this.getExceptionBecons();
          this.deregisterBeaconExceptionArray = [];
        });
    }
  }
}
