import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetExceptionInfoComponent } from './asset-exception-info.component';

describe('AssetExceptionInfoComponent', () => {
  let component: AssetExceptionInfoComponent;
  let fixture: ComponentFixture<AssetExceptionInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetExceptionInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetExceptionInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
