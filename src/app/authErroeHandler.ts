import {ErrorHandler, Injectable} from "@angular/core";
import {ForbiddenError} from "@casl/ability";
/**
 * Created by suhas on 14/2/19.
 */
@Injectable()
export class CustomErrorHandler extends ErrorHandler {
  handleError(error) {
    if (error instanceof ForbiddenError) {
      //this.toaster.error('Access Defined')
      console.log(('Access Defined'))
    } else {
      super.handleError(error)
    }
  }
}
