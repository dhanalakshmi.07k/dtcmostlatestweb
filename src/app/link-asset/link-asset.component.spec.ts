import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkAssetComponent } from './link-asset.component';

describe('LinkAssetComponent', () => {
  let component: LinkAssetComponent;
  let fixture: ComponentFixture<LinkAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
