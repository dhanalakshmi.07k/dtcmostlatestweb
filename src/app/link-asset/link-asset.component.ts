import { Component, OnInit } from '@angular/core';
import {NearableService} from "../../services/nearable.service";

@Component({
  selector: 'app-link-asset',
  templateUrl: './link-asset.component.html',
  styleUrls: ['./link-asset.component.scss']
})
export class LinkAssetComponent implements OnInit {
  public assetTypes = ["rfId", "beacon"];
  public assetStatusTypes = [true, false];
  public allAssets;
  public assetSelected="";
  public assetStatusSelected;
  p: number = 1;
  constructor(public nearableService: NearableService) {

  }

  ngOnInit() {
    this.nearableService.getNearby(null,null,null)
      .subscribe(nearbaleAssets => {
        this.allAssets = nearbaleAssets;
      });
  }


  getAssetsBasedOnTypeAndStatus(assetName,assetStatus){
    this.nearableService.getNearby(assetName,assetStatus,30)
      .subscribe(nearbaleAssets => {
        this.allAssets = nearbaleAssets;
      });
  }


}
