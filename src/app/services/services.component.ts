import { Component, OnInit, OnDestroy  } from '@angular/core';
import {ServiceLinkableService} from '../../services/serviceLinkable.service';
import {AssetConfigService} from '../../services/asset-config.service';
import {SearchService} from '../../services/search.service';
import {HealthCheckService} from '../../services/healthCheck.service';
import {SocketService} from "../../services/socket.service";
import { Subscription } from 'rxjs';

import { Subject,Actions} from '../../auth/rules';
declare var $ :any;
import * as moment from 'moment-timezone';
import * as _ from 'lodash';
@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit, OnDestroy {
  public allServiceAssets: any = [];
  public allServicesStatus: any = [];
  public individualSericeStatus: any = [];
  public serviceAssetConfigTypes: any = [];
  public allServersStatsArray: any = [];
  public serviceAssetSelectedType: string;
  public assetDetailsAfterLinkingUpdated: any;
  public assetConfigDetails: any;
  public showSlider: boolean;
  public serviceAssetId: any;
  public isShowAsset: boolean;
  public resetAddAssetFormValue: number;
  public showCircularLoaderValue: number;
  public resetFormData: number;
  public serviceAssetData: any;
  public assetsLinkedToServiceDetails: any = {};
  public assetRegisterErrorMessages: any = [];
  public  deregisterSelectedServiceAssetId: any;
  public isAssetDeregisteredSuccess: boolean;
  public isServerManageActionSuccess: boolean;
  public upadtedCartSpecificDetails: any;
  public updatedIndividualSericeStatus: any;
  public linkDelinkAction: boolean;
  public linkingAssetId: string;
  public delinkingAssetId: string;
  public serverAction: any;
  public serviceStatus: string;
  private webSocketSub: Subscription;
  ACTIONS=Actions;
  SUBJECT=Subject;
  constructor(private  serviceLinkableService: ServiceLinkableService,
              public assetConfigService: AssetConfigService,
              private searchService: SearchService,
              private socketService: SocketService,
              private healthCheckService: HealthCheckService) {
    this.serviceAssetSelectedType = '';
    this.isShowAsset = false;
    this.isAssetDeregisteredSuccess = false;
    this.linkDelinkAction = false;
    this.showSlider = false;
    this.linkingAssetId = '';
    this.delinkingAssetId = '';
    this.serviceStatus = '';
    this.serverAction = null;
    this.webSocketSub = this.socketService.getHealthManagementResponse()
      .subscribe(healthManagementResponse => {
        if (healthManagementResponse) {
          healthManagementResponse = JSON.parse(healthManagementResponse);
          if (healthManagementResponse.updatedDetails) {
            healthManagementResponse.updatedDetails['id'] = this.serviceAssetId;
            this.updatedIndividualSericeStatus = healthManagementResponse.updatedDetails;
            this.isServerManageActionSuccess = true;
            this.individualSericeStatus = [];
            this.individualSericeStatus.push(healthManagementResponse.updatedDetails);
            this.getServiceAssetDetails(this.serviceAssetId);
            this.serviceStatus = this.updatedIndividualSericeStatus.status;
            this.showMessagePopup('serviceActionUpdated');
          }
        }
    });
  }

  ngOnInit() {
    this.searchService.showSearchBar(false);
    this.getServicesHealth();
    this.getAllServersStats();
    // this.getServiceAssetConfigTypes();
    this.assetConfigService.getAssetsConfig()
      .then(assetConfig => {
        this.assetConfigDetails = assetConfig;
      });
  }

  ngOnDestroy() {
    this.webSocketSub.unsubscribe();
  }

  getServicesHealth() {
      this.isShowAsset = true;
      let durationInMinutes: any = 1;
      let start= moment().subtract(durationInMinutes, 'minutes').utc();
      let end = moment().utc();
      this.serviceLinkableService.getServicesHealth(start, end)
        .subscribe(allServicesStatus => {
          //this.isShowAsset = false;
          this.allServicesStatus = allServicesStatus;
          this.getServiceAssetConfigTypes();
        });
  }
  getAllServersStats() {
    let durationInMinutes: any = 5;
    let start= moment().subtract(durationInMinutes, 'minutes').utc();
    let end = moment().utc();
    this.healthCheckService.getAssetStatus(start, end)
      .subscribe(data => {
        this.allServersStatsArray = data;
      });
  }
  getIndividualServiceHealth(id) {
    let durationInMinutes: any = 1;
    let start= moment().subtract(durationInMinutes, 'minutes').utc();
    let end = moment().utc();
    this.serviceLinkableService.getServicesHealth(start, end, id)
      .subscribe(servicesStatus => {
        //this.isShowAsset = false;
        this.individualSericeStatus = [];
        this.individualSericeStatus = servicesStatus;
        this.getServiceAssetDetails(this.serviceAssetId);
       // this.allServiceAssets = this.syncSingleServiceStatus(servicesStatus, this.allServiceAssets, id);
      });
  }

  getAllServiceAssets() {
    this.isShowAsset = true;
    this.serviceLinkableService.getAllServiceAssets()
      .subscribe(serviceAssets => {
        this.isShowAsset = false;
        this.allServiceAssets = this.syncAllServiceStatus(this.allServicesStatus, serviceAssets);
      });
  }

  /*modifyAllServiceAssetsData(serviceAssets) {
   // let serviceDetails: any;
    let serviceStatusGroupedbyId = _.groupBy(this.allServicesStatus, '_id');
   // let serviceAssetsId = _.keys(serviceStatusGroupedbyId);
    serviceAssets = _.map(serviceAssets, function(data) {
      data.serverStatus = false;
      data.running = false;
      if (serviceStatusGroupedbyId[data._id] && serviceStatusGroupedbyId[data._id].length > 0) {
        data.serverStatus = true;
        if (serviceStatusGroupedbyId[data._id][0].running) {
          data.running = true;
        }
      }
      return data;
    });
    return serviceAssets;
  }*/

  modifyIndividualServiceAssetData(serviceAssetDetails) {
    if (this.individualSericeStatus.length > 0) {
      serviceAssetDetails['serverStatus'] = true;
      if (this.individualSericeStatus[0]['running']) {
        serviceAssetDetails['running'] = true;
      } else { serviceAssetDetails['running'] = false; }
    } else {
      serviceAssetDetails['running'] = false;
      serviceAssetDetails['serverStatus'] = false;
    }
    return serviceAssetDetails;
  }

  getServiceAssetConfigTypes() {
    this.assetConfigService.getServiceAssetConfig()
      .then(types => {
        this.serviceAssetConfigTypes = types;
        this.getAllServiceAssets();
      });
  }
  assetTypeToAdd(serviceAssetType) {
    this.serviceAssetSelectedType = serviceAssetType;
    this.resetAddAssetFormValue = Math.random();
  }
  editServiceAsset(serviceAssetDetails) {
    this.linkDelinkAction = false;
    this.isServerManageActionSuccess = false;
    this.showSlider = true;
    this.serviceAssetId = serviceAssetDetails._id;
    this.getIndividualServiceHealth(this.serviceAssetId);
    this.showCircularLoaderValue = Math.random();
  }
  getServiceAssetDetails(serviceAssetId) {
    this.serviceLinkableService.getServiceAssetDetailsById(serviceAssetId)
      .subscribe((serviceAssetDetails: any) => {
       // this.serviceAssetData = serviceAssetDetails;
        this.serviceAssetData = this.modifyIndividualServiceAssetData(serviceAssetDetails);
        this.assetsLinkedToServiceDetails = serviceAssetDetails.gatewayAsset;
        this.assetDetailsAfterLinkingUpdated = serviceAssetDetails;
        //this.getAssetsLinkedDetails(serviceAssetDetails);
      });
  }
  syncUpdateAssetCart(dataUpdated) {
    let durationInMinutes: any = 1;
    let start= moment().subtract(durationInMinutes, 'minutes').utc();
    let end = moment().utc();
    this.serviceLinkableService.getServicesHealth(start, end, dataUpdated._id)
      .subscribe(servicesStatus => {
        this.individualSericeStatus = [];
        this.individualSericeStatus = servicesStatus;
        this.upadtedCartSpecificDetails = this.modifyIndividualServiceAssetData(dataUpdated);
      });
    this.showMessagePopup('updatedServiceSuccessfully');
  }
  /*getAssetsLinkedDetails(serviceAssetDetails) {
    if (serviceAssetDetails.gatewayAsset && serviceAssetDetails.gatewayAsset.assetType) {
      let obj: any = {};
      let propName: string = '';
      let array: any = [];
      propName = serviceAssetDetails.gatewayAsset.assetType;
      array.push(serviceAssetDetails.gatewayAsset);
      obj[propName] = array;
      this.assetsLinkedToServiceDetails = obj;
      this.assetDetailsAfterLinkingUpdated = serviceAssetDetails;
    }
  }*/
  savedServiceAssetCard(savedServiceAsset) {
    this.showMessagePopup('savedServiceSuccessfully');
    this.getAllServiceAssets();
  }
  assetRegistrationErrorMsg(errMsg: string) {
    this.assetRegisterErrorMessages = [];
    this.assetRegisterErrorMessages.push(errMsg);
    document.getElementById('assetRegisterErrorMsgBtn').click();
    $('#sidebar-right-to-add').modal('hide');
  }
  deregisterServiceAsset(serviceAssetId) {
    this.deregisterSelectedServiceAssetId = serviceAssetId;
  }
  manageServices(actionDetails) {
    this.serverAction = actionDetails;
  }
  confirmedServerAction(status) {
    if (status === 'OK') {
      let serviceId: any = '';
      let action: string = '';
      action = this.serverAction.value;
      serviceId = this.serviceAssetId;
      let obj = {};
      obj = {
        'id': serviceId,
        'action': action
      };
      this.serviceLinkableService.manageServices(obj)
        .subscribe(res => {
          /*res['id'] = this.serviceAssetId;
          this.updatedIndividualSericeStatus = res;
          this.isServerManageActionSuccess = true;
          this.individualSericeStatus = [];
          this.individualSericeStatus.push(res);
          this.getServiceAssetDetails(this.serviceAssetId);
          this.serviceStatus = this.updatedIndividualSericeStatus.status;
          this.showMessagePopup('serviceActionUpdated');*/

         //this.allServiceAssets = this.syncSingleServiceStatus(res, this.allServiceAssets, this.serviceAssetId);
          //this.getIndividualServiceHealth(this.serviceAssetId);
        });
    }
  }
  confirmedDeregisteringAsset(status) {
    this.isAssetDeregisteredSuccess = false;
    if (status === 'OK') {
      this.serviceLinkableService.deregisterServiceAsset(this.deregisterSelectedServiceAssetId)
        .subscribe(res => {
          this.isAssetDeregisteredSuccess = true;
          this.getAllServiceAssets();
          $('#sidebar-right').modal('hide');
          $('html').css('overflow-y', 'auto');
          this.showMessagePopup('deregisterServiceSuccessfully');
          this.resetFormData = Math.random();
        });
    }
  }
  linkAssetToService(serviceAssetsArray) {
     this.linkingAssetId = serviceAssetsArray.toString();
  }

  delinkAsset(assetDetails) {
    this.delinkingAssetId = assetDetails._id;
  }
  confirmedlink(status) {
    if (status === 'OK') {
      let serviceAssetIds : any = [];
      let gatewayId: string = '';
      serviceAssetIds.push(this.serviceAssetId);
      gatewayId = this.linkingAssetId;
      let obj = {};
      obj = {
        'serviceAssetIds': serviceAssetIds,
        'gatewayId': gatewayId
      };
      this.serviceLinkableService.linkAsset(obj)
        .subscribe(res => {
          this.linkDelinkAction = true;
          this.getServiceAssetDetails(this.serviceAssetId);
          $('#assetListingPopup').hide(300);
        });
    }
  }

  confirmedDelink(status) {
    if (status === 'OK') {
      let serviceAssetIds : any = [];
      let gatewayId: string = '';
      serviceAssetIds.push(this.serviceAssetId);
      gatewayId = this.delinkingAssetId;
      let obj = {};
      obj = {
        'serviceAssetIds': serviceAssetIds,
        'gatewayId': gatewayId
      };
      this.serviceLinkableService.delinkAsset(obj)
        .subscribe(res => {
          this.linkDelinkAction = true;
          this.getServiceAssetDetails(this.serviceAssetId);
        });
    }
  }
  showMessagePopup(popup) {
    $('.' + popup).fadeIn(300);
    setTimeout(() => {
      $('.' + popup).fadeOut(300);
    }, 3000);
  }

  syncAllServiceStatus(serviceStatus, allServices) {
    let serviceStatusGroupById = _.groupBy(serviceStatus, '_id');
    // let serviceAssetsId = _.keys(serviceStatusGroupedbyId);
    allServices = _.map(allServices, function(data) {
      data.serverStatus = false;
      data.running = false;
      if (serviceStatusGroupById[data._id] && serviceStatusGroupById[data._id].length > 0) {
        data.serverStatus = true;
        if (serviceStatusGroupById[data._id][0].running) {
          data.running = true;
        }
      }
      return data;
    });
    return allServices;
  }
  /*syncSingleServiceStatus(updatedService) {
    //this.syncAllServiceStatus(updatedService,this.allServiceAssets);
    console.log(updatedService)
    this.allServiceAssets = _.map(this.allServiceAssets, function(servicesSelected) {
      if (updatedService && updatedService._id) {
        servicesSelected.serverStatus = true;
        if (servicesSelected._id===updatedService._id) {

          console.log(servicesSelected._id)
          console.log(servicesSelected.running)
          servicesSelected.running = updatedService.running;
        }
      }
      return servicesSelected;
    });
  }*/
}
