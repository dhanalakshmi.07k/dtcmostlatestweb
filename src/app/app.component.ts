import {Component, AfterViewInit, ViewChild, ElementRef, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { HTTPStatus } from '../interceptors/HttpInterceptor';
import { ValidityCheckService } from '../services/validity-check.service';
import {SetCssPropertiesService} from '../services/set-css-properties.service';
declare let bodymovin: any;
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements  OnInit {
  title = 'app';
  HTTPActivity: boolean;
  public resetSearchTextToHeader: any;
  public showAsset;
  public licenceValidityMessage: string;
  constructor(private httpStatus: HTTPStatus, private setCssPropertiesService: SetCssPropertiesService, public router: Router,
              private validityCheckService: ValidityCheckService) {
    this.licenceValidityMessage = '';
    this.httpStatus.getHttpStatus().subscribe((status: boolean) => {this.HTTPActivity = status;
      this.showAsset = status;
    });
  }

  ngOnInit() {
    if (!sessionStorage.getItem('userRoles')) {
      let userRoles: any;
      userRoles = ['operator'];
      sessionStorage.setItem('userRoles', JSON.stringify(userRoles));
    }
    this.validityCheckService.getValidityStatus()
      .subscribe(validityData => {
        if (validityData['enableSoftWarning']) {
          this.licenceValidityMessage = validityData['message'];
          this.validityCheckService.setValidityMessage(validityData['message']);
        } else {
          this.validityCheckService.setValidityMessage('');
        }
      });
    this.setCssPropertiesService.setCssClasses();
    this.checkForTheme();
  }

  checkForTheme() {
    let theme: string;
    theme = sessionStorage.getItem('theme');
    if (theme === 'dark-theme') {
      $('#body').removeClass('light-theme').addClass('dark-theme');
    } else if (theme === 'light-theme') {
      $('#body').removeClass('dark-theme').addClass('light-theme');
    }
  }

  resetSearchTextValue(url) {
    this.resetSearchTextToHeader = url;
  }

}
