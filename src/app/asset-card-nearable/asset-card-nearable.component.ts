import {Component, EventEmitter, Input, OnInit, Output, OnChanges, SimpleChanges} from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-asset-card-nearable',
  templateUrl: './asset-card-nearable.component.html',
  styleUrls: ['./asset-card-nearable.component.scss']
})
export class AssetCardNearableComponent implements OnChanges, OnInit {
  @Input() assetData: any;
  @Input() isSelectAllClicked: boolean;
  @Input() assetDetailsAfterLinkingUpdated: any;
  public assetLinkedDetails: any;
  public icon: string;
  public embossedFlag: boolean = false;
  constructor() { }
  @Output() registerAssetDetails:EventEmitter<any> = new EventEmitter<any>();
  @Output() registeredAssetData:EventEmitter<any> = new EventEmitter<any>();
  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'assetData') {
        if (change.currentValue !== change.previousValue) {
          this.getAssetsLinkedDetails(this.assetData);
        }
      }
    }
    if (this.assetData.selected === true) {
      this.embossedFlag = true;
    } else {
      this.embossedFlag = false;
    }
    if (this.assetDetailsAfterLinkingUpdated) {
      this.checkToUpdatedAssetLinkCount();
    }
  }

  checkToUpdatedAssetLinkCount() {
    if (this.assetData._id === this.assetDetailsAfterLinkingUpdated._id) {
      this.getAssetsLinkedDetails(this.assetDetailsAfterLinkingUpdated);
    }
  }

  getAssetsLinkedDetails(assetDetails) {
    let assetLinkedData: any;
    assetLinkedData = _.groupBy(assetDetails.assetsLinked, function (asset) {
      return asset.assetType;
    });
    this.assetLinkedDetails = assetLinkedData;
  }

  registerAsset() {
    if (!this.assetData.selected && !this.assetData.isRegistered) {
      this.embossedFlag = !this.embossedFlag;
      let obj = {};
      obj['embossedFlag'] = this.embossedFlag;
      obj['assetData'] = this.assetData.assetData;
      this.registerAssetDetails.emit(obj);
    }
    if (this.assetData.selected && !this.assetData.isRegistered) {
      this.embossedFlag = !this.embossedFlag;
      let obj = {};
      obj['embossedFlag'] = this.embossedFlag;
      obj['assetData'] = this.assetData.assetData;
      this.registerAssetDetails.emit(obj);
    }
  }

  registeredAssetToEdit() {
    if (this.assetData && this.assetData.isRegistered) {
      this.registeredAssetData.emit(this.assetData);
    }
  }

}
