import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetEditFromComponent } from './asset-edit-from.component';

describe('AssetEditFromComponent', () => {
  let component: AssetEditFromComponent;
  let fixture: ComponentFixture<AssetEditFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetEditFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetEditFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
