import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {FormService} from '../../services/form.service';
import {AssetService} from '../../services/asset.service';
import {ConfigService} from '../../services/config.service';
import {BeaconGatewayService} from '../../services/beacon-gateway.service';
import {RfidGatewayService} from '../../services/rfid-gateway.service';
import {ServiceLinkableService} from '../../services/serviceLinkable.service';
import { Subject,Actions} from '../../auth/rules';
declare var $: any;

@Component({
  selector: 'app-asset-edit-from',
  templateUrl: './asset-edit-from.component.html',
  styleUrls: ['./asset-edit-from.component.scss']
})
export class AssetEditFromComponent implements OnChanges, OnInit {

  Object = Object;
  @Input() formConfigData: any;
  @Input() assetId: string;
  @Input() assetData: any;
  @Input() resetFormData: number;
  @Input() isEditOptionClicked: boolean;
  @Input() isInputDisabled: boolean;
 // @Output() event: EventEmitter<string> = new EventEmitter<string>();
  @Output() updateAssetCart: EventEmitter<string> = new EventEmitter<string>();
  @Output() deregisterAsset: EventEmitter<any> = new EventEmitter();

  SUBJECT=Subject;
  ACTIONS=Actions;
  constructor(public formService: FormService, public assetService: AssetService, public configService: ConfigService,
              public beaconGatewayService: BeaconGatewayService, public rfidGatewayService: RfidGatewayService,
              private serviceLinkableService: ServiceLinkableService) {
    this.isInputDisabled = true;
  }

   ngOnChanges(changes: SimpleChanges) {
     for (const propName of Object.keys(changes)) {
       let change = changes[propName];
       if (propName === 'resetFormData') {
         if (change.currentValue !== change.previousValue) {
           this.formConfigData = [];
         }
       }
     }
     if (this.isEditOptionClicked === true) {
       $('.buttonEnable').prop('disabled', false);
       this.isInputDisabled = false;
       console.log('this.formConfigData', this.formConfigData);
     } else {
       $('.buttonEnable').prop('disabled', true);
       this.isInputDisabled = true;
     }
    }

  submitEditConfig() {
    if (this.assetData.assetType) {
      this.editAssetConfig();
    } else if (this.assetData.serviceType) {
      this.editServiceConfig();
    }
  }

  editServiceConfig() {
   this.serviceLinkableService.updateServiceById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
     .subscribe((serviceData: any) => {
       this.updateAssetCart.emit(serviceData);
    });
  }

  editAssetConfig() {
    this.assetService.updateAssetsById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
      .subscribe((assetvalues: any) => {
        let url = assetvalues.gatewayProtocol + '://' + assetvalues.gatewayIpAddress + ':' + assetvalues.gatewayPortNumber;
        if (this.configService.appConfig.beaconGateway.BEACON_GATEWAY_ASSET_NAME === assetvalues.assetType){
          this.beaconGatewayService.saveBeaconGatewaySetting(url, this.formService.formatAssetSaveDetails(assetvalues))
            .subscribe((res: any) => {
              this.updateAssetCart.emit(res);
            });
        }
        if (this.configService.appConfig.rfidGateway.RFID_GATEWAY_ASSET_NAME === assetvalues.assetType){
          let url = assetvalues.gatewayProtocol + '://' + assetvalues.gatewayIpAddress + ':' + assetvalues.gatewayPortNumber;
          this.rfidGatewayService.saveRFIDGatewaySetting(url, this.formService.formatAssetSaveDetails(assetvalues))
            .subscribe((res: any) => {
              this.updateAssetCart.emit(res);
            });
        }
        this.updateAssetCart.emit(assetvalues);
      });
  }

  deregisterSelectedAsset() {
    this.deregisterAsset.emit(this.assetId);
  }

  ngOnInit() {
    this.formConfigData = [];
    //this.getAbilitiesFromSession();
  }
 /* private getAbilitiesFromSession() {
    if (sessionStorage.getItem('rules')) {
      let rules = sessionStorage.getItem('rules');
      let userRules: any;
      userRules = JSON.parse(rules);
      ability.update(userRules);
    }
  }
  isAuthorized(action,subject){
    return ability.can(action,subject)
  }*/
}
