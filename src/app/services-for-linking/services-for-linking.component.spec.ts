import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesForLinkingComponent } from './services-for-linking.component';

describe('ServicesForLinkingComponent', () => {
  let component: ServicesForLinkingComponent;
  let fixture: ComponentFixture<ServicesForLinkingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesForLinkingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesForLinkingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
