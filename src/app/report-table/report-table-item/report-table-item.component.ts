import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-report-table-item',
  templateUrl: './report-table-item.component.html',
  styleUrls: ['./report-table-item.component.scss']
})
export class ReportTableItemComponent implements OnChanges, OnInit {
  @Input() inspectedCarDetails: any;
  @Input() showCarPlate: boolean;
  @Input() showEqiFound: boolean;
  @Input() showEngineDig: boolean;
  @Input() showUvi: boolean;
  @Input() showAvdds: boolean;
  @Input() showDriverInput: boolean;
  @Input() showStartTime: boolean;
  @Input() showEndTime: boolean;
  @Input() showPdf: boolean;
  @Output() inspectedCarDetailsForReportTable: EventEmitter<any> = new EventEmitter();
  @Output() reportComponentDetailsForReportTable: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit() {
  }
  ngOnChanges() {
  }
  pdfViewer(inspectedCarDetails) {
    this.inspectedCarDetailsForReportTable.emit(inspectedCarDetails);
  }

  showReportComponentDetails(inspectedCarDetails, component) {
     inspectedCarDetails['component'] = component;
     this.reportComponentDetailsForReportTable.emit(inspectedCarDetails);
  }

  getAvddsStatus(inspectedCarDetails) {
    if ( inspectedCarDetails) {
      if ( inspectedCarDetails['avdds'] || inspectedCarDetails['avds'] ) {
        return '../../assets/connectedIcon.png';
      } else {
        return '../../assets/notconnectedIcon.png';
      }
    }
  }

  getUvisStatus(inspectedCarDetails) {
    if ( inspectedCarDetails) {
      if ( inspectedCarDetails['uvis'] || inspectedCarDetails['uvs'] ) {
        return '../../assets/connectedIcon.png';
      } else {
        return '../../assets/notconnectedIcon.png';
      }
    }
  }

  getDriverFeedbackStatus(inspectedCarDetails) {
    let src = '../../assets/notconnectedIcon.png';
    if (inspectedCarDetails['driverFeedBack']) {
      if ((inspectedCarDetails.driverFeedBack != null) &&
        (!inspectedCarDetails.driverFeedBack.ac || inspectedCarDetails.driverFeedBack.ac === 'working') &&
        (!inspectedCarDetails.driverFeedBack.battery || inspectedCarDetails.driverFeedBack.battery === 'working') &&
        (!inspectedCarDetails.driverFeedBack.engine || inspectedCarDetails.driverFeedBack.engine === 'working') &&
        (!inspectedCarDetails.driverFeedBack.headLamps || inspectedCarDetails.driverFeedBack.headLamps === 'working') &&
        (!inspectedCarDetails.driverFeedBack.braking || inspectedCarDetails.driverFeedBack.braking === 'working') &&
        (!inspectedCarDetails.driverFeedBack.steering || inspectedCarDetails.driverFeedBack.steering === 'working') &&
        (!inspectedCarDetails.driverFeedBack.suspension || inspectedCarDetails.driverFeedBack.suspension === 'working') &&
        (!inspectedCarDetails.driverFeedBack.wheelAlignment || inspectedCarDetails.driverFeedBack.wheelAlignment === 'working') &&
        (!inspectedCarDetails.driverFeedBack.wheelVibration || inspectedCarDetails.driverFeedBack.wheelVibration === 'working') &&
        (!inspectedCarDetails.driverFeedBack.transmission || inspectedCarDetails.driverFeedBack.transmission === 'working')) {
        src = '../../assets/connectedIcon.png';
      } else if (inspectedCarDetails.driverFeedBack != null &&
        ((inspectedCarDetails.driverFeedBack.ac && inspectedCarDetails.driverFeedBack.ac === 'notWorking' ) ||
        (inspectedCarDetails.driverFeedBack.battery && inspectedCarDetails.driverFeedBack.battery === 'notWorking') ||
        (inspectedCarDetails.driverFeedBack.engine && inspectedCarDetails.driverFeedBack.engine === 'notWorking') ||
        (inspectedCarDetails.driverFeedBack.headLamps && inspectedCarDetails.driverFeedBack.headLamps === 'notWorking') ||
        (inspectedCarDetails.driverFeedBack.braking && inspectedCarDetails.driverFeedBack.braking === 'notWorking' ) ||
        (inspectedCarDetails.driverFeedBack.steering && inspectedCarDetails.driverFeedBack.steering === 'notWorking' ) ||
        (inspectedCarDetails.driverFeedBack.suspension && inspectedCarDetails.driverFeedBack.suspension === 'notWorking' ) ||
        (inspectedCarDetails.driverFeedBack.wheelAlignment && inspectedCarDetails.driverFeedBack.wheelAlignment === 'notWorking' ) ||
        (inspectedCarDetails.driverFeedBack.wheelVibration && inspectedCarDetails.driverFeedBack.wheelVibration === 'notWorking' ) ||
        (inspectedCarDetails.driverFeedBack.transmission && inspectedCarDetails.driverFeedBack.transmission === 'notWorking' ))) {
        src =  '../../assets/checkConnectedIcon.png';
      }
    } else {
      src =  '../../assets/notconnectedIcon.png';
    }
    return src;
  }

  getEngineInspectionStatus(inspectedCarDetails) {
    /*is engine inspection data recieved*/
    let src = '../../assets/notconnectedIcon.png';
    if ( inspectedCarDetails['engineInspection']) {
      src =  '../../assets/connectedIcon.png';
      if (inspectedCarDetails['engineInspection'] === 'pass') {
        src =  '../../assets/connectedIcon.png';
      } else if (inspectedCarDetails['engineInspection'] === 'fail') {
        src =  '../../assets/checkConnectedIcon.png';
      } else if (inspectedCarDetails['engineInspection'] === 'notFound') {
        src =  '../../assets/notconnectedIcon.png';
      }
    } else {
      src =  '../../assets/notconnectedIcon.png';
    }
    return src;
  }
}
