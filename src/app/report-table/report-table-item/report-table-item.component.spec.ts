import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportTableItemComponent } from './report-table-item.component';

describe('ReportTableItemComponent', () => {
  let component: ReportTableItemComponent;
  let fixture: ComponentFixture<ReportTableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportTableItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportTableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
