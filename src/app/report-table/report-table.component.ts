import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-report-table',
  templateUrl: './report-table.component.html',
  styleUrls: ['./report-table.component.scss']
})
export class ReportTableComponent implements OnInit {
  @Input() historicalInspectionList: any;
  @Input() showCarPlate: boolean;
  @Input() showEqiFound: boolean;
  @Input() showEngineDig: boolean;
  @Input() showUvi: boolean;
  @Input() showAvdds: boolean;
  @Input() showDriverInput: boolean;
  @Input() showStartTime: boolean;
  @Input() showEndTime: boolean;
  @Input() showPdf: boolean;
  @Output() inspectedCarDetailsForPdfViewer: EventEmitter<any> = new EventEmitter();
  @Output() reportComponentDetailsForModalReportDetails: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  inspectedCarDetailsForReport(inspectedCarDetails) {
    this.inspectedCarDetailsForPdfViewer.emit(inspectedCarDetails);
  }

  reportComponentDetailsForReport(inspectedCarDetails) {
    this.reportComponentDetailsForModalReportDetails.emit(inspectedCarDetails);
  }

}
