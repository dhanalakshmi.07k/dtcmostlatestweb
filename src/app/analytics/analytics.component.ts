import {Component, OnInit, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import {InspectionBayService} from '../../services/inspectionBay.service';
import {SearchService} from '../../services/search.service';
import {IotzenPillType} from  '../core/iotzen-pills/iotzen-pill-type';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
// import {NgbDateCustomParserFormatter} from '../../services/dateformat';
import { Chart } from 'chart.js';
import * as _ from 'lodash';
import {NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import {NgbDate} from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import * as moment from 'moment-timezone';
import { Subject,Actions} from '../../auth/rules';
export const MY_FORMATS = {
  parse: {
    dateInput: 'll',
  },
  display: {
    dateInput: 'll',
    monthYearLabel: 'MMM YYYY',
  },
};

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss'],
  /*
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}
  ]*/
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class AnalyticsComponent implements OnInit, AfterViewInit {
  minDate = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
  maxDate = new Date();
 //  date = new FormControl(moment());
  public isEndDateDisabled: boolean;
  public inspectionChart: any;
  public startDate: any;
  public endDate: any;
  public today: any;
  public seriesType: any = {
    'hour': {value: 'hour', label: 'Hour'},
    'day': {value: 'day', label: 'Day'},
    'month': {value: 'month', label: 'Month'},
    'year': {value: 'year', label: 'Year'}
  };
  public chartType: any = {
    'line': {value: 'line', label: 'Line'},
    'bar': {value: 'bar', label: 'Bar'},
    'radar': {value: 'radar', label: 'Radar'},
    'pie': {value: 'pie', label: 'Pie'},
    'polarArea': {value: 'polarArea', label: 'Polar Area'}
  };
  public analyticsData: any = {
    seriesType: this.seriesType.day,
    chartType: this.chartType.bar,
    chartData: null,
    /*startDate: this.calendar.getPrev(this.calendar.getToday(), 'm', 1),
    endDate: this.calendar.getToday()*/
    startDate: new Date(new Date().setMonth(new Date().getMonth() - 1)),
    endDate: new Date()
  };
  public showCircularLoader: boolean;
  public arrayOfSeriesTypePills: Array<IotzenPillType> = [];
  public arrayOfChartTypePills: Array<IotzenPillType> = [];
  @ViewChild('myCanvas') myCanvas: ElementRef;
  constructor(public inspectionBayService: InspectionBayService, private calendar: NgbCalendar, private searchService: SearchService) {
    this.showCircularLoader = true;
    this.isEndDateDisabled = true;
  }

  SUBJECT=Subject;
  ACTIONS=Actions;
  ngOnInit() {
    // this.today = this.calendar.getToday();
    // this.analyticsData.startDate = this.calendar.getPrev(this.calendar.getToday(), 'm', 1);
    // this.analyticsData.startDate.day = this.calendar.getToday().day;
   // this.today = new Date();
    this.buildPillsArray();
    this.searchService.showSearchBar(false);
    let oneMonthBackDate: Date;
    oneMonthBackDate = new Date(new Date().setMonth(new Date().getMonth() - 1));
    // this.analyticsData.startDate = new Date(new Date().setMonth(new Date().getMonth() - 1));
    this.analyticsData.startDate.day = oneMonthBackDate.getDate();
    this.analyticsData.startDate.month = oneMonthBackDate.getMonth();
    this.analyticsData.startDate.year = oneMonthBackDate.getFullYear();
    let todayDate: Date;
    todayDate = new Date();
   // this.analyticsData.endDate = new Date();
    this.analyticsData.endDate.day = todayDate.getDate();
    this.analyticsData.endDate.month = todayDate.getMonth();
    this.analyticsData.endDate.year = todayDate.getFullYear();
    if (!this.analyticsData.chartData) {
      this.getAnalyticsData();
    }
  }
  ngAfterViewInit() {

  }

  buildPillsArray() {
    this.arrayOfSeriesTypePills = [{id: 'hour', label: 'H', description: 'Series In Hour'},
      {id: 'day', label: 'D', description: 'Series In Day'},
      {id: 'month', label: 'M', description: 'Series In Month'},
      {id: 'year', label: 'Y', description: 'Series In Year'}];
    this.arrayOfChartTypePills = [{id: 'line', label: 'Line', description: 'Graph Type Line'},
      {id: 'bar', label: 'Bar', description: 'Graph Type Bar'},
      {id: 'radar', label: 'Radar', description: 'Graph Type Radar'}];
  }
  getActivePillValue(value) {
    switch (value) {
      case 'hour':
        this.analyticsData.seriesType = this.seriesType['hour'];
        break;
      case 'day':
        this.analyticsData.seriesType = this.seriesType['day'];
        break;
      case 'month':
        this.analyticsData.seriesType = this.seriesType['month'];
        break;
      case 'year':
        this.analyticsData.seriesType = this.seriesType['year'];
        break;
      case 'line':
        this.analyticsData.chartType = this.chartType['line'];
        break;
      case 'bar':
        this.analyticsData.chartType = this.chartType['bar'];
        break;
      case 'radar':
        this.analyticsData.chartType = this.chartType['radar'];
        break;
      default:
        break;
    }
    this.inspectionChart = null;
    this.getAnalyticsData();
  }
  onDateChange(Date) {
    this.inspectionChart = null;
    this.getAnalyticsData();
  }
  /*refreshChart() {
    this.inspectionChart = null;
    this.getAnalyticsData();
  }*/
  getAnalyticsData() {
    this.showCircularLoader = true;
    if ( this.analyticsData.chartData) {
      this.analyticsData.chartData.destroy();
    }
    let status: any = null;
    let carRegNo: any = null;
    this.analyticsData.startDate = moment(this.analyticsData.startDate).toDate();
    this.analyticsData.startDate.day = this.analyticsData.startDate.getDate();
    this.analyticsData.startDate.month = this.analyticsData.startDate.getMonth();
    this.analyticsData.startDate.year = this.analyticsData.startDate.getFullYear();

    this.analyticsData.endDate = moment(this.analyticsData.endDate).toDate();
    this.analyticsData.endDate.day = this.analyticsData.endDate.getDate();
    this.analyticsData.endDate.month = this.analyticsData.endDate.getMonth();
    this.analyticsData.endDate.year = this.analyticsData.endDate.getFullYear();
    let startDate = moment().year(this.analyticsData.startDate.year).month(this.analyticsData.startDate.month)
      .date(this.analyticsData.startDate.day).utc();
    let endDate = moment().year(this.analyticsData.endDate.year).month(this.analyticsData.endDate.month)
      .date(this.analyticsData.endDate.day).utc();
    let start = startDate.clone().startOf('day').utc().valueOf();
    let end = endDate.clone().endOf('day').utc().valueOf();
    this.startDate = start;
    this.endDate = end;
    this.inspectionBayService.getAnalyticsData(this.startDate, this.endDate, this.analyticsData.seriesType.value, true, carRegNo)
        .subscribe(data => {
          this.showCircularLoader = false;
          let analyticsData: any = [];
          analyticsData = data;
          let groupByDate: any = [];
          let groupByCounts: any = [];
          let date: any;
          let modifiedDate: any;
          let milliSecondToInt: number;
          /*analyticsData.forEach((dataObj, index) => {
             milliSecondToInt = +dataObj['milliSeconds'];
             date = new Date(milliSecondToInt);
             modifiedDate = this.getFormattedDate(this.analyticsData.seriesType.value, date);
             groupByDate.push(modifiedDate);
             // groupByDate.push(dataObj['dateString']);
             groupByCounts.push(dataObj['count']);
           });*/
          let yAxis = _.keys(data);
          /*let analyticsdata = _.map(allSeriesKey,function(seriesStaus){

          })*/
          let seriesPassData = [];
          let seriesCheckData = [];
          let seriesFailData = [];
          let xAxisByStatus = {
              pass: [],
              fail: [],
              check: []
          };
          let arrayLength = 0;
          _.each(yAxis, function(dateString) {
              let allStatusDetails = data[dateString];
              xAxisByStatus.pass[arrayLength] = 0;
              xAxisByStatus.fail[arrayLength] = 0;
              xAxisByStatus.check[arrayLength] = 0;
              _.each(allStatusDetails, function(countByStatus){
                xAxisByStatus[countByStatus.status][arrayLength] = countByStatus.count;
              });
            arrayLength = arrayLength + 1;
          });
          this.drawChart(yAxis, xAxisByStatus);
        });
  }
  setSeriesType(type) {
    this.analyticsData.seriesType = this.seriesType[type];
  }
  setChartType(type) {
    this.analyticsData.chartType = this.chartType[type];
  }
  getFormattedDate(seriesType, date) {
    switch (seriesType) {
      case 'hour':
        return moment(date).format('MMM Do YYYY, h a');
      case 'day':
        return moment(date).format('MMM Do YYYY');
      case 'month':
        return moment(date).format('MMM YYYY');
      case 'year':
        return moment(date).format('YYYY');
      default:
        return moment(date).format('MMM Do YYYY');
    }
  }
  drawChart(label, data) {
    if (document.getElementById('inspectionChart')) {
      if ( this.analyticsData.chartData) {
       // this.analyticsData.chartData.destroy();
        this.graphPlotting(label, data);
      } else {
        this.graphPlotting(label, data);
      }
    }
  }

  graphPlotting(label, data) {
    let canvas : any = document.getElementById('inspectionChart');
    let ctx = canvas.getContext('2d');
      this.analyticsData.chartData = new Chart(ctx,
        {
          type: this.analyticsData.chartType.value,
          data: {
            labels: label,
            datasets: [{
              label: 'Pass',
              data: data['pass'],
              backgroundColor: 'rgba(200, 230, 201,0.8)',
              borderColor: 'rgba(56, 142, 60,1.9)',
              borderWidth: 1.5,
            }, {
              label: 'Check',
              data: data['check'],
              backgroundColor: 'rgba(254, 205, 86,0.8)',
              borderColor: 'rgba(255, 160, 0,1.9)',
              borderWidth: 1.5,
            }, {
              label: 'Fail',
              data: data['fail'],
              backgroundColor: 'rgba(255, 205, 210,0.8)',
              borderColor: 'rgba(239, 83, 80,1.9)',
              borderWidth: 1.5,
            }],
          },
          options: {
            tooltips: {
              titleFontSize: 15,
              titleMarginBottom: 10,
              titleFontColor: '#212529',
              bodyFontSize: 15,
              bodySpacing: 10,
              bodyFontColor: '#212529',
              footerFontSize: 15,
              footerMarginTop: 10,
              footerFontColor: '#212529',
              backgroundColor: 'rgba(255,255,255,0.9)',
              borderWidth: 1,
              borderColor: '#7F7F7F',
              mode: 'label',
              callbacks: {
                label: function(tooltipItem, data) {
                  let label = data.datasets[tooltipItem.datasetIndex].label;
                  let dataSet = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                  return label + ': '  + dataSet.toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, '1,');
                },
                footer: function(tooltipItems, data) {
                  let total = 0;
                  for (let i = 0; i < tooltipItems.length; i++) {
                    total += parseInt(tooltipItems[i].yLabel, 10);
                  }
                  return 'Total: ' + total;
                }
              },
            },
            responsive: true,
            legend: {
              position: 'right' // place legend on the right side of chart
            },
            scales: {
              steppedLine: true,
              xAxes: [{
                stacked: false, // this should be set to make the bars stacked
              }],
              yAxes: [{
                stacked: false // this also..
              }]
            }
          },
        });
  }

  plotLineGraph(ctx, label, data) {
    this.analyticsData.chartData = new Chart(ctx,
      {
        type: this.analyticsData.chartType.value,
        data: {
          labels: label,
          datasets: [{
            label: 'Inspection Count',
            data: data,
            backgroundColor: 'rgba(54, 162, 235, 0.2)',
            borderColor: ['rgba(54, 162, 235, 1)'],
            borderWidth: 1.5,
            fill: true
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
  }


  disableDate(date: NgbDate, current: {month: number, year: number}) {
    let disable = false;
    let currentYear = moment().year();
    if ( current.year > currentYear ) {
      disable = true;
    }
    if (current.year < currentYear - 1 ) {
      disable = true;
    }
    return disable;
  }

  downloadAnalytics() {
    this.getAnalyticsData();
    this.inspectionBayService.downloadAnalyticsData(this.startDate, this.endDate, this.analyticsData.seriesType.value, true, null);
  }

}
