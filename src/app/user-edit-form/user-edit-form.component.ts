import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import {FormControl, Validators, FormBuilder, FormGroup} from '@angular/forms';
import {UserProfile} from '../user-profile/user-profile-type';
import {UserManagementService} from '../../services/userManagement.service';
import { MyErrorStateMatcher } from '../user-profile/my-error-state-matcher';

@Component({
  selector: 'app-user-edit-form',
  templateUrl: './user-edit-form.component.html',
  styleUrls: ['./user-edit-form.component.scss']
})
export class UserEditFormComponent implements OnInit, OnChanges {
  userEditForm: FormGroup;
  @Input() roles: Array<string>;
  public hide: boolean;
  usernameExistCheck = new MyErrorStateMatcher();
  @Input() showCircularLoader: boolean;
  @Input() isUserUpdateSuccess: boolean;
  @Input() isRoleEditable: boolean;
  @Output() userDetailsEvent: EventEmitter<object> = new EventEmitter();
  @Input() userDetailsToEdit: UserProfile;
  constructor(private fb: FormBuilder, private userManagementService: UserManagementService) {
    this.hide = true;
    this.showCircularLoader = false;
  }

  ngOnInit() {
    this.buildForm();
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'userDetailsToEdit') {
        if (change.currentValue !== change.previousValue) {
          this.buildForm();
          this.setFormValues();
        }
      }
      if (propName === 'isUserUpdateSuccess') {
        if (change.currentValue !== change.previousValue) {
          if (this.isUserUpdateSuccess === true) {
            this.userDetailsToEdit = null;
          }
        }
      }
    }
  }

  setFormValues() {
    this.userEditForm.controls['usernameGroup']['controls']['username'].setValue(this.userDetailsToEdit.username);
    // this.userEditForm.controls['password'].setValue(this.userDetailsToEdit.password);
    this.userEditForm.controls['firstName'].setValue(this.userDetailsToEdit.firstName);
    this.userEditForm.controls['lastName'].setValue(this.userDetailsToEdit.lastName);
    this.userEditForm.controls['email'].setValue(this.userDetailsToEdit.email);
    this.userEditForm.controls['roles'].setValue(this.userDetailsToEdit.roles[0]);
    this.userEditForm.controls['disabledRole'].setValue(this.userDetailsToEdit.roles[0]);
    this.userEditForm.get('disabledRole').disable();
  }

  buildForm() {
    this.userEditForm = this.fb.group({
      usernameGroup: this.fb.group({
        username: ['', [Validators.required, Validators.minLength(5)]],
        isUsernameExist: ['', []],
      }, {validator: this.checkUsername }),
      // password: ['', [Validators.required, Validators.minLength(5)]],
      firstName: ['', [Validators.required]],
      lastName: [''],
      email: ['', [Validators.required, Validators.email]],
      roles: ['', [Validators.required]],
      disabledRole: ['', []]
    });
    this.setIsUsernameExistDefaultValue();
  }

  setIsUsernameExistDefaultValue() {
    this.userEditForm.controls['usernameGroup']['controls']['isUsernameExist'].setValue(false);
  }

  onUserNameChange(userName: string) {
    let strLength: number;
    strLength = userName.length;
    if ( strLength > 4 ) {
      this.userManagementService.isUserNameExist(userName)
        .then((isUserNameAlereadyUsed: boolean) => {
          if (userName === this.userDetailsToEdit.username) {
            this.setIsUsernameExistDefaultValue();
          } else {
            this.userEditForm.controls['usernameGroup']['controls']['isUsernameExist'].setValue(isUserNameAlereadyUsed);
          }
        });
    } else {
      this.setIsUsernameExistDefaultValue();
    }
  }

  emitUserDetails() {
    let rolesArray: any = [];
    let usernameValue: string;
    usernameValue = this.userEditForm.value.usernameGroup.username;
    rolesArray.push(this.userEditForm.value.roles);
    this.userEditForm.value.roles = rolesArray;
    this.userEditForm.value['username'] = usernameValue;
    this.userDetailsEvent.emit(this.userEditForm.value);
  }

  checkUsername(group: FormGroup) { // here we have the 'usernameGroup' group
    let isUsernameExist = group.controls.isUsernameExist.value;
    return isUsernameExist === true ? { usernameExist: true } : null;
  }

}
