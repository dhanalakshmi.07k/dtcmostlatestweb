import {Component, OnInit, Output, EventEmitter, Input,OnChanges} from '@angular/core';
import {AssetService} from "../../services/asset.service";
import {PagerService} from "../../services/pager.service";

//pagination plunker https://embed.plnkr.co/oyFWJe/
@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  constructor(public assetService: AssetService,public pagerService: PagerService) { }

  // array of all items to be paged
  private allItems: any[];
  public assetCount:number
  public skip:number
  public limit:number
  public currentPage:number
  // pager object
  pager: any = {};
  @Input() totalPageCount: any;

  // paged items
  pagedItems: any[];
  @Output() event:EventEmitter<number> = new EventEmitter<number>()


  ngOnInit() {

  }


  ngOnChanges(){
    if( this.totalPageCount){
      this.pager = this.pagerService.getPager(this.totalPageCount, 1);
    }

  }

  setPage(page: number) {
    this.pager.currentPage=page
    this.event.emit(page)
 /*   if(page==1){
      this.skip=page-1;
      this.limit=page*10;
    }else{
      this.skip=(page-1)*10;
      this.limit=10;
    }
    this.assetService.getAssetsBasedOnRange( this.skip, this.limit)
      .subscribe((assetsBasedOnRange: any) => {
        // get pager object from service

        this.pagedItems =assetsBasedOnRange;
         this.event.emit(assetsBasedOnRange)
      });*/
  }

  /*setPageCountForPAgination(data){

    this.assetCount = data
    // initialize to page 1
    this.setPage(1);
  }*/

}
