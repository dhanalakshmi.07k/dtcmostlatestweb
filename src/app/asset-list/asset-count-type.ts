export interface AssetCountType {
  count: Array<Count>;
}

export interface Count {
  count: number;
  type: string;
}

export interface AssetTypes {
   label: string;
   type: string;
}

export interface AssetTypeWithCount {
  label: string;
  type: string;
  count: number;
}
