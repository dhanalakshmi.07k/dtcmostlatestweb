import {Component, EventEmitter, OnInit, OnDestroy , Output, ViewChild} from '@angular/core';
import {ServiceLinkableService} from '../../services/serviceLinkable.service';
import { MatPaginator } from '@angular/material';
import { AssetService } from '../../services/asset.service';
import {AssetCountType} from './asset-count-type';
import {HealthCheckService} from '../../services/healthCheck.service';
import {Router} from "@angular/router";
import {FormService} from "../../services/form.service";
import {AssetConfigService} from '../../services/asset-config.service';
import {SearchService} from '../../services/search.service';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
import * as moment from 'moment-timezone';
import { Subject,Actions} from '../../auth/rules';
declare var $ :any;

@Component({
  selector: 'app-asset-list',
  templateUrl: './asset-list.component.html',
  styleUrls: ['./asset-list.component.scss']
})
export class AssetListComponent implements OnInit, OnDestroy {
  currRoute: string;
  assetFromConfig:any;
  public assetCountByType;
  public showSlider: boolean = false;
  public assetDetailsAfterLinkingUpdated: any;
  public allAssets: any = [];
  public assetTypesWithLabels: any = [];
  public assetCount: any = [];
  public assetCountCardArrayOne: any = [];
  public assetCountCardArrayTwo: any = [];
  public allServersStatsArray: any = [];
  public allServicesStatus: any = [];
  public selectedAssets: any;
  public assetData: any;
  public assetType: string;
  public  assetSelectedType: any;
  public assetId: any;
  public assetLinkedDetails: any = {};
  public delinkAssetDetails: any;
  public delinkServiceDetails: any;
  public  linkAssetDetails: any;
  public  deregisterSelectedAssetId: any;
  public resetFormData: number;
  public selectedPageCount: any;
  //public totalPageCount: any;
  //public currentPage: any;
  public upadtedCartSpecificDetails: any ;
  public output: string;
  public skip: number;
  public limit: number;
  public isShowAsset: boolean;
  public selectedAssetsNames: string;
  public selectedAssetsNamesArray: any = ['Car'];
  public searchForAsset: string = '';
  private searchPlaceholder: string = 'Search Car';
  public  linkDelinkAction: boolean = false;
  public  isServiceLinkDelinkAction: boolean = false;
  public assetConfigDetails: any;
  public serviceAssetConfigDetails: any;
  public embossedFlagStatus: boolean;
  public searchOperationDone: boolean;
  public isAssetDeregisteredSuccess: boolean;
  public selectedAssetToLinkCount: number;
  public showCircularLoaderValue: number;
  public resetAddAssetFormValue: number;
  public assetRegisterErrorMessages: any = [];
  public  serviceIdsToLinkAsset: any = [];
  private searchTextSub: Subscription;
  private inputArr: any = [];
  SUBJECT=Subject;
  ACTIONS=Actions;
  constructor(public assetService: AssetService, private router: Router, public formService: FormService,
              public assetConfigService: AssetConfigService, private searchService: SearchService,
              private  serviceLinkableService: ServiceLinkableService, public healthCheckService: HealthCheckService) {
    // this.getAllServersStats();
    this.pageNumber = 1;
    this.currRoute = router.url;
    this.pagination = {
      'currentPage' : 1,
      'totalPageCount': 0,
      'recordsPerPage' : 12,
      'pageCountValue' : 0,
    };
    this.selectedAssets = ['car'];
    this.selectedAssetsNames = 'Car';
    this.searchOperationDone = false;
    this.isAssetDeregisteredSuccess = false;
  }
  @ViewChild('paginator') paginator: MatPaginator;
  public countObj: AssetCountType;
  public pagination: any;
  public pageNumber: any;
  p: number = 1;
 // @Output() event:EventEmitter<string> = new EventEmitter<string>()

  ngOnInit() {
    this.isShowAsset = true;
    this.doSearchTextResetAndOtherOperations();
    this.assetConfigService.getAssetsConfig()
      .then(assetConfig => {
        this.assetConfigDetails = assetConfig;
      });
    this.getServiceAssetConfiguration();
    this.getServicesHealth();
    // this.getAssetTotalCountForPagination();
    // this.getFirstFewAssets();
    this.getAssetForSelectedAssetTypes(0, this.pagination.recordsPerPage);
    this.getAssetIndividualCounts();
    this.searchTextSub = this.searchService.searchText.subscribe(event => {
      this.searchForAsset = event;
      let eventLength;
      eventLength = event.length;
      if (eventLength > 3) {
        this.searchOperationDone = true;
        this.pagination.currentPage = 1;
        if (this.searchForAsset) {
          this.setPaginatorPageIndexToZero();
          this.getAssetForSelectedAssetTypesOnSearch(this.searchForAsset, 0 , this.pagination.recordsPerPage );
        } else {
          this.getAssetForSelectedAssetTypes(0, this.pagination.recordsPerPage);
        }
      } else {
        if (this.searchOperationDone) {
          this.setPaginatorPageIndexToZero();
          this.searchOperationDone = false;
          this.getAssetForSelectedAssetTypes(0, this.pagination.recordsPerPage);
        }
      }
    });
  }

  doSearchTextResetAndOtherOperations() {
    this.searchService.showSearchBar(true);
    this.searchService.resetSearchTextInSearchBox();
    this.searchService.sendSearchPlaceholderValue(this.searchPlaceholder);
    this.searchService.sendSearchedInfoValue(this.selectedAssetsNames);
  }

  ngOnDestroy() {
    this.searchTextSub.unsubscribe();
  }

  groupAllServiceLinkableAssets(assetConfigDetails) {
    let serviceLinkableAssets: any;
    serviceLinkableAssets = _.groupBy(assetConfigDetails, function (asset) {
      if (asset.config.isServiceLinkable === true) {
        return asset.type;
      }
    });
    return serviceLinkableAssets;
  }

  setPaginatorPageIndexToZero() {
    if (this.paginator) {
      this.paginator.pageIndex = 0;
    }
  }

  getAssetTotalCountForPagination() {
    this.assetService.getAssetPaginationCount()
      .subscribe((allAssetCount: any) => {
        this.pagination.totalPageCount = allAssetCount.count;
      });
  }

  getServiceAssetConfiguration() {
    this.assetConfigService.getServiceAssetConfig()
      .then(serviceAssetConfig => {
        this.serviceAssetConfigDetails = serviceAssetConfig;
      });
  }

  getFirstFewAssets() {
    this.isShowAsset = true;
    this.assetService.getAssetsBasedOnRange(0,this.pagination.recordsPerPage)
      .subscribe(allAssets => {
        this.isShowAsset = false;
        this.allAssets = allAssets;
      });
  }

  getAssetIndividualCounts() {
    this.assetService.getAssetCount()
      .subscribe((assetIndividualCount: AssetCountType) => {
        this.assetCountByType = assetIndividualCount['count'];
        this.assetCountByType = _.orderBy(this.assetCountByType, ['order'], ['asc']);
        this.assetCountCardArrayOne = _.slice(this.assetCountByType, 0, 6);
        this.assetCountCardArrayTwo = _.slice(this.assetCountByType, 6, this.assetCountByType.length);
        for (let assetCount of this.assetCountByType) {
          for (let selectedAsset of this.selectedAssets) {
            if (assetCount.type === selectedAsset) {
              assetCount.selectedForFiler = true;
              break;
            } else {
              assetCount.selectedForFiler = false;
            }
          }
        }
      });
  }


  filterAssetOnType(asset) {
    /*if (asset.config && asset.config.showStats && asset.config.showStats === true) {
      this.getAllServersStats();
    }*/
    // this.getAllServersStats();
    this.assetTypeCollectionToFilter(asset.type);
    this.assetLabelCollectionToDispaly(asset.label);
    this.searchOperationDone = false;
    this.searchForAsset = '';
    if (this.searchForAsset) {
      this.getAssetForSelectedAssetTypesOnSearch(this.searchForAsset, 0 , this.pagination.recordsPerPage );
    } else {
      this.setPaginatorPageIndexToZero();
      this.getAssetForSelectedAssetTypes(0, this.pagination.recordsPerPage);
    }
    this.searchService.resetSearchTextInSearchBox();
  }

  setEmbossedFlagStatus(embossedFlagStatus) {
    this.embossedFlagStatus = embossedFlagStatus;
  }

  assetTypeCollectionToFilter(assetType) {
    this.selectedAssets = [];
    this.selectedAssets.push(assetType);
    /*let res;
    res = _.find(this.selectedAssets, function(o) { return o === assetType; });
    if (res) {
      let event = _.remove(this.selectedAssets, function(n) {
        return (n === res);
      });
    }  else {
      this.selectedAssets.push(assetType);
    }*/
  }

  assetLabelCollectionToDispaly(assetLabel) {
    this.selectedAssetsNamesArray = [];
    this.selectedAssetsNamesArray.push(assetLabel);
    /*let label;
    label = _.find(this.selectedAssetsNamesArray, function(o) { return o === assetLabel; });
    if (label) {
      let event = _.remove(this.selectedAssetsNamesArray, function(n) {
        return (n === label);
      });
    }  else {
      this.selectedAssetsNamesArray.push(assetLabel);
    }*/
    this.selectedAssetsNames = this.selectedAssetsNamesArray.toString();
    this.setSearchingPlaceholder(this.selectedAssetsNamesArray);
    this.searchService.sendSearchedInfoValue(this.selectedAssetsNames);
  }

  getAssetForSelectedAssetTypesOnSearch(searchForAsset: string, skip, limit) {
    this.pagination.currentPage = 1;
    let selectedAssetTypesAsString: string = '';
    selectedAssetTypesAsString = this.selectedAssets.toString();
    if (this.embossedFlagStatus === false) {
      selectedAssetTypesAsString = '';
      this.selectedAssets = [];
      this.selectedAssetsNamesArray = [];
      this.selectedAssetsNames = this.selectedAssetsNamesArray.toString();
      this.setSearchingPlaceholder(this.selectedAssetsNamesArray);
      this.searchService.sendSearchedInfoValue(this.selectedAssetsNames);
    }
    this.getSearchedAssetPaginationCount(searchForAsset, selectedAssetTypesAsString, skip, limit);
  }

  private setAssetListByTypesOnSearch(searchForAsset: string, selectedAssetTypesAsString: string, skip: number, limit: number) {
    this.isShowAsset = true;
    this.assetService.getAssetOnSearch(searchForAsset, selectedAssetTypesAsString, skip, limit)
      .subscribe(result => {
        let isServiceLinkableAssetFoundInArray: boolean;
        this.isShowAsset = false;
        let allAssetArray: any = [];
        allAssetArray = this.checkAssetTypeToDisplayStatus(result);
        isServiceLinkableAssetFoundInArray = this.checkForServiceLinkableAssetTypeInAssetList(result);
        if (isServiceLinkableAssetFoundInArray) {
          this.getAllServersStats(allAssetArray);
        } else {
          this.allAssets = this.checkServerStatus(allAssetArray);
        }
      });
  }

  getSearchedAssetPaginationCount(searchForAsset: string, selectedAssetsAsString: string, skip: number, limit: number) {
    this.isShowAsset = true;
     this.assetService.getSearchedAssetPaginationCount(this.searchForAsset, selectedAssetsAsString)
       .subscribe(searchedAssetCount => {
         this.isShowAsset = false;
         this.pagination.totalPageCount = searchedAssetCount['count'];
         this.getPageCount();
         if (this.pagination.totalPageCount > 0) {
           this.setAssetListByTypesOnSearch(searchForAsset, selectedAssetsAsString, skip, limit);
         } else if (this.pagination.totalPageCount === 0) {
           this.allAssets = [];
         }
       });
  }


  getAssetForSelectedAssetTypes(skip, limit) {
    this.pagination.currentPage = 1;
    let selectedAssetsStr: string = '';
    selectedAssetsStr = this.selectedAssets.toString();
    if (this.embossedFlagStatus === false) {
      selectedAssetsStr = '';
      this.selectedAssets = [];
      this.selectedAssetsNamesArray = [];
      this.selectedAssetsNames = this.selectedAssetsNamesArray.toString();
      this.setSearchingPlaceholder(this.selectedAssetsNamesArray);
      this.searchService.sendSearchedInfoValue(this.selectedAssetsNames);
    }
    this.getAssetCountByTypes(selectedAssetsStr, skip, limit);
  }

  private setAssetListByTypes(selectedAssetsStr: string, skip: number, limit: number) {
    this.isShowAsset = true;
    this.assetService.getAllByType(selectedAssetsStr, skip, limit)
      .subscribe(result => {
        this.isShowAsset = false;
        let allAssetArray: any = [];
        let isServiceLinkableAssetFoundInArray: boolean;
        allAssetArray = this.checkAssetTypeToDisplayStatus(result);
        isServiceLinkableAssetFoundInArray = this.checkForServiceLinkableAssetTypeInAssetList(result);
        if (isServiceLinkableAssetFoundInArray) {
          this.getAllServersStats(allAssetArray);
        } else {
          this.allAssets = this.checkServerStatus(allAssetArray);
        }
      });
  }

  private getAssetCountByTypes(selectedAssetsStr: string, skip: number, limit: number) {
    this.isShowAsset = true;
    this.assetService.getCountByTypes(selectedAssetsStr)
      .subscribe(result => {
        this.isShowAsset = false;
        this.pagination.totalPageCount = result['count'];
        this.getPageCount();
        if (this.pagination.totalPageCount > 0) {
          this.setAssetListByTypes(selectedAssetsStr, skip, limit);
        } else if (this.pagination.totalPageCount === 0) {
          this.allAssets = [];
        }
      });
  }
  getPageCount() {
    this.inputArr = [];
    this.pageNumber = 1;
    this.pagination.pageCountValue = Math.ceil((this.pagination.totalPageCount) / 12);
    for (let i = 1; i <= this.pagination.pageCountValue; i++) {
      this.inputArr[i] = i;
    }
  }

  setDataFromConfig(data) {
    this.output = data;
  }

  syncUpdateAssetCart(dataUpdated) {
   // this.upadtedCartSpecificDetails = dataUpdated;
    let isUpdatedAssetIsServiceLinkableAsset: boolean;
    isUpdatedAssetIsServiceLinkableAsset = false;
    let array: any = [];
    array.push(dataUpdated);
    isUpdatedAssetIsServiceLinkableAsset = this.checkForServiceLinkableAssetTypeInAssetList(array);
    if (isUpdatedAssetIsServiceLinkableAsset) {
      this.checkUpdatedServiceLinkableAssetStatus(dataUpdated);
    } else {
      this.upadtedCartSpecificDetails = dataUpdated;
    }
    this.showMessagePopup('updatedAssetSuccessfully');
  }

  assetRegistrationErrorMsgToAssetList(errMsg: string) {
    this.assetRegisterErrorMessages = [];
    this.assetRegisterErrorMessages.push(errMsg);
    document.getElementById('assetRegisterErrorMsgBtn').click();
    $('#sidebar-right-to-add').modal('hide');
  }

  savedAssetCardToAssetList(savedAssetValue) {
    this.showMessagePopup('savedAssetSuccessfully');
    //this.selectedAssets = [];
    //this.getAssetTotalCountForPagination();
    //this.getFirstFewAssets();
    this.getAssetForSelectedAssetTypes(0, this.pagination.recordsPerPage);
    this.getAssetIndividualCounts();
  }

  editAsset(assetDetails) {
    this.linkDelinkAction = false;
    this.isServiceLinkDelinkAction = false;
    this.showSlider = true;
    this.assetId = assetDetails._id;
    this.assetType = assetDetails.assetType;
    this.getAssetLinked(this.assetId);
    this.showCircularLoaderValue = Math.random();
  }

  getAssetLinked(assetId) {
    this.assetService.getAssetDetailsByMongodbId(assetId)
      .subscribe((assetDetails: any) => {
        this.assetData = assetDetails;
        this.getAssetsLinkedDetails(assetDetails);
      });
  }

  getAssetsLinkedDetails(assetDetails) {
    if (assetDetails.assetsLinked && assetDetails.assetsLinked.length > 0)  {
      let assetLinkedData: any;
      assetLinkedData = _.groupBy(assetDetails.assetsLinked, function (asset) {
        return asset.assetType;
      });
      this.assetLinkedDetails = assetLinkedData;
      this.assetDetailsAfterLinkingUpdated = assetDetails;
    } else if (assetDetails.serviceAssets && assetDetails.serviceAssets.length > 0) {
      let serviceLinkedData: any;
      serviceLinkedData = _.groupBy(assetDetails.serviceAssets, function (service) {
        return service.serviceType;
      });
      this.assetLinkedDetails = serviceLinkedData;
      this.assetDetailsAfterLinkingUpdated = assetDetails;
    } else {
      this.assetLinkedDetails = {};
      this.assetDetailsAfterLinkingUpdated = assetDetails;
    }
  }

  getAllServersStats(allAssetArray?: any) {
    this.isShowAsset = true;
    let durationInMinutes: any = 5;
    let start= moment().subtract(durationInMinutes, 'minutes').utc();
    let end = moment().utc();
    this.healthCheckService.getAssetStatus(start, end)
      .subscribe(data => {
        this.isShowAsset = false;
        this.allServersStatsArray = data;
        if (allAssetArray && allAssetArray.length > 0) {
          this.allAssets = this.checkServerStatus(allAssetArray);
        }
      });
  }

  getServicesHealth() {
    let durationInMinutes: any = 1;
    let start= moment().subtract(durationInMinutes, 'minutes').utc();
    let end = moment().utc();
    this.serviceLinkableService.getServicesHealth(start, end)
      .subscribe(allServicesStatus => {
        this.allServicesStatus = allServicesStatus;
      });
  }

  checkAssetTypeToDisplayStatus(assetsList) {
    let serviceLinkableAssets = this.groupAllServiceLinkableAssets(this.assetConfigDetails);
    assetsList = _.map(assetsList, function (asset) {
      if (serviceLinkableAssets[asset.assetType]) {
        asset['showStatus'] = true;
      } else {
        asset['showStatus'] = false;
      }
      return asset;
    });

    return assetsList;
  }

  checkServerStatus(assetsList) {
    let serverStatusGroupById: any;
    serverStatusGroupById = _.groupBy(this.allServersStatsArray, '_id');
    assetsList = _.map(assetsList, function(data) {
      if (serverStatusGroupById[data._id]) {
        if ((serverStatusGroupById[data._id][0]._id) === data._id) {
          data['running'] = true;
          data['stats'] = serverStatusGroupById[data._id][0];
        }
      }
      return data;
    });
    return assetsList;
  }

  checkUpdatedServiceLinkableAssetStatus(asset) {
    let durationInMinutes: any = 5;
    let start= moment().subtract(durationInMinutes, 'minutes').utc();
    let end = moment().utc();
    this.healthCheckService.getAssetStatus(start, end)
      .subscribe(data => {
        this.allServersStatsArray = data;
        let serverStatusGroupById: any;
        serverStatusGroupById = _.groupBy(this.allServersStatsArray, '_id');
        if (serverStatusGroupById[asset._id]) {
          if ((serverStatusGroupById[asset._id][0]._id) === asset._id) {
            asset['running'] = true;
            asset['stats'] = serverStatusGroupById[asset._id][0];
          }
        }
        this.upadtedCartSpecificDetails = asset;
      });
  }

  checkForServiceLinkableAssetTypeInAssetList(assetsList) {
    let serviceLinkableAssets: any;
    serviceLinkableAssets = this.groupAllServiceLinkableAssets(this.assetConfigDetails);
    for (let i = 0; i < assetsList.length; i++) {
      if (serviceLinkableAssets[assetsList[i].assetType]) {
        return true;
      }
    }
    return false;
  }

  confirmedDelink(status) {
    if (status === 'OK') {
      let primaryAsset: any;
      let assetsToBeDeLinked : any = [];
      primaryAsset = this.assetId;
      assetsToBeDeLinked.push(this.delinkAssetDetails._id);
      let obj = {};
      obj = {
        'primaryAsset': primaryAsset,
        'assetsToBeDeLinked': assetsToBeDeLinked,
      };
      this.assetService.delinkAsset(obj)
        .subscribe(res => {
          this.linkDelinkAction = true;
          this.isServiceLinkDelinkAction = false;
          this.getAssetLinked(this.assetId);
        });
    }
  }

  confirmedServiceDelinkFromAsset(status) {
    if (status === 'OK') {
      let serviceAssetIds : any = [];
      let gatewayId: string = '';
      serviceAssetIds.push(this.delinkServiceDetails._id);
      gatewayId = this.assetId;
      let obj = {};
      obj = {
        'serviceAssetIds': serviceAssetIds,
        'gatewayId': gatewayId
      };
      this.serviceLinkableService.delinkAsset(obj)
        .subscribe((assetDetailsAfterServiceDelink: any) => {
          this.isServiceLinkDelinkAction = true;
          this.linkDelinkAction = false;
          this.assetData = assetDetailsAfterServiceDelink;
          this.getAssetsLinkedDetails(assetDetailsAfterServiceDelink);
        });
    }
  }

  confirmedlink(status) {
    if (status === 'OK') {
      let primaryAsset: any;
      let assetsToBeLinked: any = [];
      primaryAsset = this.assetId;
      assetsToBeLinked = this.linkAssetDetails;
      let obj = {};
      obj = {
        'primaryAsset': primaryAsset,
        'assetsToBeLinked': assetsToBeLinked,
      };
      this.assetService.linkAsset(obj)
        .subscribe(res => {
          this.linkDelinkAction = true;
          this.isServiceLinkDelinkAction = false;
          this.getAssetLinked(this.assetId);
          $('#assetListingPopup').hide(300);
        });
    }
  }

  confirmedDeregisteringAsset(status) {
    this.isAssetDeregisteredSuccess = false;
    if (status === 'OK') {
      this.assetService.deregisterAsset(this.deregisterSelectedAssetId)
        .subscribe(res => {
          this.isAssetDeregisteredSuccess = true;
          //this.selectedAssets = [];
          //this.getAssetTotalCountForPagination();
          //this.getFirstFewAssets();
          this.getAssetForSelectedAssetTypes(0, this.pagination.recordsPerPage);
          this.getAssetIndividualCounts();
          $('#sidebar-right').modal('hide');
          $('html').css('overflow-y', 'auto');
          this.showMessagePopup('deregisterAssetSuccessfully');
          this.resetFormData = Math.random();
        });
    }
  }
  confirmedServicelinkToAsset(status) {
    if (status === 'OK') {
      let serviceAssetIds : any = [];
      let gatewayId: string = '';
      serviceAssetIds = this.serviceIdsToLinkAsset;
      gatewayId = this.assetId;
      let obj = {};
      obj = {
        'serviceAssetIds': serviceAssetIds,
        'gatewayId': gatewayId
      };
      this.serviceLinkableService.linkAsset(obj)
        .subscribe((assetDetailsAfterServiceLink: any) => {
          this.isServiceLinkDelinkAction = true;
          this.linkDelinkAction = false;
          this.assetData = assetDetailsAfterServiceLink;
          this.getAssetsLinkedDetails(assetDetailsAfterServiceLink);
          $('#serviceListingPopup').hide(300);
        });
    }
  }
  delinkAsset(delinkAssetDetails) {
    this.delinkAssetDetails = delinkAssetDetails;
  }

  delinkService(delinkServiceDetails) {
    this.delinkServiceDetails = delinkServiceDetails;
  }

  linkAsset(linkAssetsIdsArray) {
    this.linkAssetDetails = linkAssetsIdsArray;
    this.selectedAssetToLinkCount = this.linkAssetDetails.length;
  }

  linkServicesToAsset(servicesIdsArray) {
    this.serviceIdsToLinkAsset = servicesIdsArray;
    this.selectedAssetToLinkCount = this.serviceIdsToLinkAsset.length;
  }

  deregisterAsset(assetId) {
    this.deregisterSelectedAssetId = assetId;
  }

  assetTypeToAdd(assetType) {
    this.assetSelectedType = assetType;
    this.resetAddAssetFormValue = Math.random();
  }

  getDataByPageNo(page) {
    this.pageNumber = page.pageIndex + 1;
    if (page.pageIndex === 0) {
      this.skip = 0;
      this.limit = this.pagination.recordsPerPage;
    } else {
      this.skip = (page.pageIndex) * this.pagination.recordsPerPage;
      this.limit = this.pagination.recordsPerPage;
    }
    /*if(page==1){
     this.skip=page-1;
     this.limit=page*this.pagination.recordsPerPage;
     }else{
     this.skip=(page-1)*this.pagination.recordsPerPage;
     this.limit=this.pagination.recordsPerPage;
     }*/
    if (this.searchForAsset && this.searchOperationDone) {
      this.setAssetListByTypesOnSearch(this.searchForAsset, this.selectedAssets.toString(), this.skip, this.limit);
    }
    if(this.selectedAssets.length>0 && !this.searchOperationDone){
      this.setAssetListByTypes(this.selectedAssets.toString(), this.skip, this.limit);
    }
    if(this.selectedAssets.length==0 && !this.searchOperationDone){
      this.setAssetListByTypes(null, this.skip, this.limit);
    }
  }

  setSearchingPlaceholder(selectedAssets) {
      if ( selectedAssets.length === 0 ) {
        this.searchPlaceholder = 'Search all';
      } else {
        this.searchPlaceholder = 'Search ' + selectedAssets.toString();
      }
     this.searchService.sendSearchPlaceholderValue(this.searchPlaceholder);
  }

  showMessagePopup(popup) {
    $('.' + popup).fadeIn(300);
    setTimeout(() => {
      $('.' + popup).fadeOut(300);
    }, 3000);
  }

  stopScroll() {
    $('.carousel').carousel({
      pause: true,
      interval: false
    });
  }

  public updatePageNumber(index: number): void {
    this.pageNumber = index;
    this.paginator.pageIndex = index - 1;
    this.paginator.page.next({
      pageIndex: this.paginator.pageIndex,
      pageSize: this.paginator.pageSize,
      length: this.paginator.length
    });
  }
}
