import { Component, OnInit, OnDestroy } from '@angular/core';
import {AssetService} from "../../services/asset.service";
import {NearableService} from "../../services/nearable.service";
import {AssetGroupService} from "../../services/asset.group.service";
import {AssetConfigService} from '../../services/asset-config.service';
import {ExceptionService} from '../../services/exception.service';
import { Subscription } from 'rxjs';
import {SearchService} from '../../services/search.service';
import {IotzenPillType} from '../core/iotzen-pills/iotzen-pill-type';
import * as _ from "lodash";
import {AssetCountType, AssetTypes, AssetTypeWithCount, Count} from "../asset-list/asset-count-type";
declare var $ :any;
import { Subject,Actions} from '../../auth/rules';


@Component({
  selector: 'app-nearable-component',
  templateUrl: './nearable-component.component.html',
  styleUrls: ['./nearable-component.component.scss']
})
export class NearableComponentComponent implements OnInit, OnDestroy {

  public assetTypes = ['rfId', 'beacon', 'all'];
  public assetStatusTypes = [true, false];
  public allAssets;
  public assetTypesWithLabels: any = [];
  public lastRefreshed: any = new Date();
  public selectedAssets: any = [];
  public selectedNearableAssets: any = [];
  constructor(public nearableService: NearableService,
              public assetService: AssetService,
              public assetConfigService: AssetConfigService,
              public exceptionService: ExceptionService,
              public assetGroupService: AssetGroupService,
              private searchService: SearchService) {
    this.isSelectAll = false;
    this.nearByAssets = [];
    this.searchPlaceholder = 'Search Beacon, RfIdTag';
    this.searchForBeaconAndRftag = '';
    this.isAssetDeregisteredSuccess = false;
  }
  p: number = 1;
  public selectedPageCount:any;
  public SCAN_NEAR_BY_DURATION: 1;
  public typeRadio: string = 'all';
  public featureRadio: string = 'all';
  public nearByAssetsToBeSaved:any=[];
  public assetRegisterErrorMessages: any = [];
  public arrayOfTypePills: Array<IotzenPillType> = [];
  public arrayOfStatusPills: Array<IotzenPillType> = [];
  public allGroups: any = [];
  public groupSelected: string;
  public isShowAsset: boolean;
  public isNearByAssetFound: boolean = true;
  public showSlider: boolean;
  public registeredAssetDetailsToEdit: any;
  public linkDelinkAction: boolean = false;
  public assetLinkedDetails: any;
  public typeSelected: string;
  public assetStatus: string;
  public isFilterApplied: boolean;
  public  assetConfigDetails: any;
  public nearByAssets: any;
  public  selectedAll: any;
  public linkAssetDetails: any;
  public  delinkAssetDetails: any;
  public  upadtedCartSpecificDetails: any;
  public  deregisterSelectedAssetId: any;
  public selectedAssetToLinkCount: number;
  public assetDetailsAfterLinkingUpdated: any;
  public searchForBeaconAndRftag: string;
  private searchPlaceholder: string;
  private searchTextSub: Subscription;
  public isAssetDeregisteredSuccess: boolean;
  isSelectAll: boolean;

  SUBJECT = Subject;
  ACTIONS = Actions;
  ngOnInit() {
    this.isShowAsset = true;
    this.doSearchTextResetAndOtherOperations();
    this.getAllAssetGroups();
    this.getNearByAssets();
    this.buildPillsArray();
    this.assetConfigService.getAssetsConfig()
      .then(assetConfig => {
        this.assetConfigDetails = assetConfig;
      });
    this.searchTextSub = this.searchService.searchText.subscribe(event => {
      this.searchForBeaconAndRftag = event;
    });
  }

  ngOnDestroy() {
    this.searchTextSub.unsubscribe();
  }

  doSearchTextResetAndOtherOperations() {
    this.searchService.showSearchBar(true);
    this.searchService.resetSearchTextInSearchBox();
    this.searchService.sendSearchText(''); // required when front-end filter is applied on search operation
    this.searchService.sendSearchPlaceholderValue(this.searchPlaceholder);
  }

  /*Select All Assets*/
  selectAllAssets(){
    let type = this.typeRadio;
    this.nearByAssetsToBeSaved = [];
    this.nearByAssetsToBeSaved = _.map(this.allAssets, function(assetObj) {
        if (type === assetObj.assetData.assetType && !assetObj.isRegistered) {
          return assetObj.assetData;
        }
        if (!type || (type === 'all' && !assetObj.isRegistered)) {
          return assetObj.assetData;
        }
    }) ;
    this.allAssets = _.map(this.allAssets, function(assetObj) {
      if (type === assetObj.assetData.assetType && !assetObj.isRegistered) {
        assetObj.embossedFlag = true;
      }
      if (!type || (type === 'all' && !assetObj.isRegistered)){
        assetObj.embossedFlag = true;
      }
      return assetObj;
    });
  }

  buildPillsArray() {
   this.arrayOfTypePills = [{id: 'typeAll', label: 'All', description: 'Selected Type All'},
     {id: 'beacon', label: 'Beacon', description: 'Selected Type Beacon'},
     {id: 'rfId', label: 'RF Tag', description: 'Selected Type RF Tag'}];
   this.arrayOfStatusPills = [{id: 'statusAll', label: 'All', description: 'Status All'},
     {id: 'registered', label: 'Registered', description: 'Status Registered'},
     {id: 'notRegistered', label: 'Not Registered', description: 'Status Not Registered'}];
  }
  getActivePillValue(value) {
    switch (value) {
      case 'typeAll':
        this.typeRadio = 'all';
        break;
      case 'beacon':
        this.typeRadio = 'beacon';
        break;
      case 'rfId':
        this.typeRadio = 'rfId';
        break;
      case 'statusAll':
        this.featureRadio = 'all';
        break;
      case 'registered':
        this.featureRadio = 'true';
        break;
      case 'notRegistered':
        this.featureRadio = 'false';
        break;
      default:
        break;
    }
    this.getNearByAssets();
  }

  getNearByAssets() {
    this.isFilterApplied = false;
    this.isShowAsset = true;
    this.allAssets = [];
    this.nearByAssetsToBeSaved = [];
    this.isNearByAssetFound = true;
    this.lastRefreshed = new Date();
    this.clearSelectedGroup();
    this.typeSelected = this.typeRadio;
    this.assetStatus = this.featureRadio;
    let typeSelected = this.typeRadio;
    let status = this.featureRadio;
    if (!typeSelected || typeSelected == 'all') {
      typeSelected = undefined;
    } else {
      this.isFilterApplied = true;
    }
    if (!status || status == 'all') {
      status = undefined;
    } else {
      this.isFilterApplied = true;
    }
    if ((!typeSelected || typeSelected == 'all') && (!status || status == 'all')) {
      this.isFilterApplied = false;
    }
    this.nearableService.getNearby(typeSelected, status, this.SCAN_NEAR_BY_DURATION)
      .subscribe(nearbyAssets => {
        this.modifyCartData(nearbyAssets);
        this.isShowAsset = false;
        let nearByAssetFound: any;
        nearByAssetFound =  nearbyAssets;
        if (nearByAssetFound.length === 0) {
          this.isNearByAssetFound = false;
        }
      });
  }
  modifyCartData(nearbyAssets){
    let modifiedArr = [];
    _.forEach(nearbyAssets, function(value) {
      let assetName = value['assetType'];
      let obj = {assetData: {}};
      obj["isRegistered"] = value.isRegistered;
      obj["lastSeen"] = value.createdInMilliSeconds;
      obj.assetData["assetType"] = assetName;
      obj.assetData['groups'] = value.groups;
      if (value.assetsLinked) {
        obj['assetsLinked'] = value.assetsLinked;
      }
      if (value._id) {
        obj['_id'] = value._id;
      }
      if (assetName === 'beacon') {
        obj.assetData["beaconType"] = value.beaconType;
        obj.assetData["beaconId"] = value.beaconId;
        obj.assetData["rssi"] = value.rssi;
      }
      if (assetName === 'rfId') {
        obj.assetData["RFIDId"] = value.RFIDId;
      }
      modifiedArr.push(obj);
    });
    this.isShowAsset = false;
    this.allAssets = modifiedArr;
    this.nearByAssets = modifiedArr;
    this.unSelectAllAssets();
  }

  unSelectAllAssets() {
    $('#selectAllCheckbox').prop('checked', false);
    for (let i = 0; i < this.nearByAssets.length; i++) {
      if (this.nearByAssets[i].isRegistered === false) {
        this.nearByAssets[i].selected = false;
      }
    }
    this.isSelectAll = false;
  }

  /*Called from app-asset-card-nearable to save selected nearby*/
  assetSelectedToSave(data) {
    this.showSlider = false;
    if (data.assetData.assetType === 'beacon' && this.nearByAssetsToBeSaved.length > 0) {
      _.remove(this.nearByAssetsToBeSaved, {'beaconId': data.assetData.beaconId});
    } else if (data.assetData.assetType === 'rfId' && this.nearByAssetsToBeSaved.length > 0) {
      _.remove(this.nearByAssetsToBeSaved, {'RFIDId': data.assetData.RFIDId});
    }
    if (data.embossedFlag) {
      this.nearByAssetsToBeSaved.push(data.assetData);
    }
  }
  saveRegisterAsset() {
    this.assetRegisterErrorMessages = [];
    this.assetService.saveMultipleAssetDetails(this.nearByAssetsToBeSaved)
      .subscribe((savedAssets: any) => {
      if ( savedAssets && savedAssets.saved && savedAssets.saved.length > 0 ) {
        this.linkAssetToGroup(savedAssets.saved, this.groupSelected);
      }
        if ( savedAssets && savedAssets.error && savedAssets.error.length > 0 ) {
          this.assetRegisterErrorMessages = savedAssets.error;
          document.getElementById('assetRegisterErrorMsgBtn').click();
        }
      });
  }

  /*get all groups*/
  getAllAssetGroups() {
    this.assetGroupService.get()
    .subscribe(allGroups => {
      this.allGroups = allGroups;
    });
  }
  clearSelectedGroup() {
    this.groupSelected = null;
  }
  skipGroupSelection() {
    this.saveRegisterAsset();
  }
  linkAssetToGroup(assetsList, groupId) {
    if (groupId) {
      assetsList = _.map(assetsList, function(assetObj) {return assetObj._id});
      this.assetGroupService.link(assetsList, groupId)
        .subscribe(linkedAssets => {
          this.getNearByAssets();
        });
    } else {
      this.getNearByAssets();
    }
  }

  registeredAssetDataToEdit(registeredAssetDetails) {
    //this.registeredAssetDetailsToEdit = registeredAssetDetails.assetData;
    this.linkDelinkAction = false;
    this.showSlider = true;
    this.getAssetLinked(registeredAssetDetails._id);
  }

  getAssetLinked(assetId) {
    this.assetService.getAssetDetailsByMongodbId(assetId)
      .subscribe(assetDetails => {
        this.registeredAssetDetailsToEdit = assetDetails;
        this.getAssetsLinkedDetails(assetDetails);
      });
  }

  getAssetsLinkedDetails(assetDetails) {
    let assetLinkedData: any;
    assetLinkedData = _.groupBy(assetDetails.assetsLinked, function (asset) {
      return asset.assetType;
    });
    this.assetLinkedDetails = assetLinkedData;
    this.assetDetailsAfterLinkingUpdated = assetDetails;
  }

  linkAsset(linkingAssetDetails) {
    this.linkAssetDetails = linkingAssetDetails;
    this.selectedAssetToLinkCount = this.linkAssetDetails.length;
  }

  delinkAsset(delinkAssetDetails) {
    this.delinkAssetDetails = delinkAssetDetails;
  }

  syncUpdateAssetCart(dataUpdated) {
    this.upadtedCartSpecificDetails = dataUpdated;
    this.showMessagePopup('updatedAssetSuccessfully');
  }

  deregisterAsset(assetId) {
    this.deregisterSelectedAssetId = assetId;
  }

  confirmedlink(status) {
    if (status === 'OK') {
      let primaryAsset: any;
      let assetsToBeLinked: any = [];
      primaryAsset = this.registeredAssetDetailsToEdit._id;
      assetsToBeLinked = this.linkAssetDetails;
      let obj = {};
      obj = {
        'primaryAsset': primaryAsset,
        'assetsToBeLinked': assetsToBeLinked,
      };
      this.assetService.linkAsset(obj)
        .subscribe(res => {
          this.linkDelinkAction = true;
          this.getAssetLinked(this.registeredAssetDetailsToEdit._id);
          $('#assetListingPopup').hide(300);
        });
    }
  }

  confirmedDelink(status) {
    if (status === 'OK') {
      let primaryAsset: any;
      let assetsToBeDeLinked : any = [];
      primaryAsset = this.registeredAssetDetailsToEdit._id;
      assetsToBeDeLinked.push(this.delinkAssetDetails._id);
      let obj = {};
      obj = {
        'primaryAsset': primaryAsset,
        'assetsToBeDeLinked': assetsToBeDeLinked,
      };
      this.assetService.delinkAsset(obj)
        .subscribe(res => {
          this.linkDelinkAction = true;
          this.getAssetLinked(this.registeredAssetDetailsToEdit._id);
        });
    }
  }

  confirmedDeregisteringAsset(status) {
    this.isAssetDeregisteredSuccess = false;
    if (status === 'OK') {
      this.assetService.deregisterAsset(this.deregisterSelectedAssetId)
        .subscribe(res => {
          this.isAssetDeregisteredSuccess = true;
          this.getNearByAssets();
          $('#sidebar-right').modal('hide');
          $('html').css('overflow-y', 'auto');
          this.showMessagePopup('deregisterAssetSuccessfully');
        });
    }
  }

  confirmedRegisteringExceptionBeacons(status) {
    if (status === 'OK') {
      let selectedExceptionIdsArray;
      selectedExceptionIdsArray = [];
      if (this.nearByAssetsToBeSaved && this.nearByAssetsToBeSaved.length > 0) {
        _.forEach(this.nearByAssetsToBeSaved, function(asset) {
          selectedExceptionIdsArray.push(asset.beaconId);
        });
      }
      let obj = {};
      obj = {
        'id': selectedExceptionIdsArray
      };
      this.exceptionService.saveBeaconException(obj)
        .subscribe((exceptionBeaconAssets: any) => {
          this.getNearByAssets();
        });
    }
  }

  showMessagePopup(popup) {
    $('.' + popup).fadeIn(300);
    setTimeout(() => {
      $('.' + popup).fadeOut(300);
    }, 3000);
  }

  selectAll() {
    this.isSelectAll = !this.isSelectAll;
    for (let i = 0; i < this.nearByAssets.length; i++) {
      if (this.nearByAssets[i].isRegistered === false) {
        this.nearByAssets[i].selected = this.selectedAll;
        if (this.isSelectAll) {
          if (this.nearByAssets[i].assetData.assetType === 'beacon' && this.nearByAssetsToBeSaved.length > 0) {
            _.remove(this.nearByAssetsToBeSaved, {'beaconId': this.nearByAssets[i].assetData.beaconId});
          } else if (this.nearByAssets[i].assetData.assetType === 'rfId' && this.nearByAssetsToBeSaved.length > 0) {
            _.remove(this.nearByAssetsToBeSaved, {'RFIDId': this.nearByAssets[i].assetData.RFIDId});
          }
          this.nearByAssetsToBeSaved.push(this.nearByAssets[i].assetData);
        } else {
          this.nearByAssetsToBeSaved = [];
        }
      }
    }
    //this.allAssets = [];
    this.allAssets = this.nearByAssets;
  }
}
