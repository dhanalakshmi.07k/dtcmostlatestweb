import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearableComponentComponent } from './nearable-component.component';

describe('NearableComponentComponent', () => {
  let component: NearableComponentComponent;
  let fixture: ComponentFixture<NearableComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NearableComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearableComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
