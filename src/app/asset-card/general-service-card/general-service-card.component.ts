import { Component, OnInit, Input } from '@angular/core';

import * as _ from 'lodash';
declare let $: any;

@Component({
  selector: 'app-general-service-card',
  templateUrl: './general-service-card.component.html',
  styleUrls: ['./general-service-card.component.scss']
})
/*export class GeneralServiceCardComponent implements OnChanges, OnInit {
  public id: any;
  public assetLinkedDetails: any;
  public assetLabel: string;
  public embossedFlag: boolean;
  @Input() isCardClickable: boolean;
  @Input() isLinkable: boolean;
  @Input() assetConfigDetails: any;
  @Input() assetDetailsAfterLinkingUpdated: any;
  @Input() assetData: any;
  @Input() upadtedCartSpecificDetails: any;
  @Input() showGroupBtn: boolean;
  @Input() showCloseBtn: boolean;
  @Input() showLinkBtn: boolean;
  @Input() isAssetDetailsInReadMode: boolean;
  @Input() isCardHeightFixed: boolean;
  @Input() resetFormData: number;
  @Output() editAsset: EventEmitter<any> = new EventEmitter();
  @Output() delinkAssetDetails: EventEmitter<any> = new EventEmitter();
  @Output() linkAssetDetails: EventEmitter<any> = new EventEmitter();
  constructor() {
    this.embossedFlag = false;
    this.isCardClickable = false;
    this.isLinkable = false;
    this.isAssetDetailsInReadMode = false;
  }
  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'assetData') {
        if (change.currentValue !== change.previousValue) {
         // this.getAssetsLinkedDetails(this.assetData);
          if (this.assetConfigDetails) {
            console.log('assetConfigDetails', this.assetConfigDetails);
           // this.getAssetLabel();
          }
        }
      }
      if (propName === 'resetFormData') {
        if (change.currentValue !== change.previousValue) {
          this.assetData = {};
          this.assetLinkedDetails = {};
          this.assetLabel = '';
        }
      }
    }
    if (this.assetDetailsAfterLinkingUpdated) {
    //  this.checkToUpdatedAssetLinkCount();
    }
    if (this.upadtedCartSpecificDetails) {
      if (this.assetData._id === this.upadtedCartSpecificDetails._id) {
        this.assetData = this.upadtedCartSpecificDetails;
      }
    }
  }


  checkToUpdatedAssetLinkCount() {
    if (this.assetData._id === this.assetDetailsAfterLinkingUpdated._id) {
      this.getAssetsLinkedDetails(this.assetDetailsAfterLinkingUpdated);
    }
  }

  editAssetDetails() {
    this.id = this.assetData._id;
    this.editAsset.emit(this.assetData);
    $('html').css('overflow-y', 'hidden');
  }

  closeBtnClicked() {
    this.delinkAssetDetails.emit(this.assetData);
  }

  getAssetsLinkedDetails(assetDetails) {
    let assetLinkedData: any;
    assetLinkedData = _.groupBy(assetDetails.assetsLinked, function (asset) {
      return asset.assetType;
    });
    this.assetLinkedDetails = assetLinkedData;
  }

  getAssetLabel() {
    let assetDetails =  _.find(this.assetConfigDetails, ['type', this.assetData.serviceType]);
    this.assetLabel = assetDetails.label;
  }

  getBoxShadowForCard(type) {
    let assetConfig = _.find(this.assetConfigDetails, ['type', type]);
    switch (type) {
      case type:
        return '0 2px 5px 0' + assetConfig.theme.color.primary, '0 2px 10px 0' + assetConfig.theme.color.primary;
    }
  }

  selectedAssetsForLinking() {
    if (this.isLinkable) {
      this.embossedFlag = !this.embossedFlag;
      let obj = {};
      obj['isAssetSelected'] = this.embossedFlag;
      obj['assetData'] = this.assetData;
      this.linkAssetDetails.emit(obj);
    }
  }

}*/

export class GeneralServiceCardComponent implements  OnInit {
  @Input() assetData: any;
  constructor() { }

  ngOnInit() {
  }
}
