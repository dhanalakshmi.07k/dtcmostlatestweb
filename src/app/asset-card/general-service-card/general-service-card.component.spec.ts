import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralServiceCardComponent } from './general-service-card.component';

describe('GeneralServiceCardComponent', () => {
  let component: GeneralServiceCardComponent;
  let fixture: ComponentFixture<GeneralServiceCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralServiceCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralServiceCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
