import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-driver-card',
  templateUrl: './driver-card.component.html',
  styleUrls: ['./driver-card.component.scss']
})
export class DriverCardComponent implements OnChanges, OnInit {
  @Input() assetData: any;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
  }

}
