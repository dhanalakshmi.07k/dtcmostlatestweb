import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-obd-card',
  templateUrl: './obd-card.component.html',
  styleUrls: ['./obd-card.component.scss']
})
export class ObdCardComponent implements OnInit {
  @Input() assetData: any;

  constructor() { }
  ngOnInit() {
  }

}
