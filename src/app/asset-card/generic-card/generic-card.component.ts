import {Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-generic-card',
  templateUrl: './generic-card.component.html',
  styleUrls: ['./generic-card.component.scss']
})
export class GenericCardComponent implements OnChanges, OnInit {
  @Input() assetData: any;
  @Input() assetConfigDetails: any;
  @Input() config: any;
  @Input() resetFormData: any;
  public  fieldsToBeDisplayed: any = [];
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'assetData') {
        if (change.currentValue !== change.previousValue) {
          let groupByAssetType = _.groupBy(this.config, 'type');
          let type = null;
          if ( this.assetData.assetType ) {
            type = this.assetData.assetType;
          } else if ( this.assetData.serviceType ) {
            type = this.assetData.serviceType;
          }
          if (groupByAssetType[type] && groupByAssetType[type][0] && groupByAssetType[type][0]['configuration']) {
            this.fieldsToBeDisplayed = _.groupBy(groupByAssetType[type][0]['configuration'], 'showInCard')['true'];
          }
        }
      }
      if (propName === 'resetFormData') {
        if (change.currentValue !== change.previousValue) {
          this.assetData = {};
          this.fieldsToBeDisplayed = [];
        }
      }
    }
  }

}
