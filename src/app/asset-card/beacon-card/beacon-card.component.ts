import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-beacon-card',
  templateUrl: './beacon-card.component.html',
  styleUrls: ['./beacon-card.component.scss']
})
export class BeaconCardComponent implements OnInit {
  constructor() { }
  @Input() assetData: any;

  ngOnInit() {
  }
}
