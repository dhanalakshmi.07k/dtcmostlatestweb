import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfTagCardComponent } from './rf-tag-card.component';

describe('RfTagCardComponent', () => {
  let component: RfTagCardComponent;
  let fixture: ComponentFixture<RfTagCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfTagCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfTagCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
