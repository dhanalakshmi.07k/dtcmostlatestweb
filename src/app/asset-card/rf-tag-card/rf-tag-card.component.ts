import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-rf-tag-card',
  templateUrl: './rf-tag-card.component.html',
  styleUrls: ['./rf-tag-card.component.scss']
})
export class RfTagCardComponent implements OnInit {
  @Input() assetData: any;

  constructor() {
  }

  ngOnInit() {
  }
}
