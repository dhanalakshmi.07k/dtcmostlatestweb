import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-beacon-gateway-card',
  templateUrl: './beacon-gateway-card.component.html',
  styleUrls: ['./beacon-gateway-card.component.scss']
})
export class BeaconGatewayCardComponent implements OnInit {

  constructor() { }
  @Input() assetData: any;

  ngOnInit() {
  }

}
