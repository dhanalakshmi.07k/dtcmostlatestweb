import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeaconGatewayCardComponent } from './beacon-gateway-card.component';

describe('BeaconGatewayCardComponent', () => {
  let component: BeaconGatewayCardComponent;
  let fixture: ComponentFixture<BeaconGatewayCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeaconGatewayCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeaconGatewayCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
