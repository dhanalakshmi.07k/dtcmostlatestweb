import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import * as _ from 'lodash';
declare let $: any;
import { Subject,Actions} from '../../auth/rules';

@Component({
  selector: 'app-asset-card',
  templateUrl: './asset-card.component.html',
  styleUrls: ['./asset-card.component.scss']
})
export class AssetCardComponent implements OnChanges, OnInit {
  public id: any;
  public assetLinkedDetails: any;
  public assetLabel: string;
  public serviceLabel: string;
  public embossedFlag: boolean;
  @Input() isCardClickable: boolean;
  @Input() showStatusOnAssetCard: boolean;
  @Input() isLinkable: boolean;
  @Input() assetConfigDetails: any;
  @Input() serviceAssetConfigDetails: any;
  @Input() assetDetailsAfterLinkingUpdated: any;
  @Input() assetData: any;
  @Input() upadtedCartSpecificDetails: any;
  @Input() updatedIndividualSericeStatus: any;
  @Input() showGroupBtn: boolean;
  @Input() showCloseBtn: boolean;
  @Input() showDelinkServiceBtn: boolean;
  @Input() showLinkBtn: boolean;
  @Input() isAssetDetailsInReadMode: boolean;
  @Input() isCardHeightFixed: boolean;
  @Input() resetFormData: number;
  @Output() editAsset: EventEmitter<any> = new EventEmitter();
  @Output() delinkAssetDetails: EventEmitter<any> = new EventEmitter();
  @Output() delinkServiceDetails: EventEmitter<any> = new EventEmitter();
  @Output() linkAssetDetails: EventEmitter<any> = new EventEmitter();
  @Input() config: any;
  SUBJECT=Subject;
  ACTIONS=Actions;
  constructor() {
    this.embossedFlag = false;
    this.isCardClickable = false;
    this.showStatusOnAssetCard = false;
    this.isLinkable = false;
    this.isAssetDetailsInReadMode = false;
    this.assetLinkedDetails = {};
    this.showDelinkServiceBtn = false;
   // this.serviceAssetConfigDetails = [];
   // this.assetConfigDetails = [];
  }
  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'assetData') {
        if (change.currentValue !== change.previousValue) {
          this.embossedFlag = false;
          //this.getAssetsLinkedDetails(this.assetData);
          if (this.assetData && this.assetData.assetType && this.assetConfigDetails) {
            if (this.assetData.serviceAssets) {
              this.getServicesLinkedForAssetDetails(this.assetData.serviceAssets);
            } else {
              this.getAssetsLinkedDetails(this.assetData);
            }
            this.getAssetLabel();
          } else if (this.assetData && this.assetData.serviceType) {
            this.getAssetLinkedForService(this.assetData);
            this.getServiceAssetLabel();
          }
        }
      }
      if (propName === 'resetFormData') {
        if (change.currentValue !== change.previousValue) {
          this.assetLinkedDetails = {};
          this.assetLabel = '';
          this.serviceLabel = '';
        }
      }
      if (propName === 'upadtedCartSpecificDetails') {
        if (change.currentValue !== change.previousValue) {
          if (this.assetData._id === this.upadtedCartSpecificDetails._id) {
            this.assetData = this.upadtedCartSpecificDetails;
          }
        }
      }
      if (propName === 'updatedIndividualSericeStatus') {
        if (change.currentValue !== change.previousValue) {
          if (this.assetData._id === this.updatedIndividualSericeStatus.id) {
            this.assetData['running'] = this.updatedIndividualSericeStatus['running'];
          }
        }
      }
    }
    if (this.assetDetailsAfterLinkingUpdated) {
      this.checkToUpdatedAssetLinkCount();
    }
    /*if (this.upadtedCartSpecificDetails) {
      if (this.assetData._id === this.upadtedCartSpecificDetails._id) {
        console.log('this.assetData', this.assetData);
        console.log('this.upadtedCartSpecificDetails', this.upadtedCartSpecificDetails);
        this.assetData = this.upadtedCartSpecificDetails;
      }
    }*/
  }


  checkToUpdatedAssetLinkCount() {
    if (this.assetData._id === this.assetDetailsAfterLinkingUpdated._id) {
      if (this.assetData && this.assetData.assetType) {
        this.getAssetsLinkedDetails(this.assetDetailsAfterLinkingUpdated);
      } else if (this.assetData && this.assetData.serviceType) {
        this.getAssetLinkedForService(this.assetDetailsAfterLinkingUpdated);
      }
    }
  }

  editAssetDetails() {
    this.id = this.assetData._id;
    this.editAsset.emit(this.assetData);
    $('html').css('overflow-y', 'hidden');
  }

  closeBtnClicked() {
    this.delinkAssetDetails.emit(this.assetData);
  }

  delinkServiceBtnClicked() {
    this.delinkServiceDetails.emit(this.assetData);
  }

  /*linkBtnClicked() {
    this.linkAssetDetails.emit(this.assetData);
  }*/

  getAssetsLinkedDetails(assetData) {
    if (assetData.assetsLinked)  {
      let assetLinkedData: any;
      assetLinkedData = _.groupBy(assetData.assetsLinked, function (asset) {
        return asset.assetType;
      });
      this.assetLinkedDetails = assetLinkedData;
    } else if (assetData.serviceAssets) {
      let assetLinkedData: any;
      assetLinkedData = _.groupBy(assetData.serviceAssets, function (service) {
        return service.serviceType;
      });
      this.assetLinkedDetails = assetLinkedData;
    }
  }

  getServicesLinkedForAssetDetails(serviceLinkedToAsset) {
    if (serviceLinkedToAsset && serviceLinkedToAsset.length > 0) {
      let serviceLinkedData: any;
      serviceLinkedData = _.groupBy(serviceLinkedToAsset, function (service) {
        return service.serviceType;
      });
      this.assetLinkedDetails = serviceLinkedData;
    } else {
      this.assetLinkedDetails = {};
    }
  }

  getAssetLinkedForService(assetDetails) {
    if (assetDetails.gatewayAsset && assetDetails.gatewayAsset.assetType) {
      let obj: any = {};
      let propName: string = '';
      let array: any = [];
      propName = assetDetails.gatewayAsset.assetType;
      array.push(assetDetails.gatewayAsset);
      obj[propName] = array;
      this.assetLinkedDetails = obj;
    } else {
      this.assetLinkedDetails = {};
    }
  }

  getServiceAssetLabel() {
    /*this.serviceLabel = '';
    if (this.assetData && this.assetData.serviceType) {
      let assetDetails =  _.find(this.serviceAssetConfigDetails, ['type', this.assetData.serviceType]);
      this.serviceLabel = assetDetails.label;
    }*/
    this.serviceLabel = '';
    this.serviceLabel = this.assetData.name;
  }

  getAssetLabel() {
    this.assetLabel = '';
    if (this.assetData && this.assetData.assetType) {
      let assetDetails =  _.find(this.assetConfigDetails, ['type', this.assetData.assetType]);
      this.assetLabel = assetDetails.label;
    }
  }

  getBoxShadowForCard(assetData) {
    if (assetData && assetData.assetType) {
      let type = assetData.assetType;
      let assetConfig = _.find(this.assetConfigDetails, ['type', type]);
      switch (type) {
        case type:
          return '0 2px 5px 0' + assetConfig.theme.color.primary, '0 2px 10px 0' + assetConfig.theme.color.primary;
      }
    } else if (assetData && assetData.serviceType) {
      let type = assetData.serviceType;
      let assetConfig = _.find(this.serviceAssetConfigDetails, ['type', type]);
      switch (type) {
        case type:
          return '0 2px 5px 0' + assetConfig.theme.color.primary, '0 2px 10px 0' + assetConfig.theme.color.primary;
      }
    }
  }

  selectedAssetsForLinking() {
    if (this.isLinkable) {
      this.embossedFlag = !this.embossedFlag;
      let obj = {};
      obj['isAssetSelected'] = this.embossedFlag;
      obj['assetData'] = this.assetData;
      this.linkAssetDetails.emit(obj);
    }
  }

}
