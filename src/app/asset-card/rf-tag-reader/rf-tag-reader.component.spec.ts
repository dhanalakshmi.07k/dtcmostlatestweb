import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfTagReaderComponent } from './rf-tag-reader.component';

describe('RfTagReaderComponent', () => {
  let component: RfTagReaderComponent;
  let fixture: ComponentFixture<RfTagReaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfTagReaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfTagReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
