import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-rf-tag-reader',
  templateUrl: './rf-tag-reader.component.html',
  styleUrls: ['./rf-tag-reader.component.scss']
})
export class RfTagReaderComponent implements OnInit {
  @Input() assetData: any;
  constructor() { }

  ngOnInit() {
  }

}
