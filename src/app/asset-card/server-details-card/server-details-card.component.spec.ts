import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerDetailsCardComponent } from './server-details-card.component';

describe('ServerDetailsCardComponent', () => {
  let component: ServerDetailsCardComponent;
  let fixture: ComponentFixture<ServerDetailsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerDetailsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerDetailsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
