import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { CountCardComponent } from './count-card/count-card.component';
import { AssetListComponent } from './asset-list/asset-list.component';
import { SideMenuComponent } from './core/side-menu/side-menu.component';
import { HeaderComponent } from './core/header/header.component';
import { AssetService } from '../services/asset.service';
import { ServiceLinkableService } from '../services/serviceLinkable.service';
import { LoginComponent } from './login/login.component';
import { LoginHeaderComponent } from './core/login-header/login-header.component';
import { AssetFormComponent } from './asset-form/asset-form.component';
import {FormService} from '../services/form.service';
import { AssetCardComponent } from './asset-card/asset-card.component';
import { BeaconCardComponent } from './asset-card/beacon-card/beacon-card.component';
import { CarCardComponent } from './asset-card/car-card/car-card.component';
import { DriverCardComponent } from './asset-card/driver-card/driver-card.component';
import { ObdCardComponent } from './asset-card/obd-card/obd-card.component';
import { RfTagCardComponent } from './asset-card/rf-tag-card/rf-tag-card.component';
import { RightSidebarComponent } from './core/right-sidebar/right-sidebar.component';
import { AddAssetListComponent } from './core/add-asset-list/add-asset-list.component';
import { AssetEditFromComponent } from './asset-edit-from/asset-edit-from.component';
import { GroupBadgeComponent } from './core/group-badge/group-badge.component';
import { NearableComponentComponent } from './nearable-component/nearable-component.component';
import {NearableService} from '../services/nearable.service';
import {AssetGroupService} from '../services/asset.group.service';
import {ExceptionService} from '../services/exception.service';
import {AssetConfigService} from '../services/asset-config.service';
import {SearchService} from '../services/search.service';
import {ValidityCheckService} from '../services/validity-check.service';
import { Ng2SearchPipeModule } from 'ng2-search-filter'; // importing the module
import { Ng2OrderModule } from 'ng2-order-pipe'; // importing the module
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import { AssetsForLinkingComponent } from './assets-for-linking/assets-for-linking.component';
import { AlertPopupComponent } from './core/alert-popup/alert-popup.component';
import { RightSiderToAddComponent } from './core/right-sider-to-add/right-sider-to-add.component';
import {LinkAssetComponent} from "./link-asset/link-asset.component";
import { PaginationComponent } from './pagination/pagination.component';
import {PagerService} from "../services/pager.service";
import { AssetCardNearableComponent } from './asset-card-nearable/asset-card-nearable.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AssetLinkedCountComponent } from './core/asset-linked-count/asset-linked-count.component';
import {SettingsComponent} from "./settings/settings.component";
import { ReportComponent } from './report/report.component';
import { InspectionBayComponent } from './inspection-bay/inspection-bay.component';
import {ConfigService} from "../services/config.service";
import {MyProfileManagementService} from "../services/my-profile-management.service";
import {SetCssPropertiesService} from '../services/set-css-properties.service';
import { LoaderComponent } from './loader/loader.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';


import { HTTPListener, HTTPStatus } from '../interceptors/HttpInterceptor';
import {BeaconGatewayService} from "../services/beacon-gateway.service";
import {NgbDatePipe} from "../services/dateformat";
import { SearchForAssetComponent } from './core/search-for-asset/search-for-asset.component';
import { BeaconGatewayCardComponent } from './asset-card/beacon-gateway-card/beacon-gateway-card.component';

import { InspectionBayService } from '../services/inspectionBay.service';
import { WebsocketService } from '../services/webSocket.service';
import { SocketService } from '../services/socket.service';
import { HealthCheckService } from '../services/healthCheck.service';
import { UserManagementService } from '../services/userManagement.service';
import { FullScreenComponent } from './core/full-screen/full-screen.component';
import {RfidGatewayService} from '../services/rfid-gateway.service';
import { RfTagReaderComponent } from './asset-card/rf-tag-reader/rf-tag-reader.component';
import { AssetCardExceptionComponent } from './asset-card-exception/asset-card-exception.component';
import { PdfViewerComponent } from './core/pdf-viewer/pdf-viewer.component';
import { CircularLoaderComponent } from './core/circular-loader/circular-loader.component';
import {LoginServiceService} from '../services/login-service.service';
import { ReportTableComponent } from './report-table/report-table.component';
import { ReportTableItemComponent } from './report-table/report-table-item/report-table-item.component';
import { ModalLgReportDetailsComponent } from './core/modal-lg-report-details/modal-lg-report-details.component';
import { EngineDiagnosticDetailsComponent } from './car-inspection-details/engine-diagnostic-details/engine-diagnostic-details.component';
import { CarPlateDetailsComponent } from './car-inspection-details/car-plate-details/car-plate-details.component';
import { UvisDetailsComponent } from './car-inspection-details/uvis-details/uvis-details.component';
import { FilterPipe} from './core/filter/filter.pipe';
import { SearchBoxComponent } from './core/search-box/search-box.component';
import { AvddsDetailsComponent } from './car-inspection-details/avdds-details/avdds-details.component';
import { VehicleDetailsComponent } from './car-inspection-details/vehicle-details/vehicle-details.component';
import { VehicleStatusComponent } from './car-inspection-details/vehicle-status/vehicle-status.component';
import { AssetsListComponent } from './car-inspection-details/assets-list/assets-list.component';
import { DriverDetailsComponent } from './car-inspection-details/driver-details/driver-details.component';
import { DriverFeedbackComponent } from './car-inspection-details/driver-feedback/driver-feedback.component';
import { NumberPlateRecognitionComponent } from './car-inspection-details/number-plate-recognition/number-plate-recognition.component';
import {AuthGuard} from '../auth/auth.guard';
import { ForbiddenComponent } from './core/forbidden/forbidden.component';
import { GeneralServiceCardComponent } from './asset-card/general-service-card/general-service-card.component';
import { ServicesComponent } from './services/services.component';
import { ServicesForLinkingComponent } from './services-for-linking/services-for-linking.component';
import { SearchForServiceComponent } from './core/search-for-service/search-for-service.component';
import { GenericCardComponent } from './asset-card/generic-card/generic-card.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { UploadAssetDataComponent } from './upload-asset-data/upload-asset-data.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import {MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatInputModule,
        MatSelectModule, MatRadioModule, MatCardModule, MatNativeDateModule, MatDatepickerModule, MatTabsModule, MatTableModule,
        MatExpansionModule, MatPaginatorModule, MatTooltipModule, MatSlideToggleModule} from '@angular/material';
import { GaugeModule } from 'angular-gauge';
import { AbilityModule } from '@casl/angular';
import { Ability } from '@casl/ability';
import { ability} from '../auth/ability';
import { CustomErrorHandler } from './authErroeHandler';
import { Subject,Actions} from '../auth/rules';

/*
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';*/

const RxJS_Services = [HTTPListener, HTTPStatus];
import { FileUploadModule } from 'ng2-file-upload';
import { AssetExceptionInfoComponent } from './asset-exception-info/asset-exception-info.component';
import { ServerDetailsCardComponent } from './asset-card/server-details-card/server-details-card.component';
import { SystemStatsGraphComponent } from './core/system-stats-graph/system-stats-graph.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { IotzenPillsComponent } from './core/iotzen-pills/iotzen-pills.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

import {ErrorHandler} from "@angular/core";
import { UserCreateFormComponent } from './user-create-form/user-create-form.component';
import { UserEditFormComponent } from './user-edit-form/user-edit-form.component';


@NgModule({
  declarations: [
    AppComponent,
    CountCardComponent,
    AssetListComponent,
    SideMenuComponent,
    HeaderComponent,
    LoginComponent,
    LoginHeaderComponent,
    AssetFormComponent,
    AssetCardComponent,
    BeaconCardComponent,
    CarCardComponent,
    DriverCardComponent,
    ObdCardComponent,
    RfTagCardComponent,
    RightSidebarComponent,
    AddAssetListComponent,
    AssetEditFromComponent,
    GroupBadgeComponent,
    NearableComponentComponent,
    AssetsForLinkingComponent,
    AlertPopupComponent,
    RightSiderToAddComponent,
    LinkAssetComponent,
    PaginationComponent,
    AssetLinkedCountComponent,
    AssetCardNearableComponent,
    SettingsComponent,
    ReportComponent,
    InspectionBayComponent,
    LoaderComponent,
    SearchForAssetComponent,
    BeaconGatewayCardComponent,
    FullScreenComponent,
    RfTagReaderComponent,
    AssetCardExceptionComponent,
    PdfViewerComponent,
    CircularLoaderComponent,
    ReportTableComponent,
    ReportTableItemComponent,
    ModalLgReportDetailsComponent,
    EngineDiagnosticDetailsComponent,
    CarPlateDetailsComponent,
    UvisDetailsComponent,
    FilterPipe,
    NgbDatePipe,
    SearchBoxComponent,
    AvddsDetailsComponent,
    VehicleDetailsComponent,
    VehicleStatusComponent,
    AssetsListComponent,
    DriverDetailsComponent,
    DriverFeedbackComponent,
    NumberPlateRecognitionComponent,
    ForbiddenComponent,
    GeneralServiceCardComponent,
    ServicesComponent,
    ServicesForLinkingComponent,
    SearchForServiceComponent,
    GenericCardComponent,
    AnalyticsComponent,
    UploadAssetDataComponent,
    AssetExceptionInfoComponent,
    ServerDetailsCardComponent,
    SystemStatsGraphComponent,
    IotzenPillsComponent,
    UserManagementComponent,
    UserProfileComponent,
    UserCreateFormComponent,
    UserEditFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),
    Ng2SearchPipeModule, // including into imports
    Ng2OrderModule, // importing the sorting package here
    NgxPaginationModule,
    NgbModule.forRoot(),
    FileUploadModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatTabsModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatExpansionModule,
    GaugeModule.forRoot(),
    AbilityModule.forRoot()
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    AssetService,
    ServiceLinkableService,
    FormService,
    NearableService,
    PagerService,
    AssetGroupService,
    AssetConfigService,
    ConfigService,
    ExceptionService,
    RxJS_Services,
    BeaconGatewayService,
    SetCssPropertiesService,
    WebsocketService,
    RfidGatewayService,
    SocketService,
    HealthCheckService,
    UserManagementService,
    SearchService,
    ValidityCheckService,
    LoginServiceService,
    MyProfileManagementService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HTTPListener,
      multi: true
    },
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    InspectionBayService,
    { provide: Ability, useValue:ability},
    {provide: ErrorHandler, useClass: CustomErrorHandler}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
