import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ConfigService } from '../../../services/config.service';

@Component({
  selector: 'app-number-plate-recognition',
  templateUrl: './number-plate-recognition.component.html',
  styleUrls: ['./number-plate-recognition.component.scss']
})
export class NumberPlateRecognitionComponent implements OnChanges, OnInit {
  @Input() numberPlateRecognitionDetails: any;
  @Input() resetComponetValue: number;
  public urlString: string = '';
  public uvisPlateRecognitionImageUrl: string;
  public avddsPlateRecognitionImageUrl: string;
  public uvisRecognisedPlateNumber: string;
  public avddsRecognisedPlateNumber: string;
  public showNumberPlateRecognition: boolean;
  public uvisPlateRecognitionImageNotFound: boolean;
  public avddsPlateRecognitionImageNotFound: boolean;
  public uvisRecognisedPlateNumberNotFound: boolean;
  public avddsRecognisedPlateNumberNotFound: boolean;
  constructor(public configService: ConfigService) {
    this.setFlagValues();
  }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    this.urlString = this.configService.appConfig.appBaseUrl;
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'numberPlateRecognitionDetails') {
        if (change.currentValue !== change.previousValue) {
          this.showNumberPlateRecognition = true;
          this.getAvddsDetails();
          this.getUvisDetails();
        }
      }
      if (propName === 'resetComponetValue') {
        if (change.currentValue !== change.previousValue) {
          this.setFlagValues();
        }
      }
    }
  }

  getAvddsDetails() {
    if (this.numberPlateRecognitionDetails['avdds']) {
      this.getAvddsPlateImgDetails();
      this.getAvddsPlateNumberDetails();
    } else {
      this.avddsRecognisedPlateNumberNotFound = true;
      this.avddsPlateRecognitionImageNotFound = true;
    }
  }

  getAvddsPlateImgDetails() {
    if (this.numberPlateRecognitionDetails['avdds']['image'] && this.numberPlateRecognitionDetails['avdds']['image']['Front LPR']) {
      this.numberPlateRecognitionDetails['avdds']['image']['Front LPR']['fileName'] ?
        this.avddsPlateRecognitionImageUrl = this.urlString + 'avdds/image?imageName=' + this.numberPlateRecognitionDetails['avdds']['image']['Front LPR']['fileName'] :
        this.avddsPlateRecognitionImageNotFound = true;
    } else {
      this.avddsPlateRecognitionImageNotFound = true;
    }
  }

  getAvddsPlateNumberDetails() {
    if (this.numberPlateRecognitionDetails['avdds']['event'] &&
      this.numberPlateRecognitionDetails['avdds']['event']['accessdecision']
      && this.numberPlateRecognitionDetails['avdds']['event']['accessdecision']['frontplate']) {
      this. avddsRecognisedPlateNumber = this.numberPlateRecognitionDetails['avdds']['event']['accessdecision']['frontplate'];
    } else {
      if (this.numberPlateRecognitionDetails['avdds']['event'] &&
        this.numberPlateRecognitionDetails['avdds']['event']['accessdecision']
        && this.numberPlateRecognitionDetails['avdds']['event']['accessdecision']['rearplate']) {
        this. avddsRecognisedPlateNumber = this.numberPlateRecognitionDetails['avdds']['event']['accessdecision']['rearplate'];
      } else {
        this.avddsRecognisedPlateNumberNotFound = true;
      }
    }
  }

  getUvisDetails() {
    if (this.numberPlateRecognitionDetails['uvis']) {
      this.getUvisPlateImgDetails();
      this.getUvisPlateNumberDetails();
    } else {
      this.uvisPlateRecognitionImageNotFound = true;
      this.uvisRecognisedPlateNumberNotFound = true;
    }
  }

  getUvisPlateImgDetails() {
    if (this.numberPlateRecognitionDetails['uvis']['plate'] && this.numberPlateRecognitionDetails['uvis']['plate']['image']) {
      this.numberPlateRecognitionDetails['uvis']['plate']['image']['fileName'] ?
        this.uvisPlateRecognitionImageUrl = this.urlString + 'uvis/image?imageName=' + this.numberPlateRecognitionDetails['uvis']['plate']['image']['fileName'] :
        this.uvisPlateRecognitionImageNotFound = true;
    } else {
      this.uvisPlateRecognitionImageNotFound = true;
    }
  }

  getUvisPlateNumberDetails() {
    if (this.numberPlateRecognitionDetails['uvis']['com'] && this.numberPlateRecognitionDetails['uvis']['com']['analyticalimaging'] &&
      this.numberPlateRecognitionDetails['uvis']['com']['analyticalimaging']['uvis'] && this.numberPlateRecognitionDetails['uvis']['com']['analyticalimaging']['uvis']['EntranceExportable'] &&
      this.numberPlateRecognitionDetails['uvis']['com']['analyticalimaging']['uvis']['EntranceExportable']['plate']) {
      this.uvisRecognisedPlateNumber = this.numberPlateRecognitionDetails['uvis']['com']['analyticalimaging']['uvis']['EntranceExportable']['plate'];
    } else {
      this.uvisRecognisedPlateNumberNotFound = true;
    }
  }

  setFlagValues() {
    this.uvisPlateRecognitionImageUrl = '';
    this.uvisRecognisedPlateNumber = '';
    this.avddsRecognisedPlateNumber = '';
    this.avddsPlateRecognitionImageUrl = '';
    this.avddsPlateRecognitionImageNotFound = false;
    this.showNumberPlateRecognition = false;
    this.uvisPlateRecognitionImageNotFound = false;
    this.uvisRecognisedPlateNumberNotFound = false;
    this.avddsRecognisedPlateNumberNotFound = false;
  }
}
