import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberPlateRecognitionComponent } from './number-plate-recognition.component';

describe('NumberPlateRecognitionComponent', () => {
  let component: NumberPlateRecognitionComponent;
  let fixture: ComponentFixture<NumberPlateRecognitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumberPlateRecognitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberPlateRecognitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
