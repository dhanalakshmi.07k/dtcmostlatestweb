import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-driver-feedback',
  templateUrl: './driver-feedback.component.html',
  styleUrls: ['./driver-feedback.component.scss']
})
export class DriverFeedbackComponent implements OnChanges, OnInit {
  @Input() driverFeedback: any;
  @Input() resetComponetValue: number;
  @Input() randomNumberToSetData: number;
  public acUrl: string;
  public engineUrl: string;
  public brakingUrl: string;
  public steeringUrl: string;
  public suspensionUrl: string;
  public transmissionUrl: string;
  public batteryUrl: string;
  public headLampsUrl: string;
  public wheelAlignmentUrl: string;
  public wheelVibrationUrl: string;

  public feedBackNotFoundUrl: string;
  public acNotWorking: boolean;
  public engineNotWorking: boolean;
  public brakingNotWorking: boolean;
  public steeringNotWorking: boolean;
  public suspensionNotWorking: boolean;
  public transmissionNotWorking: boolean;
  public batteryNotWorking: boolean;
  public headLampsNotWorking: boolean;
  public wheelAlignmentNotWorking: boolean;
  public wheelVibrationNotWorking: boolean;
  constructor() {
    this.resetImageUrl();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'driverFeedback') {
        if (change.currentValue !== change.previousValue) {
          this.getDriverFeedbackDetails();
        }
      }
      if (propName === 'randomNumberToSetData') {
        if (change.currentValue !== change.previousValue) {
          this.getDriverFeedbackDetails();
        }
      }
      if (propName === 'resetComponetValue') {
        if (change.currentValue !== change.previousValue) {
          this.resetImageUrl();
        }
      }
    }
  }

  getDriverFeedbackDetails() {
    let driverFeedback: any;
    driverFeedback = this.driverFeedback;
    if (driverFeedback.driverFeedBack && driverFeedback.driverFeedBack !== null) {
        if ((driverFeedback.driverFeedBack.ac && driverFeedback.driverFeedBack.ac === 'notWorking' )) {
          this.acNotWorking = true;
          this.acUrl = '../../../assets/driver-feedback/ac.png';
        } else if ((!driverFeedback.driverFeedBack.ac || driverFeedback.driverFeedBack.ac === 'working' )) {
          this.acNotWorking = false;
          this.acUrl = '../../../assets/driver-feedback/ac.png';
        }
        if ((driverFeedback.driverFeedBack.engine && driverFeedback.driverFeedBack.engine === 'notWorking' )) {
          this.engineNotWorking = true;
          this.engineUrl = '../../../assets/driver-feedback/engine.png';
        } else if ((!driverFeedback.driverFeedBack.engine || driverFeedback.driverFeedBack.engine === 'working' )) {
          this.engineNotWorking = false;
          this.engineUrl = '../../../assets/driver-feedback/engine.png';
        }
        if ((driverFeedback.driverFeedBack.braking && driverFeedback.driverFeedBack.braking === 'notWorking' )) {
          this.brakingNotWorking = true;
          this.brakingUrl = '../../../assets/driver-feedback/braking.png';
        } else if ((!driverFeedback.driverFeedBack.braking || driverFeedback.driverFeedBack.braking === 'working' )) {
          this.brakingNotWorking = false;
          this.brakingUrl = '../../../assets/driver-feedback/braking.png';
        }
        if ((driverFeedback.driverFeedBack.steering && driverFeedback.driverFeedBack.steering === 'notWorking' )) {
          this.steeringNotWorking = true;
          this.steeringUrl = '../../../assets/driver-feedback/steering.png';
        } else if ((!driverFeedback.driverFeedBack.steering || driverFeedback.driverFeedBack.steering === 'working' )) {
          this.steeringNotWorking = false;
          this.steeringUrl = '../../../assets/driver-feedback/steering.png';
        }
        if ((driverFeedback.driverFeedBack.suspension && driverFeedback.driverFeedBack.suspension === 'notWorking' )) {
          this.suspensionNotWorking = true;
          this.suspensionUrl = '../../../assets/driver-feedback/suspension.png';
        } else if ((!driverFeedback.driverFeedBack.suspension || driverFeedback.driverFeedBack.suspension === 'working' )) {
          this.suspensionNotWorking = false;
          this.suspensionUrl = '../../../assets/driver-feedback/suspension.png';
        }
        if ((driverFeedback.driverFeedBack.transmission && driverFeedback.driverFeedBack.transmission === 'notWorking' )) {
          this.transmissionNotWorking = true;
          this.transmissionUrl = '../../../assets/driver-feedback/transmission.png';
        } else if ((!driverFeedback.driverFeedBack.transmission || driverFeedback.driverFeedBack.transmission === 'working' )) {
          this.transmissionNotWorking = false;
          this.transmissionUrl = '../../../assets/driver-feedback/transmission.png';
        }
        if ((driverFeedback.driverFeedBack.battery && driverFeedback.driverFeedBack.battery === 'notWorking' )) {
          this.batteryNotWorking = true;
          this.batteryUrl = '../../../assets/driver-feedback/battery.png';
        } else if ((!driverFeedback.driverFeedBack.battery || driverFeedback.driverFeedBack.battery === 'working' )) {
          this.batteryNotWorking = false;
          this.batteryUrl = '../../../assets/driver-feedback/battery.png';
        }
        if ((driverFeedback.driverFeedBack.headLamps && driverFeedback.driverFeedBack.headLamps === 'notWorking' )) {
          this.headLampsNotWorking = true;
          this.headLampsUrl = '../../../assets/driver-feedback/headlamps.png';
        } else if ((!driverFeedback.driverFeedBack.headLamps || driverFeedback.driverFeedBack.headLamps === 'working' )) {
          this.headLampsNotWorking = false;
          this.headLampsUrl = '../../../assets/driver-feedback/headlamps.png';
        }
        if ((driverFeedback.driverFeedBack.wheelAlignment && driverFeedback.driverFeedBack.wheelAlignment === 'notWorking' )) {
          this.wheelAlignmentNotWorking = true;
          this.wheelAlignmentUrl = '../../../assets/driver-feedback/wheelAlignment.png';
        } else if ((!driverFeedback.driverFeedBack.wheelAlignment || driverFeedback.driverFeedBack.wheelAlignment === 'working' )) {
          this.wheelAlignmentNotWorking = false;
          this.wheelAlignmentUrl = '../../../assets/driver-feedback/wheelAlignment.png';
        }
        if ((driverFeedback.driverFeedBack.wheelVibration && driverFeedback.driverFeedBack.wheelVibration === 'notWorking' )) {
          this.wheelVibrationNotWorking = true;
          this.wheelVibrationUrl = '../../../assets/driver-feedback/wheelVibration.png';
        } else if ((!driverFeedback.driverFeedBack.wheelVibration || driverFeedback.driverFeedBack.wheelVibration === 'working' )) {
          this.wheelVibrationNotWorking = false;
          this.wheelVibrationUrl = '../../../assets/driver-feedback/wheelVibration.png';
        }
    } else if (!driverFeedback.driverFeedBack || driverFeedback.driverFeedBack === null) {
        this.feedBackNotFoundUrl = '../../../assets/notconnectedIcon.png';
    }
  }

  resetImageUrl() {
    this.feedBackNotFoundUrl = '';
    this.acUrl = '';
    this.engineUrl = '';
    this.brakingUrl = '';
    this.steeringUrl = '';
    this.suspensionUrl = '';
    this.transmissionUrl = '';
    this.batteryUrl = '';
    this.headLampsUrl = '';
    this.wheelAlignmentUrl = '';
    this.wheelVibrationUrl = '';
  }

}
