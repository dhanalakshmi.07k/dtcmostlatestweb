import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineDiagnosticDetailsComponent } from './engine-diagnostic-details.component';

describe('EngineDiagnosticDetailsComponent', () => {
  let component: EngineDiagnosticDetailsComponent;
  let fixture: ComponentFixture<EngineDiagnosticDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngineDiagnosticDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineDiagnosticDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
