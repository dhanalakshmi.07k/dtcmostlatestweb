import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as _ from "lodash";

@Component({
  selector: 'app-engine-diagnostic-details',
  templateUrl: './engine-diagnostic-details.component.html',
  styleUrls: ['./engine-diagnostic-details.component.scss']
})
export class EngineDiagnosticDetailsComponent implements OnChanges, OnInit {
   @Input() engineDiagnosticDetails: any;
   @Input() resetComponetValue: number;
   public engineFaultArray: any = [];
   public showEngineFault: boolean;
   public showNoIssue: boolean;
   public showNoData: boolean;
   public showCircularLoader: boolean;
  constructor() {
    this.setFlagValues();
  }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'engineDiagnosticDetails') {
        if (change.currentValue !== change.previousValue) {
          if (this.engineDiagnosticDetails.engineInspection) {
            this.showCircularLoader = false;
            let carInfo = this.engineDiagnosticDetails['engineInspection']['carInfo'];
            if (carInfo) {
              let dtcp = carInfo['dtc_p'];
              let dtcs = carInfo['dtc_s'];
              let engineFault = [];
              engineFault = _.union(dtcp, dtcs);
              this.engineFaultArray = engineFault;
              if (this.engineFaultArray && this.engineFaultArray.length > 0 ) {
                this.showEngineFault = true;
              } else {
                this.showNoIssue = true;
              }
            }
          } else {
            this.showNoData = true;
            this.showCircularLoader = false;
          }
        }
      }
      if (propName === 'resetComponetValue') {
        if (change.currentValue !== change.previousValue) {
          this.setFlagValues();
        }
      }
    }
  }

  setFlagValues() {
    this.showNoIssue = false;
    this.showNoData = false;
    this.showEngineFault = false;
    this.showCircularLoader = true;
  }

}
