import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ConfigService } from '../../../services/config.service';

import { Subject, Actions} from '../../../auth/rules';
@Component({
  selector: 'app-uvis-details',
  templateUrl: './uvis-details.component.html',
  styleUrls: ['./uvis-details.component.scss']
})
export class UvisDetailsComponent implements OnChanges, OnInit {
  @Input() uvisDetails: any;
  @Input() resetComponetValue: number;
  SUBJECT = Subject;
  ACTIONS = Actions;
  constructor(public configService: ConfigService ) {
    this.resetFlagValues();
    this.resetImageUrl();
    this.setFlagValues();
  }
  public urlString: string = '';
  public uvisImageUrl: string = '';
  public uvisComparedImageUrl: string = '';
  public uvisDriveImageUrl: string = '';
  public showUvisCaution: boolean;
  public showNoDataAvailable: boolean;
  public showNoComparedImageAvailable: boolean;
  public showCircularLoader: boolean;
  public mismatchPlate: string = '';

  public uvisPlateRecognitionImageUrl: string;
  public uvisRecognisedPlateNumber: string;
  public showNumberPlateRecognition: boolean;
  public uvisPlateRecognitionImageNotFound: boolean;
  public uvisRecognisedPlateNumberNotFound: boolean;

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    this.urlString = this.configService.appConfig.appBaseUrl;
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'uvisDetails') {
        if (change.currentValue !== change.previousValue) {
          this.showNumberPlateRecognition = true;
          this.getUvisDetails();
          if (this.uvisDetails['uvis']) {
            if (this.uvisDetails['uvis']['driverImage'] && this.uvisDetails['uvis']['driverImage']['image'] && this.uvisDetails['uvis']['driverImage']['image']['fileName']) {
              this.uvisDriveImageUrl = this.urlString + 'uvis/image?imageName=' + this.uvisDetails['uvis']['driverImage']['image']['fileName'];
            } else {
              this.uvisDriveImageUrl = '';
            }
            if (this.uvisDetails['uvis']['uvis'] && this.uvisDetails['uvis']['uvis']['image']) {
              this.showNoDataAvailable = false;
              this.showCircularLoader = false;
              this.uvisDetails['uvis']['uvis']['image']['fileName'] ?
                this.uvisImageUrl = this.urlString + 'uvis/image?imageName=' + this.uvisDetails['uvis']['uvis']['image']['fileName'] :
                this.uvisImageUrl = '';
              // this.uvisImageUrl = this.urlString + 'uvis/image?imageName=' + this.uvisDetails['uvis']['uvis']['image']['fileName'];
            } else {
              this.showNoDataAvailable = true;
              this.showCircularLoader = false;
              // this.resetImageUrl();
            }
            if (this.uvisDetails['uvis']['imageCompared'] && this.uvisDetails['uvis']['imageCompared']['image']) {
              this.showNoComparedImageAvailable = false;
              this.showCircularLoader = false;
              this.uvisDetails['uvis']['imageCompared']['image']['fileName'] ?
                this.uvisComparedImageUrl = this.urlString + 'uvis/image?imageName=' + this.uvisDetails['uvis']['imageCompared']['image']['fileName'] :
                this.uvisComparedImageUrl = '';
              // this.uvisComparedImageUrl = this.urlString + 'uvis/image?imageName=' + this.uvisDetails['uvis']['imageCompared']['image']['fileName'];
            } else {
              this.showNoComparedImageAvailable = true;
              this.showCircularLoader = false;
              // this.resetImageUrl();
            }
            if (this.uvisDetails['uvis']['plateNumber']) {
              this.mismatchPlate = this.uvisDetails['uvis']['plateNumber'];
            } else if (this.uvisDetails['uvis']['com'] && this.uvisDetails['uvis']['com']['analyticalimaging'] &&
              this.uvisDetails['uvis']['com']['analyticalimaging']['uvis'] && this.uvisDetails['uvis']['com']['analyticalimaging']['uvis']['EntranceExportable'] &&
              this.uvisDetails['uvis']['com']['analyticalimaging']['uvis']['EntranceExportable']['plate']) {
              this.mismatchPlate = this.uvisDetails['uvis']['com']['analyticalimaging']['uvis']['EntranceExportable']['plate'];
            }
            /*if (this.uvisDetails['uvis']['com'] && this.uvisDetails['uvis']['com']['analyticalimaging'] &&
             this.uvisDetails['uvis']['com']['analyticalimaging']['uvis'] && this.uvisDetails['uvis']['com']['analyticalimaging']['uvis']['EntranceExportable'] &&
             this.uvisDetails['uvis']['com']['analyticalimaging']['uvis']['EntranceExportable']['plate']) {
             this.mismatchPlate = this.uvisDetails['uvis']['com']['analyticalimaging']['uvis']['EntranceExportable']['plate'];
             }*/
            this.uvisDetails['uvis']['match'] ? this.showUvisCaution = false : this.showUvisCaution = true;
          } else {
            this.showNoDataAvailable = true;
            this.showCircularLoader = false;
            this.resetImageUrl();
          }
        }
      }
      if (propName === 'resetComponetValue') {
        if (change.currentValue !== change.previousValue) {
          this.resetImageUrl();
          this.resetFlagValues();
          this.setFlagValues();
        }
      }
    }
  }

  getUvisDetails() {
    if (this.uvisDetails['uvis']) {
      this.getUvisPlateImgDetails();
      this.getUvisPlateNumberDetails();
    } else {
      this.uvisPlateRecognitionImageNotFound = true;
      this.uvisRecognisedPlateNumberNotFound = true;
    }
  }

  getUvisPlateImgDetails() {
    if (this.uvisDetails['uvis']['plate'] && this.uvisDetails['uvis']['plate']['image']) {
      this.uvisDetails['uvis']['plate']['image']['fileName'] ?
        this.uvisPlateRecognitionImageUrl = this.urlString + 'uvis/image?imageName=' + this.uvisDetails['uvis']['plate']['image']['fileName'] :
        this.uvisPlateRecognitionImageNotFound = true;
    } else {
      this.uvisPlateRecognitionImageNotFound = true;
    }
  }

  getUvisPlateNumberDetails() {
    if (this.uvisDetails['uvis']['plateNumber']) {
      this.uvisRecognisedPlateNumber = this.uvisDetails['uvis']['plateNumber'];
    } else if (this.uvisDetails['uvis']['com'] && this.uvisDetails['uvis']['com']['analyticalimaging'] &&
      this.uvisDetails['uvis']['com']['analyticalimaging']['uvis'] && this.uvisDetails['uvis']['com']['analyticalimaging']['uvis']['EntranceExportable'] &&
      this.uvisDetails['uvis']['com']['analyticalimaging']['uvis']['EntranceExportable']['plate']) {
      this.uvisRecognisedPlateNumber = this.uvisDetails['uvis']['com']['analyticalimaging']['uvis']['EntranceExportable']['plate'];
    } else {
      this.uvisRecognisedPlateNumberNotFound = true;
    }
  }

  resetFlagValues() {
    this.showUvisCaution = false;
    this.showNoDataAvailable = false;
    this.showCircularLoader = true;
    this.showNoComparedImageAvailable = false;
  }

  resetImageUrl() {
    this.uvisImageUrl = '';
    this.uvisComparedImageUrl = '';
    this.mismatchPlate = '';
    this.uvisDriveImageUrl = '';
  }

  setFlagValues() {
    this.uvisPlateRecognitionImageUrl = '';
    this.uvisRecognisedPlateNumber = '';
    this.showNumberPlateRecognition = false;
    this.uvisPlateRecognitionImageNotFound = false;
    this.uvisRecognisedPlateNumberNotFound = false;
  }

}
