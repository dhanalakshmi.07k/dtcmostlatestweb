import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UvisDetailsComponent } from './uvis-details.component';

describe('UvisDetailsComponent', () => {
  let component: UvisDetailsComponent;
  let fixture: ComponentFixture<UvisDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UvisDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UvisDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
