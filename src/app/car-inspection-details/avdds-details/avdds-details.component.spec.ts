import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvddsDetailsComponent } from './avdds-details.component';

describe('AvddsDetailsComponent', () => {
  let component: AvddsDetailsComponent;
  let fixture: ComponentFixture<AvddsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvddsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvddsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
