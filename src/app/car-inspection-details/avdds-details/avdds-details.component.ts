import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ConfigService } from '../../../services/config.service';
declare var $: any;

@Component({
  selector: 'app-avdds-details',
  templateUrl: './avdds-details.component.html',
  styleUrls: ['./avdds-details.component.scss']
})
export class AvddsDetailsComponent implements OnChanges, OnInit {
  @Input() avddsDetails: any;
  @Input() resetComponetValue: number;
  constructor(private configService: ConfigService) {
    this.resetImageUrl();
    this.setFlagValues();
    this.showNoDataAvailable = false;
    this.showAvddsCaution = false;
    this.showCircularLoader = true;
  }
  public urlString: string = '';
  public avddsRightImageUrl: string = '';
  public avddsLeftImageUrl: string = '';
  public avddsTopImageUrl: string = '';
  public avddsFrontImageUrl: string = '';
  public avddsRearImageUrl: string = '';
  public frontplateMismatch: string = '';
  public rearplateMismatch: string = '';
  public showCircularLoader: boolean;
  public showNoDataAvailable: boolean;
  public showAvddsCaution: boolean;

  public showNumberPlateRecognition: boolean;
  public avddsPlateRecognitionImageUrl: string;
  public avddsRecognisedPlateNumber: string;
  public avddsPlateRecognitionImageNotFound: boolean;
  public avddsRecognisedPlateNumberNotFound: boolean;

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    this.urlString = this.configService.appConfig.appBaseUrl;
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'avddsDetails') {
        if (change.currentValue !== change.previousValue) {
          this.showNumberPlateRecognition = true;
          this.getAvddsDetails();
          if (this.avddsDetails['avdds']) {
            if (this.avddsDetails['avdds']['image']) {
              this.showNoDataAvailable = false;
              this.showCircularLoader = false;
              $('.avdds-row .avdds-col').css('height', '500px');
              if (this.avddsDetails['avdds']['image']['Right DI'] && this.avddsDetails['avdds']['image']['Right DI']['fileName']) {
                this.avddsRightImageUrl = this.urlString + 'avdds/image?imageName=' + this.avddsDetails['avdds']['image']['Right DI']['fileName'];
              }
              if (this.avddsDetails['avdds']['image']['Left DI'] && this.avddsDetails['avdds']['image']['Left DI']['fileName']) {
                this.avddsLeftImageUrl = this.urlString + 'avdds/image?imageName=' + this.avddsDetails['avdds']['image']['Left DI']['fileName'];
              }
              if (this.avddsDetails['avdds']['image']['Top DI'] && this.avddsDetails['avdds']['image']['Top DI']['fileName']) {
                this.avddsTopImageUrl = this.urlString + 'avdds/image?imageName=' + this.avddsDetails['avdds']['image']['Top DI']['fileName'];
              }
              if (this.avddsDetails['avdds']['image']['Front DI'] && this.avddsDetails['avdds']['image']['Front DI']['fileName']) {
                this.avddsFrontImageUrl = this.urlString + 'avdds/image?imageName=' + this.avddsDetails['avdds']['image']['Front DI']['fileName'];
              }
              if (this.avddsDetails['avdds']['image']['Rear DI'] && this.avddsDetails['avdds']['image']['Rear DI']['fileName']) {
                this.avddsRearImageUrl = this.urlString + 'avdds/image?imageName=' + this.avddsDetails['avdds']['image']['Rear DI']['fileName'];
              }
            } else {
              this.showNoDataAvailable = true;
              this.showCircularLoader = false;
              this.resetImageUrl();
            }
            this.setPlateMismatchValues();
            this.avddsDetails['avdds']['match'] ? this.showAvddsCaution = false : this.showAvddsCaution = true;
          } else {
            this.showNoDataAvailable = true;
            this.showCircularLoader = false;
            this.resetImageUrl();
          }
        }
      }
      if (propName === 'resetComponetValue') {
        if (change.currentValue !== change.previousValue) {
          this.resetImageUrl();
          this.setFlagValues();
          this.showCircularLoader = true;
          this.showNoDataAvailable = false;
          this.showAvddsCaution = false;
        }
      }
    }
  }

  setPlateMismatchValues() {
    let frontPlateMismatch = '';
    let rearPlateMismatch = '';
    if (this.avddsDetails['avdds']['event'] && this.avddsDetails['avdds']['event']['accessdecision']) {
      if (this.avddsDetails['avdds']['event']['accessdecision']['frontplate']) {
        frontPlateMismatch = this.avddsDetails['avdds']['event']['accessdecision']['frontplate'];
        frontPlateMismatch ? this.frontplateMismatch = frontPlateMismatch : this.frontplateMismatch = '-';
      }
      if (this.avddsDetails['avdds']['event']['accessdecision']['rearplate']) {
        rearPlateMismatch = this.avddsDetails['avdds']['event']['accessdecision']['rearplate'];
        rearPlateMismatch ? this.rearplateMismatch = rearPlateMismatch : this.rearplateMismatch = '-';
      }
    }
  }

  getAvddsDetails() {
    if (this.avddsDetails['avdds']) {
      this.getAvddsPlateImgDetails();
      this.getAvddsPlateNumberDetails();
    } else {
      this.avddsRecognisedPlateNumberNotFound = true;
      this.avddsPlateRecognitionImageNotFound = true;
    }
  }

  getAvddsPlateImgDetails() {
    if (this.avddsDetails['avdds']['image'] && this.avddsDetails['avdds']['image']['Front LPR']) {
      this.avddsDetails['avdds']['image']['Front LPR']['fileName'] ?
        this.avddsPlateRecognitionImageUrl = this.urlString + 'avdds/image?imageName=' + this.avddsDetails['avdds']['image']['Front LPR']['fileName'] :
        this.avddsPlateRecognitionImageNotFound = true;
    } else {
      this.avddsPlateRecognitionImageNotFound = true;
    }
  }

  getAvddsPlateNumberDetails() {
    if (this.avddsDetails['avdds']['event'] &&
      this.avddsDetails['avdds']['event']['accessdecision']
      && this.avddsDetails['avdds']['event']['accessdecision']['frontplate']) {
      this. avddsRecognisedPlateNumber = this.avddsDetails['avdds']['event']['accessdecision']['frontplate'];
    } else {
      if (this.avddsDetails['avdds']['event'] &&
        this.avddsDetails['avdds']['event']['accessdecision']
        && this.avddsDetails['avdds']['event']['accessdecision']['rearplate']) {
        this. avddsRecognisedPlateNumber = this.avddsDetails['avdds']['event']['accessdecision']['rearplate'];
      } else {
        this.avddsRecognisedPlateNumberNotFound = true;
      }
    }
  }

  resetImageUrl() {
    $('.avdds-row .avdds-col').css('height', 'auto');
    this.avddsRightImageUrl = '';
    this.avddsLeftImageUrl = '';
    this.avddsTopImageUrl = '';
    this.avddsFrontImageUrl = '';
    this.avddsRearImageUrl = '';
    this.frontplateMismatch = '';
    this.rearplateMismatch = '';
  }

  setFlagValues() {
    this.avddsRecognisedPlateNumber = '';
    this.avddsPlateRecognitionImageUrl = '';
    this.avddsPlateRecognitionImageNotFound = false;
    this.showNumberPlateRecognition = false;
    this.avddsRecognisedPlateNumberNotFound = false;
  }

}
