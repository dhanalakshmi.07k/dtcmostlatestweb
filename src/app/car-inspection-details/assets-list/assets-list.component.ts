import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as _ from "lodash";

@Component({
  selector: 'app-assets-list',
  templateUrl: './assets-list.component.html',
  styleUrls: ['./assets-list.component.scss']
})
export class AssetsListComponent implements OnChanges, OnInit {
  @Input() assetsList: any;
  @Input() resetComponetValue: number;
  public showAssetsList: boolean;
  public beaconsAssociatedGroupsDetails: any = [];
  public rfIdDetails: any = [];
  constructor() { this.showAssetsList = true; }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.beaconsAssociatedGroupsDetails = [];
    this.rfIdDetails = [];
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'assetsList') {
        if (change.currentValue !== change.previousValue) {
          this.showAssetsList = true;
          this.getRfIdAndBeaconsDetails();
        }
      }
      if (propName === 'resetComponetValue') {
        if (change.currentValue !== change.previousValue) {
          this.showAssetsList = false;
        }
      }
    }
  }

  getRfIdAndBeaconsDetails() {
    let rfIdDetail;
    let beaconPresent;
    if (this.assetsList.equipments) {
      beaconPresent = this.assetsList.equipments.beacon;
      rfIdDetail = this.assetsList.equipments.rfId;
      if (rfIdDetail) {
        this.rfIdDetails = rfIdDetail;
      }
      if (beaconPresent) {
        this.beaconsAssociatedGroupsDetails = this.setEquipmentIcons(beaconPresent);
      }
    }
  }

  setEquipmentIcons(equipmentList) {
    _.forEach(equipmentList, function(value) {
       let type: any;
       let isEquipmentFound: any;
       type = value.groupDetail.type;
       isEquipmentFound = value.found;
      switch (type) {
        case 'cashMachine':
          if (isEquipmentFound) {
            value['icon'] = '../../../assets/asset-icons/card-machine.png';
          } else {value['icon'] = '../../../assets/asset-icons/card-machine-red.png'; }
          break;
        case 'emergencyWarningTriangle':
          if (isEquipmentFound) {
            value['icon'] = '../../../assets/asset-icons/danger.png';
          } else {value['icon'] = '../../../assets/asset-icons/danger-red.png'; }
          break;
        case 'medicalKit':
          if (isEquipmentFound) {
            value['icon'] = '../../../assets/asset-icons/first-aid-kit.png';
          } else {value['icon'] = '../../../assets/asset-icons/first-aid-kit-red.png'; }
          break;
        case 'fireExtinguisher':
          if (isEquipmentFound) {
            value['icon'] = '../../../assets/asset-icons/extinguisher.png';
          } else {value['icon'] = '../../../assets/asset-icons/extinguisher-red.png'; }
          break;
        case 'spareTyre':
          if (isEquipmentFound) {
            value['icon'] = '../../../assets/asset-icons/tyre.png';
          } else {value['icon'] = '../../../assets/asset-icons/tyre-red.png'; }
          break;
      }
    });
    return equipmentList;
  }

}
