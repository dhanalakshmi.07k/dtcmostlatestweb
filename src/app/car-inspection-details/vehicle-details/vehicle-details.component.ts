import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-vehicle-details',
  templateUrl: './vehicle-details.component.html',
  styleUrls: ['./vehicle-details.component.scss']
})
export class VehicleDetailsComponent implements OnChanges, OnInit {
  @Input() vehicleDetails: any;
  @Input() resetComponetValue: number;
  public showVehicleDetails: boolean;
  public failStatus: boolean;
  public checkStatus: boolean;
  public passStatus: boolean;
  public carLifeSpan: string;
  constructor() {
    this.showVehicleDetails = true;
    this.carLifeSpan = 'NA';
    this.resetStatusFlagVariables();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'vehicleDetails') {
        if (change.currentValue !== change.previousValue) {
          this.checkVehicleStatus();
          this.showVehicleDetails = true;
          this.carLifeSpan = 'NA';
          if (this.vehicleDetails['car'] && this.vehicleDetails['car']['lifeSpan']) {
            if (parseInt(this.vehicleDetails['car']['lifeSpan']) === 1) {
              this.carLifeSpan = this.vehicleDetails.car.lifeSpan + ' day';
            } else {
              this.carLifeSpan = this.vehicleDetails.car.lifeSpan + ' days';
            }
          }
        }
      }
      if (propName === 'resetComponetValue') {
        if (change.currentValue !== change.previousValue) {
          this.showVehicleDetails = false;
          this.resetStatusFlagVariables();
        }
      }
    }
  }

  checkVehicleStatus() {
    let overview;
    if (this.vehicleDetails && this.vehicleDetails.overview) {
      overview = this.vehicleDetails.overview;
      if (overview && overview.status) {
        if (overview.status === 'fail') {
          this.failStatus = true;
        } else if (overview.status === 'pass') {
          this.passStatus = true;
        } else if (overview.status === 'check') {
          this.checkStatus = true;
        }
      }
    }
  }

  resetStatusFlagVariables() {
    this.failStatus = false;
    this.passStatus = false;
    this.checkStatus = false;
  }

}
