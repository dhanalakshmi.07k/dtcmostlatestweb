import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-driver-details',
  templateUrl: './driver-details.component.html',
  styleUrls: ['./driver-details.component.scss']
})
export class DriverDetailsComponent implements OnChanges, OnInit {
  @Input() driverDetails: any;
  @Input() resetComponetValue: number;
  public showDriverDetails: boolean;
  constructor() { this.showDriverDetails = true; }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'driverDetails') {
        if (change.currentValue !== change.previousValue) {
          this.showDriverDetails = true;
        }
      }
      if (propName === 'resetComponetValue') {
        if (change.currentValue !== change.previousValue) {
          this.showDriverDetails = false;
        }
      }
    }
  }

}
