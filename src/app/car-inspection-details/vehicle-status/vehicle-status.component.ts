import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-vehicle-status',
  templateUrl: './vehicle-status.component.html',
  styleUrls: ['./vehicle-status.component.scss']
})
export class VehicleStatusComponent implements OnChanges, OnInit {
  @Input() vehicleStatus: any;
  public failStatus: boolean;
  public checkStatus: boolean;
  public passStatus: boolean;
  @Input() resetComponetValue: number;
  public showVehicleStatus: boolean;
  constructor() {
  this.showVehicleStatus = true;
  this.resetFlagVariables();
  }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'vehicleStatus') {
        if (change.currentValue !== change.previousValue) {
          this.showVehicleStatus = true;
          this.checkVehicleStatus();
        }
      }
      if (propName === 'resetComponetValue') {
        if (change.currentValue !== change.previousValue) {
          this.showVehicleStatus = false;
          this.resetFlagVariables();
        }
      }
    }
  }

  checkVehicleStatus() {
    let overview;
    if (this.vehicleStatus) {
      overview = this.vehicleStatus.overview;
      if (overview && overview.status) {
        if (overview.status === 'fail') {
          this.failStatus = true;
        } else if (overview.status === 'pass') {
          this.passStatus = true;
        } else if (overview.status === 'check') {
          this.checkStatus = true;
        }
      }
    }
  }

  resetFlagVariables() {
    this.failStatus = false;
    this.passStatus = false;
    this.checkStatus = false;
  }

}
