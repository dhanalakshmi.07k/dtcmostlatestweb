import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarPlateDetailsComponent } from './car-plate-details.component';

describe('CarPlateDetailsComponent', () => {
  let component: CarPlateDetailsComponent;
  let fixture: ComponentFixture<CarPlateDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarPlateDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarPlateDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
