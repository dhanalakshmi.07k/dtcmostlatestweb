import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-car-plate-details',
  templateUrl: './car-plate-details.component.html',
  styleUrls: ['./car-plate-details.component.scss']
})
export class CarPlateDetailsComponent implements OnChanges, OnInit {
  @Input() carDetails: any;
  @Input() resetComponetValue: number;
  @Input() randomNumberToSetData: number;
  public registrationNo: string;
  constructor() {
    this.registrationNo = '';
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'carDetails') {
        if (change.currentValue !== change.previousValue) {
          this.registrationNo = this.carDetails.car.registrationNumber;
        }
      }
      if (propName === 'randomNumberToSetData') {
        if (change.currentValue !== change.previousValue) {
          this.registrationNo = this.carDetails.car.registrationNumber;;
        }
      }
      if (propName === 'resetComponetValue') {
        if (change.currentValue !== change.previousValue) {
          this.registrationNo = '';
        }
      }
    }
  }

}
