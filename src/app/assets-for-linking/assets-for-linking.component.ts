import { Component, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
declare  var $: any;
import * as _ from "lodash";

@Component({
  selector: 'app-assets-for-linking',
  templateUrl: './assets-for-linking.component.html',
  styleUrls: ['./assets-for-linking.component.scss']
})
export class AssetsForLinkingComponent implements OnChanges, OnInit {
  searchedAssetForLinking: any;
  resetSearchedAssetOnBackBtn: number;
  public  linkAssetsDetails: any;
  public linkAssetsIds: any = [];
  @Input() allAssetTypes: any;
  @Input() assetData: any;
  @Input() config: any;
  @Input() resetSearchedAssetOnPlusClick: any;
  @Output() linkAssetDetailsToSlider: EventEmitter<any> = new EventEmitter();
  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges() {
  }

  searchedAssetsFound (searchedAssets) {
    this.searchedAssetForLinking = searchedAssets;
    this.linkAssetsIds = [];
  }

  linkAssetDetails(linkingAssetDetails) {
    if (this.assetData && this.assetData.assetType) {
      $('.buttonEnable').prop('disabled', false);
      this.emitSelectedAssetIdsTOSlider(linkingAssetDetails);
    } else if (this.assetData && this.assetData.serviceType) {
      this.emitSelectedAssetIdsTOSlider(linkingAssetDetails);
      if (this.linkAssetsIds.length > 1) {
        $('.buttonEnable').attr('disabled', true);
      } else {
        $('.buttonEnable').attr('disabled', false);
      }
    }
  }

  emitSelectedAssetIdsTOSlider(linkingAssetDetails) {
    this.linkAssetsDetails = linkingAssetDetails;
    if (linkingAssetDetails.isAssetSelected === true) {
      let res;
      res = _.find(this.linkAssetsIds, function(o) { return o === linkingAssetDetails.assetData._id; });
      if (!res) {
        this.linkAssetsIds.push(linkingAssetDetails.assetData._id);
      }
    } else if (linkingAssetDetails.isAssetSelected === false) {
      _.remove(this.linkAssetsIds, function(n) {
        return (n === linkingAssetDetails.assetData._id);
      });
    }
    this.linkAssetDetailsToSlider.emit(this.linkAssetsIds);
  }

  resetSearchedAssets() {
    this.resetSearchedAssetOnBackBtn = Math.random();
  }
}
