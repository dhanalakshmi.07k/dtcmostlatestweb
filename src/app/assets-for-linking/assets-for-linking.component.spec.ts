import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetsForLinkingComponent } from './assets-for-linking.component';

describe('AssetsForLinkingComponent', () => {
  let component: AssetsForLinkingComponent;
  let fixture: ComponentFixture<AssetsForLinkingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetsForLinkingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetsForLinkingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
