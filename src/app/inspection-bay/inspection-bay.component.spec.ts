import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionBayComponent } from './inspection-bay.component';

describe('InspectionBayComponent', () => {
  let component: InspectionBayComponent;
  let fixture: ComponentFixture<InspectionBayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionBayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionBayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
