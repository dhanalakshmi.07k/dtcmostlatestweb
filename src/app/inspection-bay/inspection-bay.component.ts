import { Component, OnInit, OnDestroy } from '@angular/core';
import * as _ from "lodash";
import {ConfigService} from "../../services/config.service";
import {InspectionBayService} from "../../services/inspectionBay.service";
import {AssetGroupService} from "../../services/asset.group.service";
import {AssetService} from '../../services/asset.service';
import {SocketService} from "../../services/socket.service";
import {SearchService} from '../../services/search.service';
import { Subscription } from 'rxjs';
import {AssetConfigService} from '../../services/asset-config.service';

@Component({
  selector: 'app-inspection-bay',
  templateUrl: './inspection-bay.component.html',
  styleUrls: ['./inspection-bay.component.scss']
})
export class InspectionBayComponent implements OnInit, OnDestroy {
  public isShowAsset: boolean;
  public showSlider: boolean;
  public carDetails: any;
  public isAssetDetailsInReadMode: boolean;
  public assetLinkedDetails: any;
  public assetConfigDetails: any;
  public randomNumberToSetData: number;
  private webSocketSub: Subscription;
  constructor(public configService: ConfigService,
              public inspectionBayService: InspectionBayService,
              public assetGroupService: AssetGroupService,
              private socketService: SocketService,
              private assetService: AssetService,
              public assetConfigService: AssetConfigService,
              private searchService: SearchService) {
    this.isShowAsset = false;
    this.showSlider = false;
    this.isAssetDetailsInReadMode = true;
    this.webSocketSub = this.socketService.getActiveInspectionDataObservable().subscribe(activeData => {
      this.setActiveAssetComponent(activeData);
    });
  }
  public selectedInspectedCarIndividualComponentDetails: any;
  public showReportComponentDetails = {
    'showEngineDiagnosticDetails' : false,
    'showUvisDetails' : false,
    'showAvddsDetails' : false,
    'showDriverFeedback': false
  };
  public inspectionDetails = {
    'activeDetails': {
      car: null,
      rfId: null,
      beacons: [],
      uvis: null,
      avdds: null,
      engineInspection: null,
      inspectionTime: 0,
      components: {
        found: [],
        notFound: [],
      }
    }, pagination: {
      'currentPage' : 1,
      'totalNoOfRecords': 0,
      'recordsPerPage' : 5
    }, assetGroups: {
      all: []
    }
  };
  public historicalInspectionList: any = [];
  public allGroup: any = [];
  ngOnInit() {
    this.isShowAsset = true;
    this.searchService.showSearchBar(false);
    this.clearActiveDetails();
    this.getAssetGroups();
    this.getTotalCount();
    this.assetConfigService.getAssetsConfig()
      .then(assetConfig => {
        this.assetConfigDetails = assetConfig;
      });
  }
  ngOnDestroy() {
    this.webSocketSub.unsubscribe();
  }
  getAssetGroups() {
    this.assetGroupService.get().subscribe(allGroups => {
      this.allGroup = allGroups;
      this.getActiveAsset();
    });
  }
  clearActiveDetails(){
  this.inspectionDetails.activeDetails.car = null;
  this.inspectionDetails.activeDetails.rfId = null;
  this.inspectionDetails.activeDetails.beacons = [];
  this.inspectionDetails.activeDetails.inspectionTime = 0;
  this.inspectionDetails.activeDetails.components.found = [];
  this.inspectionDetails.activeDetails.components.notFound = [];
}
  getActiveAsset() {
    this.inspectionBayService.getActive()
    .subscribe(activeData => {
      this.setActiveAssetComponent(activeData);
    });
}

  private setActiveAssetComponent(activeData) {
    if (!activeData['car']) {
      this.getTotalCount();
    }
    this.inspectionDetails['activeDetails'].car = activeData['car'];
    this.inspectionDetails['activeDetails'].rfId = activeData['rfId'];
    this.inspectionDetails['activeDetails'].beacons = activeData['beacons'];
    if (activeData['uvis']) {
      this.inspectionDetails['activeDetails'].uvis = activeData['uvis'];
    } else if (activeData['uvs']) {
      this.inspectionDetails['activeDetails'].uvis = activeData['uvs'];
    } else {
      this.inspectionDetails['activeDetails'].uvis = null;
    }
    if (activeData['avdds']) {
      this.inspectionDetails['activeDetails'].avdds = activeData['avdds'];
    } else if (activeData['avds']) {
      this.inspectionDetails['activeDetails'].avdds = activeData['avds'];
    } else {
      this.inspectionDetails['activeDetails'].avdds = null;
    }
    if (activeData['engineInspection']) {
      this.inspectionDetails['activeDetails'].engineInspection = activeData['engineInspection'];
    } else {
      this.inspectionDetails['activeDetails'].engineInspection = null;
    }
    this.inspectionDetails['activeDetails'].inspectionTime = activeData['inspectionTime'];
    this.setActiveComponents(activeData['beacons']);
  }
  getAllInspectedAsset(currentPage) {
    this.isShowAsset = true;
  let recordsPerPage;
  let skip;
    if (currentPage === 1) {
      skip = currentPage - 1;
      recordsPerPage = currentPage * this.inspectionDetails.pagination.recordsPerPage;
    } else {
      skip = (currentPage - 1) * this.inspectionDetails.pagination.recordsPerPage;
      recordsPerPage = this.inspectionDetails.pagination.recordsPerPage;
    }

  this.inspectionBayService.getAllMinifiedReportData(skip, recordsPerPage)
    .subscribe(historicalList => {
      this.isShowAsset = false;
      this.historicalInspectionList = historicalList;
      _.forEach(this.historicalInspectionList, function(value) {
        value.groups = _.uniqBy(value.groups, '_id');
      });
    });
}
  getTotalCount() {
  this.inspectionBayService.getCount()
    .subscribe(response => {
      this.inspectionDetails.pagination.totalNoOfRecords = response['count'];
      this.getAllInspectedAsset(this.inspectionDetails.pagination.currentPage);
    });
}
  setActiveComponents(beaconsList) {
    let allGroups = [];
    if (beaconsList && beaconsList.length > 0) {
      let componentLabel = [];
      _.each(beaconsList, function(beacon, index) {
        if (beacon.groups) {
          allGroups = _.union(allGroups, beacon.groups);
        }
      });
      let groupAllEquipmentsByLabel = _.groupBy(this.allGroup, 'label');
      let allLabel = _.uniq(_.keys(groupAllEquipmentsByLabel));
      let allFoundComponentLabel = _.uniq(_.keys(_.groupBy(allGroups, 'label')));
      this.inspectionDetails['activeDetails'].components.found = _.map(allFoundComponentLabel, function(label) {
        return groupAllEquipmentsByLabel[label][0];
      });
      let labelsNotFound =  _.uniq(_.difference(allLabel, allFoundComponentLabel));
      this.inspectionDetails['activeDetails'].components.notFound = _.map(labelsNotFound, function(label) {
        return groupAllEquipmentsByLabel[label][0];
      });
    } else {
      this.inspectionDetails['activeDetails'].components.found = [];
    }
}
  finishInspection() {
    if (this.inspectionDetails.activeDetails.car) {
      this.inspectionBayService.stopInspection()
        .subscribe(response => {
          if (!response['msg']) {
            this.historicalInspectionList.shift(response);
            _.slice(this.historicalInspectionList, this.historicalInspectionList.length-1);
          }
        });
    }
  }

  reportComponentDetailsToView(inspectedCarDetails) {
    this.showReportComponentDetails = {
      'showEngineDiagnosticDetails' : false,
      'showUvisDetails' : false,
      'showAvddsDetails' : false,
      'showDriverFeedback': false
    };
    if (inspectedCarDetails.component === 'engineInspection') {
      this.showReportComponentDetails.showEngineDiagnosticDetails = true;
      this.inspectionBayService.getEngineDiagnosticDetails(inspectedCarDetails)
        .subscribe(data => {
          data['car'] = inspectedCarDetails['car'];
          this.selectedInspectedCarIndividualComponentDetails = data;
        });
    } else if (inspectedCarDetails.component === 'uvis') {
      this.showReportComponentDetails.showUvisDetails = true;
      this.inspectionBayService.getUvisDetails(inspectedCarDetails)
        .subscribe(data => {
          data['car'] = inspectedCarDetails['car'];
          this.selectedInspectedCarIndividualComponentDetails = data;
        });
    } else if (inspectedCarDetails.component === 'avdds') {
      this.showReportComponentDetails.showAvddsDetails = true;
      this.inspectionBayService.getAvddsDetails(inspectedCarDetails)
        .subscribe(data => {
          data['car'] = inspectedCarDetails['car'];
          this.selectedInspectedCarIndividualComponentDetails = data;
        });
    } else if (inspectedCarDetails.component === 'driverFeedBack') {
      this.showReportComponentDetails.showDriverFeedback = true;
      this.selectedInspectedCarIndividualComponentDetails = inspectedCarDetails;
      this.randomNumberToSetData = Math.random();
    } else if (inspectedCarDetails.component === 'carDetails') {
      this.showSlider = true;
      this.assetService.getAssetDetailsByMongodbId(inspectedCarDetails.car._id)
        .subscribe(data => {
          let carDetails;
          carDetails = data;
          this.carDetails = carDetails;
          this.assetLinkedDetails = carDetails.assetsLinked;
          this.isAssetDetailsInReadMode = true;
        });
    }
  }

}
