import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-asset-card-exception',
  templateUrl: './asset-card-exception.component.html',
  styleUrls: ['./asset-card-exception.component.scss']
})
export class AssetCardExceptionComponent implements OnChanges, OnInit {
  @Input() assetData: any;
  @Input() isSelectAllClicked: boolean;
  @Output() deregisterExceptionAssetData: EventEmitter<any> = new EventEmitter<any>();
  public embossedFlag: boolean;
  constructor() {
    this.embossedFlag = false;
  }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.assetData.selected === true) {
      this.embossedFlag = true;
    } else {
      this.embossedFlag = false;
    }
  }

  deregisterExceptionAsset() {
      this.embossedFlag = !this.embossedFlag;
      let obj = {};
      obj['embossedFlag'] = this.embossedFlag;
      obj['assetData'] = this.assetData;
      this.deregisterExceptionAssetData.emit(obj);
  }
}
