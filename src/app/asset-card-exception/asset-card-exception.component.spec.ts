import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetCardExceptionComponent } from './asset-card-exception.component';

describe('AssetCardExceptionComponent', () => {
  let component: AssetCardExceptionComponent;
  let fixture: ComponentFixture<AssetCardExceptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetCardExceptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetCardExceptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
