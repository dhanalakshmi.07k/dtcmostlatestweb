/*export const admin = [
  { subject: 'all', actions: 'manage' },
  { subject: 'ServicesComponent', actions: 'manage', inverted: true }
];

export const superadmin = [
  { subject: 'all', actions: 'manage' }
];

export const operator = [
  { subject: 'all', actions: 'manage' },
  { subject: 'AssetListComponent', actions: 'manage', inverted: true },
  { subject: 'NearableComponentComponent', actions: 'manage', inverted: true },
  { subject: 'ServicesComponent', actions: 'manage', inverted: true },
  { subject: 'SettingsComponent', actions: 'manage', inverted: true },
];*/

import {AbilityBuilder} from '@casl/ability';

export const admin = AbilityBuilder.define((can, cannot) => {
 can('manage', 'all');
 cannot('manage', 'ServicesComponent');
 cannot('manage', 'UserManagementComponent');
 });

export const superadmin = AbilityBuilder.define((can, cannot) => {
 can('manage', 'all');
 });

export const operator = AbilityBuilder.define((can, cannot) => {
 can('manage', 'all');
 cannot('manage', 'AssetListComponent');
 cannot('manage', 'NearableComponentComponent');
 cannot('manage', 'ServicesComponent');
 cannot('manage', 'SettingsComponent');
 cannot('manage', 'UserManagementComponent');
 });

