import {Component, OnInit} from '@angular/core';
import {AssetConfigService} from '../../services/asset-config.service';
import { FileUploader } from 'ng2-file-upload';
import {ConfigService} from '../../services/config.service';
import {SearchService} from '../../services/search.service';
import { Subject, Actions} from '../../auth/rules';
import * as _ from 'lodash';
declare var $: any;

@Component({
  selector: 'app-upload-asset-data',
  templateUrl: './upload-asset-data.component.html',
  styleUrls: ['./upload-asset-data.component.scss']
})

export class UploadAssetDataComponent implements OnInit {
  public assetlistForBulkUpload: any = [];
  public selectedAssetType: string;
  public selectedAssetLabel: string;
  public showSampleFile: boolean;
  public assetConfigDetails: any;
  public responseMessage: any = [];
  public fileName: string;
  SUBJECT = Subject;
  ACTIONS = Actions;
  public uploader: FileUploader;
  constructor(private configService: ConfigService, private assetConfigService: AssetConfigService, private searchService: SearchService) {
    this.selectedAssetType = '';
    this.selectedAssetLabel = 'Select asset type';
    this.fileName = '';
    this.showSampleFile = false;
    this.uploader = new FileUploader({url: this.configService.appConfig.appBaseUrl + 'assetBulkUpload?type=' + this.selectedAssetType,
      authToken: sessionStorage.getItem('token'), itemAlias: 'file', queueLimit: 1});
}

  ngOnInit() {
    this.searchService.showSearchBar(false);
    this.assetConfigService.getAssetsConfig()
      .then(assetConfig => {
        this.assetConfigDetails = assetConfig;
        this.assetlistForBulkUpload = this.filterAssets(this.assetConfigDetails);
      });
    // override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    // overide the onCompleteItem property of the uploader so we are
    // able to deal with the server response.
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      if (status === 200) {
        if (response) {
          let obj: any;
          obj = JSON.parse(response);
          let responseMessage: any = [];
          if (obj.msg) {
            responseMessage.push(obj.msg);
            this.responseMessage = responseMessage;
            this.showMessagePopup('uploadedSuccessfully');
          } else if (obj.err) {
            responseMessage.push(obj.err);
            this.responseMessage = responseMessage;
            document.getElementById('bulkUploadErrorMsgBtn').click();
          }
        }
      }
    };
    this.uploader.onCompleteAll = () => {
      console.log('uploaded ALL:');
      this.uploader.clearQueue();
      $('#file').val('');
       this.fileName = '';
    };
  }
  filterAssets(assetlistForBulkUpload) {
    let filtedAssets: any = [];
    _.forEach(assetlistForBulkUpload, function(value) {
      if (value.config && value.config.isBulkUpload) {
        filtedAssets.push(value);
      }
    });
    return filtedAssets;
  }
  fileChangeListener(event) {
    this.fileName = event.target.files[0].name;
  }
  setSelectedAssetType(asset) {
    this.selectedAssetType = asset['type'];
    this.selectedAssetLabel = asset['label'];
    this.showSampleFile = true;
    this.uploader['options']['url'] = this.configService.appConfig.appBaseUrl + 'assetBulkUpload?type=' + this.selectedAssetType;
  }
  downloadSampleFile() {
    window.open(this.configService.appConfig.appBaseUrl + 'assetBulkUpload/sample?type=' + this.selectedAssetType, '_blank');
  }
  showMessagePopup(popup) {
    $('.' + popup).fadeIn(300);
    setTimeout(() => {
      $('.' + popup).fadeOut(300);
    }, 3000);
  }
}
