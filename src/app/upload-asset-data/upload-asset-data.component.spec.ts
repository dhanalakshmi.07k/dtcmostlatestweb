import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadAssetDataComponent } from './upload-asset-data.component';

describe('UploadAssetDataComponent', () => {
  let component: UploadAssetDataComponent;
  let fixture: ComponentFixture<UploadAssetDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadAssetDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadAssetDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
