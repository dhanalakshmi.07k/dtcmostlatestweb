import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material';
import {InspectionBayService} from "../../services/inspectionBay.service";
import {SearchService} from '../../services/search.service';
import {AssetService} from '../../services/asset.service';
import {SocketService} from "../../services/socket.service";
import { Subscription } from 'rxjs';
import {AssetConfigService} from '../../services/asset-config.service';
import * as _ from "lodash";
declare var $: any;

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit, OnDestroy {
  public historicalInspectionList: any = [];
  public isShowAsset;
  public showSlider: boolean;
  public isAssetDetailsInReadMode: boolean;
  public selectedReportDetails: any;
  public selectedInspectedCarIndividualComponentDetails: any;
  public pageNumber: any;
  public searchForReport: string = '';
  private searchOperationDone: boolean;
  private searchPlaceholder: string;
  private inputArr: any = [];
  public assetConfigDetails: any;
  public carDetails: any;
  public assetLinkedDetails: any;
  public randomNumberToSetData: number;
  private searchTextSub: Subscription;
  private webSocketSub: Subscription;
  public showReportComponentDetails = {
    'showEngineDiagnosticDetails' : false,
    'showUvisDetails' : false,
    'showAvddsDetails' : false,
    'showDriverFeedback': false
  }
  public inspectionDetails = {
     pagination:{
      'currentPage' : 1,
      'totalNoOfRecords': 0,
      'recordsPerPage' : 10,
      'pageCountValue' : 0,
    },assetGroups:{
      all:[]
    }
  }
  constructor(public inspectionBayService: InspectionBayService,
              private searchService: SearchService,
              private assetService: AssetService,
              private socketService: SocketService,
              public assetConfigService: AssetConfigService
  ) {
    this.pageNumber = 1;
    this.isShowAsset = false;
    this.searchOperationDone = false;
    this.isAssetDetailsInReadMode = true;
    this.showSlider = false;
    this.searchPlaceholder = 'Search Report';
    this.webSocketSub = this.socketService.getActiveInspectionDataObservable().subscribe(activeData => {
      this.setActiveAssetComponent(activeData);
    });
  }

  @ViewChild('paginator') paginator: MatPaginator;
  ngOnInit() {
    this.isShowAsset = true;
    this.doSearchTextResetAndOtherOperations();
    this.getTotalCount();
    this.assetConfigService.getAssetsConfig()
      .then(assetConfig => {
        this.assetConfigDetails = assetConfig;
      });
    this.searchTextSub = this.searchService.searchText.subscribe(event => {
      this.searchForReport = event;
      let eventLength;
      eventLength = event.length;
      if (eventLength > 3) {
        this.searchOperationDone = true;
        this.getSearchedReportPaginationCount(this.searchForReport);
      } else {
        if (this.searchOperationDone) {
          this.setPaginatorPageIndexToZero();
          this.searchOperationDone = false;
          this.getTotalCount();
        }
      }
    });
  }

  private doSearchTextResetAndOtherOperations() {
    this.searchService.showSearchBar(true);
    this.searchService.resetSearchTextInSearchBox();
    this.searchService.sendSearchText('');
    this.searchService.sendSearchPlaceholderValue(this.searchPlaceholder);
  }

  private setActiveAssetComponent(activeData) {
    if (!activeData['car']) {
      this.setPaginatorPageIndexToZero();
      this.getTotalCount();
    }
  }


  getSearchedReportPaginationCount(searchText) {
    this.isShowAsset = true;
    this.inspectionBayService.getSearchedReportPaginationCount(searchText)
      .subscribe(response => {
        this.isShowAsset = false;
        this.inspectionDetails.pagination.totalNoOfRecords = response['count'];
        if (this.inspectionDetails.pagination.totalNoOfRecords > 0) {
          this.setPaginatorPageIndexToZero();
          this.getPageCount();
          let obj: any = {};
          obj.pageIndex = 0;
          this.getAllInspectedAsset(obj);
        } else if (this.inspectionDetails.pagination.totalNoOfRecords === 0) {
          this.historicalInspectionList = [];
        }
      });
  }

  getReportsOnSearch(searchText, skip, limit) {
    this.inspectionBayService.getReportsOnSearch(searchText, skip, limit)
      .subscribe(reports => {
        this.isShowAsset = false;
        this.historicalInspectionList = reports;
        _.forEach(this.historicalInspectionList, function(value) {
          value.groups = _.uniqBy(value.groups, '_id');
        });
      });
  }

  ngOnDestroy() {
    this.searchTextSub.unsubscribe();
    this.webSocketSub.unsubscribe();
  }
  setPaginatorPageIndexToZero() {
    if (this.paginator) {
      this.paginator.pageIndex = 0;
    }
  }
  getAllInspectedAsset(currentPage) {
    this.isShowAsset = true;
    let recordsPerPage;
    let skip;
    this.pageNumber = currentPage.pageIndex + 1;
    /*if(currentPage==1){
     skip=currentPage-1;
     recordsPerPage=currentPage*this.inspectionDetails.pagination.recordsPerPage;
     }else{
     skip=(currentPage-1)*this.inspectionDetails.pagination.recordsPerPage;
     recordsPerPage=this.inspectionDetails.pagination.recordsPerPage;
     }*/
    if (currentPage.pageIndex === 0) {
      skip = 0;
      recordsPerPage = this.inspectionDetails.pagination.recordsPerPage;
    } else {
      skip = (currentPage.pageIndex) * this.inspectionDetails.pagination.recordsPerPage;
      recordsPerPage = this.inspectionDetails.pagination.recordsPerPage;
    }
    if (this.searchOperationDone) {
      this.getReportsOnSearch(this.searchForReport, skip, recordsPerPage);
    } else {
      this.inspectionBayService.getAllMinifiedReportData(skip, recordsPerPage)
        .subscribe(historicalList => {
          this.isShowAsset = false;
          this.historicalInspectionList = historicalList;
          _.forEach(this.historicalInspectionList, function(value) {
            value.groups = _.uniqBy(value.groups, '_id');
          });
        });
    }
  }

  getTotalCount() {
    this.isShowAsset = true;
    this.inspectionBayService.getCount()
      .subscribe(response => {
        this.isShowAsset = false;
        this.inspectionDetails.pagination.totalNoOfRecords = response["count"];
        let obj: any = {};
        obj.pageIndex = 0;
        this.getPageCount();
        this.getAllInspectedAsset(obj);
      });
  }
  getPageCount() {
    this.inputArr = [];
    this.pageNumber = 1;
    this.inspectionDetails.pagination.pageCountValue = Math.ceil((this.inspectionDetails.pagination.totalNoOfRecords) / 10);
    for (let i = 1; i <= this.inspectionDetails.pagination.pageCountValue; i++) {
      this.inputArr[i] = i;
    }
  }

  pdfViewer(inspectedCarDetails) {
    this.inspectionBayService.getEntireReportDetails(inspectedCarDetails)
      .subscribe(entireReport => {
        this.selectedReportDetails = entireReport;
        console.log('this.selectedReportDetails', this.selectedReportDetails);
      });
    $('html').css('overflow-y', 'hidden');
  }

  generatePdf(inspectedCarDetails) {
    this.inspectionBayService.getReportById(inspectedCarDetails._id);
  }

  reportComponentDetailsToView(inspectedCarDetails) {
    this.showReportComponentDetails = {
      'showEngineDiagnosticDetails' : false,
      'showUvisDetails' : false,
      'showAvddsDetails': false,
      'showDriverFeedback': false
    };
    if (inspectedCarDetails.component === 'engineInspection') {
      this.showReportComponentDetails.showEngineDiagnosticDetails = true;
      this.inspectionBayService.getEngineDiagnosticDetails(inspectedCarDetails)
        .subscribe(data => {
          data['car'] = inspectedCarDetails['car'];
          this.selectedInspectedCarIndividualComponentDetails = data;
        });
    } else if (inspectedCarDetails.component === 'uvis') {
      this.showReportComponentDetails.showUvisDetails = true;
      this.inspectionBayService.getUvisDetails(inspectedCarDetails)
        .subscribe(data => {
          data['car'] = inspectedCarDetails['car'];
          this.selectedInspectedCarIndividualComponentDetails = data;
        });
    } else if (inspectedCarDetails.component === 'avdds') {
      this.showReportComponentDetails.showAvddsDetails = true;
      this.inspectionBayService.getAvddsDetails(inspectedCarDetails)
        .subscribe(data => {
          data['car'] = inspectedCarDetails['car'];
          this.selectedInspectedCarIndividualComponentDetails = data;
        });
    } else if (inspectedCarDetails.component === 'driverFeedBack') {
      this.showReportComponentDetails.showDriverFeedback = true;
      this.selectedInspectedCarIndividualComponentDetails = inspectedCarDetails;
      this.randomNumberToSetData = Math.random();
    } else if (inspectedCarDetails.component === 'carDetails') {
      this.showSlider = true;
      this.assetService.getAssetDetailsByMongodbId(inspectedCarDetails.car._id)
        .subscribe(data => {
          let carDetails;
          carDetails = data;
          this.carDetails = carDetails;
          this.assetLinkedDetails = carDetails.assetsLinked;
          this.isAssetDetailsInReadMode = true;
        });
    }
  }
  public updatePageNumber(index: number): void {
    this.pageNumber = index;
    this.paginator.pageIndex = index - 1;
    this.paginator.page.next({
      pageIndex: this.paginator.pageIndex,
      pageSize: this.paginator.pageSize,
      length: this.paginator.length
    });
  }
}
