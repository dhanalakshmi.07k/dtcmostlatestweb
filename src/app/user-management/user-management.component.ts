import { Component, OnInit } from '@angular/core';
import {SearchService} from '../../services/search.service';
import {UserManagementService} from '../../services/userManagement.service';
import {MyProfileManagementService} from '../../services/my-profile-management.service';
import {UserProfile} from '../user-profile/user-profile-type';
import {ability} from '../../auth/ability';
import { Subject, Actions} from '../../auth/rules';
declare var $ :any;
@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss'],
})
export class UserManagementComponent implements OnInit {

  public isBuildUserFormInitiated: boolean;
  public showLoader: boolean;
  public showCircularLoaderInEditForm: boolean;
  public isUserUpdateSuccess: boolean;
  public userIdToDelete: string;
  public usernameToDelete: string;
  public userIdToEdit: string;
  public actionResultMsg: string;
  public userDetailsToEdit: UserProfile;
  public userProfileDetails: UserProfile;
  ACTIONS = Actions;
  SUBJECT = Subject;
  formConfigData: Array<Object>;
  roles: Array<string>;

  displayedColumns: string[] = ['username', 'firstName', 'lastName', 'roles', 'email', 'edit' , 'delete'];
  dataSource: PeriodicElement[];

  constructor(private searchService: SearchService, private userManagementService: UserManagementService, private myProfileManagementService: MyProfileManagementService) {
    this.isBuildUserFormInitiated = false;
    this.isUserUpdateSuccess = false;
    this.dataSource = [];
    this.roles = [];
  }

  ngOnInit() {
    this.searchService.showSearchBar(false);
    this.getUserList();
    this.getRoles();
    this.getUserProfileDetails();
  }

  getUserList() {
    this.showLoader = true;
    this.userManagementService.getUsers()
      .subscribe((users: PeriodicElement[]) => {
        this.showLoader = false;
        this.dataSource = users;
      });
  }
  getUserProfileDetails() {
    this.myProfileManagementService.getUserProfile()
      .subscribe((profileDetails: UserProfile) => {
        this.userProfileDetails = profileDetails;
      });
  }
  getRoles() {
    this.userManagementService.getRoles()
      .subscribe((roles: Array<string>) => {
        this.roles = roles;
      });
  }
  openModal() {
    this.isBuildUserFormInitiated = !this.isBuildUserFormInitiated;
  }

  saveUser(userDetails) {
    this.userManagementService.saveUserDetails(userDetails)
      .subscribe(res => {
        this.actionResultMsg = '';
          if (res && res['msg']) {
            this.actionResultMsg = res['msg'];
          }
        this.showMessagePopup('savedUserSuccessfully');
        this.getUserList();
        $('#user-create-modal').modal('hide');
      });
  }

  updateUserDetails(userDetails) {
    this.isUserUpdateSuccess = false;
    this.userManagementService.updateUserDetails(this.userIdToEdit, userDetails)
      .subscribe(res => {
        if (this.userProfileDetails._id === this.userIdToEdit) {
          this.myProfileManagementService.sendUserProfileDetails(res);
        }
        this.isUserUpdateSuccess = true;
        this.showMessagePopup('userUpdatedSuccessfully');
        this.getUserList();
        $('#user-edit-modal').modal('hide');
      });
  }

  editUser(userDetails) {
    this.showCircularLoaderInEditForm = true;
    this.userIdToEdit = userDetails._id;
    this.userManagementService.getUserData(this.userIdToEdit)
      .subscribe((userData: UserProfile) => {
        this.userDetailsToEdit = userData;
        this.showCircularLoaderInEditForm = false;
      });
  }

  deleteUser(userDetails) {
    this.userIdToDelete = userDetails._id;
    this.usernameToDelete = userDetails.username;
  }

  deleteUserConfirmed(status) {
    if (status === 'OK') {
      this.userManagementService.deleteUser(this.userIdToDelete)
        .subscribe(res => {
          this.actionResultMsg = '';
          if (res && res['msg']) {
            this.actionResultMsg = res['msg'];
          }
          this.showMessagePopup('deleteUserSuccessfully');
          this.getUserList();
        });
    }
  }

  canDeleteRoles(role) {
    let canDelete: boolean;
    canDelete = false;
    if ( ability.can( Actions.DELETE, Subject.USER)) {
      if (ability.can( role, Subject.DELETE_ROLE)) {
        canDelete = true;
      }
    }
    return canDelete;
  }

  canEditRoles(role) {
    let canDelete: boolean;
    canDelete = false;
    if ( ability.can(Actions.UPDATE, Subject.USER)) {
      if (ability.can(role, Subject.UPDATE_ROLE )) {
        canDelete = true;
      }
    }
    return canDelete;
  }

  showMessagePopup(popup) {
    $('.' + popup).fadeIn(300);
    setTimeout(() => {
      $('.' + popup).fadeOut(300);
    }, 3000);
  }

}

export interface PeriodicElement {
  created: string;
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  roles: Array<string>;
  updated: string;
  username: string;
  _id: string;
}
