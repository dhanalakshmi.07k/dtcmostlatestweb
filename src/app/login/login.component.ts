import { Component, OnInit, OnDestroy } from '@angular/core';
import {Router} from "@angular/router";
import {LoginServiceService} from "../../services/login-service.service";
import {AssetConfigService} from '../../services/asset-config.service';
import { ValidityCheckService } from '../../services/validity-check.service';
import {ConfigService} from '../../services/config.service';
import {SetCssPropertiesService} from '../../services/set-css-properties.service';
import {MyProfileManagementService} from '../../services/my-profile-management.service';
import { Subscription } from 'rxjs';
declare var $: any;
import { unpackRules } from '@casl/ability/extra';

import {ability} from '../../auth/ability';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginStatus: string;
  isShowAsset: boolean;
  currRoute: string;
  email: string;
  password: string;
  passwordChangedMsg: string;
  public iotzenLogoSrcUrl: string;
  public licenceValidityMessage: string;
  private isPasswordChangedValueSub: Subscription;

  constructor(private router: Router, public loginServiceService: LoginServiceService, public assetConfigService: AssetConfigService,
              private validityCheckService: ValidityCheckService, private configService: ConfigService, private setCssPropertiesService: SetCssPropertiesService,
              private myProfileManagementService: MyProfileManagementService) {
    this.currRoute = router.url;
    this.iotzenLogoSrcUrl = this.configService.appConfig.iotzenLogoUrl;
    this.licenceValidityMessage = '';
    this.passwordChangedMsg = '';
  }

  ngOnInit() {
    this.validityCheckService.getValidityStatus()
      .subscribe(validityData => {
        if (validityData['enableSoftWarning']) {
          this.licenceValidityMessage = validityData['message'];
          this.validityCheckService.setValidityMessage(validityData['message']);
        } else {
          this.validityCheckService.setValidityMessage('');
        }
      });

    this.isPasswordChangedValueSub = this.myProfileManagementService.isPasswordChanged.subscribe((isPasswordChanged: boolean) => {
      if (isPasswordChanged) {
        this.passwordChangedMsg = 'Please login with new password';
      }
    });

    if (this.currRoute === '/login') {
      $('#content').addClass('login-header');
      $('#side-bar-grid').addClass('login-header');
      $('#header-container').addClass('login-header');
      $('#main-container').addClass('login-header');
    }
  }

  ngOnDestroy() {
    this.isPasswordChangedValueSub.unsubscribe();
  }

  login() {
    this.isShowAsset = true;
    let obj = {};
    obj['username'] = this.email;
    obj['password'] = this.password;
    this.loginServiceService.verifyUserDetails(obj)
      .subscribe((res: any) => {
          this.myProfileManagementService.passwordChanged(false);
          ability.update([]);
          this.assetConfigService.resetAssetsConfigValue();
          this.assetConfigService.resetServiceAssetsConfigValue();
          let userRoles: any;
          userRoles = res.roles;
          this.setRules(unpackRules(res.roleInfo.ability), res.roleInfo.landingPage);
          sessionStorage.setItem('token', res.token);
          sessionStorage.setItem('userRoles', JSON.stringify(userRoles));
          let user: any;
          user = userRoles.toString();
          this.isShowAsset = false;
          this.myProfileManagementService.sendUserProfileDetails();
          this.setCssPropertiesService.setCssClasses();
        },
        err => {
          this.loginStatus = err.error;
          this.isShowAsset = false;
        }
      );
  }
  setRules(abilityFound, landingPage) {
    ability.update(abilityFound);
    sessionStorage.setItem('rules', JSON.stringify(ability.rules));
    this.router.navigate([landingPage]);
  }

}
