import { Component, Input, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-count-card',
  templateUrl: './count-card.component.html',
  styleUrls: ['./count-card.component.scss']
})
export class CountCardComponent implements OnChanges, OnInit {
  @Input() dashData: any;
  @Input() selectedAssetTypesForFilter: any;
  @Output() filterAssetType: EventEmitter<string> = new EventEmitter();
  @Output() embossedFlagStatus: EventEmitter<boolean> = new EventEmitter();
  icon: string;
  embossedFlag:boolean = false;
  showCloseBtn:boolean = false;
  constructor() {}

  /*getBackgroundColorLeftCard(type) {
    switch (type) {
      case 'car':
        return '#7986CB';
      case 'obdDevice':
        return '#4DB6AC';
      case 'driver':
        return '#81C784';
      case 'beacon':
        return '#64B5F6';
      case 'rfId':
        return '#d27f72c2';
      case 'beaconGateway':
        return '#ce84ba';
    }
  }

  getBackgroundColorRightCard(type) {
    switch (type) {
      case 'car':
        return '#5C6BC0';
      case 'obdDevice':
        return '#26A69A';
      case 'driver':
        return '#66BB6A';
      case 'beacon':
        return '#42A5F5';
      case 'rfId':
        return '#dc786b';
      case 'beaconGateway':
        return '#d05fb1';
    }
  }*/

  getIcon(type) {
    switch (type) {
      case 'car':
        return '../../assets/asset-icons/taxi-white.png';
      case 'obdDevice':
        return '../../assets/asset-icons/obd-white.png';
      case 'driver':
        return '../../assets/asset-icons/driver-white.png';
      case 'beacon':
        return '../../assets/asset-icons/beacon-white.png';
      case 'rfId':
        return '../../assets/asset-icons/rfId-white.png';
      case 'beaconGateway':
        return '../../assets/asset-icons/beaconGateWay.png';
      case 'rfidGateway':
        return '../../assets/asset-icons/rfIdGateWay.png';
      case 'server':
        return '../../assets/asset-icons/server-white.png';
    }
  }
  ngOnChanges() {
    if (this.dashData.selectedForFiler === true) {
      this.dashData.selectedForFiler = false;
    } else {
      this.embossedFlag = false;
    }
  }

  ngOnInit() {
    this.icon = this.getIcon(this.dashData.type);
    let selectedAssetTypesToFilter;
    selectedAssetTypesToFilter = this.selectedAssetTypesForFilter.toString();
    if (this.dashData.type === selectedAssetTypesToFilter) {
      this.embossedFlag = true;
    }
  }

  filterAssetsOnType(asset) {
    asset.selectedForFiler = true;
    if (asset.selectedForFiler) {
      this.embossedFlag = !this.embossedFlag;
    }
    this.embossedFlagStatus.emit(this.embossedFlag);
    this.filterAssetType.emit(asset);
  }

}
