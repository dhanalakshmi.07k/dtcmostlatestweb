import { Component, OnInit, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import {Router} from '@angular/router';
import {SearchService} from '../../../services/search.service';
import {MyProfileManagementService} from '../../../services/my-profile-management.service';
import { Subscription } from 'rxjs';
import {ability} from '../../../auth/ability';
import {UserProfile} from '../../user-profile/user-profile-type';
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnChanges, OnInit, OnDestroy {
  currRoute: string;
  public resetSearchTextToSearchBox: any;
  public showSearchBox: boolean;
  private isSearchBarNeededSub: Subscription;
  private userProfileDetailsSub: Subscription;
  public userDetails: UserProfile;
  public roleAlphabet: string;
  @Input() resetSearchTextToHeader: any;
  @Input() licenceValidityMessage: string;
  constructor(public router: Router, private  searchService: SearchService, private myProfileManagementService: MyProfileManagementService) {
    this.currRoute = router.url;
    this.showSearchBox = true;
    this.licenceValidityMessage = '';
  }

  ngOnInit() {
    this.isSearchBarNeededSub = this.searchService.isSearchBarNeeded.subscribe((isSearchBarNeeded: boolean) => {
      this.showSearchBox = isSearchBarNeeded;
    });
    this.userProfileDetailsSub = this.myProfileManagementService.userProfileDetails.subscribe((userDetails) => {
      this.userDetails = userDetails;
      if (this.userDetails && this.userDetails.username) {
        this.roleAlphabet = this.getAlphabetsToDisplay(this.userDetails);
      }
    });
    if (!this.userDetails) {
      this.getUserDetails();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'resetSearchTextToHeader') {
        if (change.currentValue !== change.previousValue) {
          this.resetSearchTextToSearchBox = this.resetSearchTextToHeader;
        }
      }
    }
  }
  ngOnDestroy() {
    this.isSearchBarNeededSub.unsubscribe();
    this.userProfileDetailsSub.unsubscribe();
  }

  getUserDetails() {
    this.myProfileManagementService.getUserProfile()
      .subscribe((details: UserProfile) => {
        if (details) {
          this.userDetails = details;
          if (this.userDetails.username) {
            this.roleAlphabet = this.getAlphabetsToDisplay(this.userDetails);
          }
        }
      });
  }

  getAlphabetsToDisplay(userDetails) {
    let alphabetsToDisplay: string;
    alphabetsToDisplay = '';
    if (userDetails.firstName) {
      alphabetsToDisplay = userDetails.firstName.charAt(0);
    }
    if (userDetails.lastName) {
      alphabetsToDisplay += userDetails.lastName.charAt(0);
    }
    if (!userDetails.firstName && !userDetails.lastName) {
      if (userDetails.roles.length > 0) {
        alphabetsToDisplay = (userDetails.roles[0]).charAt(0);
      }
    }
    return alphabetsToDisplay;
  }

  navigateToMyProfilePage() {
    this.router.navigate(['settings/userProfile']);
  }

  logout() {
    this.clearSession();
    this.roleAlphabet = '';
    this.userDetails = null;
    /*clearing rules*/
    ability.update([]);
    this.router.navigate(['login']);
  }

  private clearSession() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('userRoles');
    sessionStorage.removeItem('rules');
  }
}
