/**
 * Created by chandru on 13/7/18.
 */
export const menus = [
  {
    'name'   : 'Assets',
    'icon'  : 'fa-home',
    'routerLink'   : '/asset',
  },
  {
    'name'   : 'Near-By',
    'icon'  : 'fa-bullseye',
    'routerLink'   : '/nearable',
  },
  {
    'name'   : 'Inspection-Bay',
    'icon'  : '../../assets/asset-icons/carinspection.png',
    'iconActive' : '../../assets/asset-icons/inspection_bay_active.png',
    'routerLink'   : '/inspectionBay',
  },
  {
    'name'   : 'Report',
    'icon'  : 'fa-file-pdf',
    'routerLink'   : '/report',
  },
  {
    'name'   : 'Analytics',
    'icon'  : 'fa-chart-line',
    'routerLink'   : '/analytics',
  }
]

export const settingTab = [
  {
    'name'   : 'Services',
    'icon'  : '../../assets/asset-icons/services.png',
    'iconActive' : '../../assets/asset-icons/services_active.png',
    'routerLink'   : '/services',
  },
  {
  'name'   : 'Settings',
  'icon'  : 'fa-cogs',
  'routerLink'   : '/settings',
  }
]
