import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { menus, settingTab } from './menu-element';
import { LoginServiceService } from '../../../services/login-service.service';
import {ConfigService} from '../../../services/config.service';
import {Router} from '@angular/router';
import {ability} from '../../../auth/ability';
declare var $: any;

import { Subject,Actions} from '../../../auth/rules';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {
  public menus = menus;
  public settingTab = settingTab;
  @Output() resetSearchTextValue: EventEmitter<any> = new EventEmitter();
  iotzenLogoSrcUrl: string;
  ACTIONS=Actions;
  SUBJECT=Subject;
  constructor(public loginServiceService: LoginServiceService, public configService: ConfigService, public router: Router) {
    this.iotzenLogoSrcUrl = this.configService.appConfig.iotzenLogoUrl;
  }

  ngOnInit() {
    if (sessionStorage.getItem('rules')) {
      let rules = sessionStorage.getItem('rules');
      let userRules: any;
      userRules = JSON.parse(rules);
      ability.update(userRules);
    }
    if (sessionStorage.getItem('userRoles')) {
      const user = sessionStorage.getItem('userRoles');
    }
    this.settingCssClasses();
  }

  resetSearchText() {
    this.resetSearchTextValue.emit(this.router.url);
  }

  settingCssClasses() {
    if (this.configService.appConfig.isMenuCollapse) {
      $('#sidebar').addClass('active');
    } else {
      $('#sidebar').removeClass('active');
    }
    if (this.configService.appConfig.isSubMenuCollapse) {
      $('.main-menu > a').find('.arrow-icon').addClass('fa-angle-down');
      $('.sub-menu').addClass('display-none');
    } else {
      $('.main-menu > a').find('.arrow-icon').addClass('fa-angle-up');
      $('.sub-menu').addClass('display-block');
    }
  }

}
