import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAssetListComponent } from './add-asset-list.component';

describe('AddAssetListComponent', () => {
  let component: AddAssetListComponent;
  let fixture: ComponentFixture<AddAssetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAssetListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAssetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
