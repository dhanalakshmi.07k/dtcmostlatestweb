import {Component, OnInit, OnChanges, Input, EventEmitter, Output} from '@angular/core';
declare var $:any;
import { Subject,Actions} from '../../../auth/rules';

@Component({
  selector: 'app-add-asset-list',
  templateUrl: './add-asset-list.component.html',
  styleUrls: ['./add-asset-list.component.scss']
})
export class AddAssetListComponent implements OnChanges, OnInit {
  @Input() assetTypesWithLabelsData: any;
  @Output() assetToAdd: EventEmitter<any> = new EventEmitter();
  SUBJECT=Subject;
  ACTIONS=Actions;
  constructor() {
  }

  ngOnInit() {
  }
  ngOnChanges() {
  }
  assetSelected(assetType) {
    this.assetToAdd.emit(assetType);
    $('html').css('overflow-y', 'hidden');
  }

  getIcon(type) {
    switch (type) {
      case 'car':
        return '../../assets/asset-icons/taxi-white.png';
      case 'obdDevice':
        return '../../assets/asset-icons/obd-white.png';
      case 'driver':
        return '../../assets/asset-icons/driver-white.png';
      case 'beacon':
        return '../../assets/asset-icons/beacon-white.png';
      case 'rfId':
        return '../../assets/asset-icons/rfId-white.png';
      case 'beaconGateway':
        return '../../assets/asset-icons/beaconGateWay.png';
      case 'rfidGateway':
        return '../../assets/asset-icons/rfIdGateWay.png';
      case 'server':
        return '../../assets/asset-icons/server-white.png';
      case 'general':
        return '../../assets/asset-icons/general-white.png';
      default:
        return '../../assets/asset-icons/beaconGateWay.png';
    }
  }

  /*getBackgroundColor(type) {
    switch (type) {
      case 'car':
        return '#5C6BC0';
      case 'obdDevice':
        return '#26A69A';
      case 'driver':
        return '#66BB6A';
      case 'beacon':
        return '#42A5F5';
      case 'rfId':
        return '#DC786B';
      case 'beaconGateway':
        return '#d05fb1';
    }
  }*/

}
