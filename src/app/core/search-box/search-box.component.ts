import { Component, OnInit, OnChanges, Input, OnDestroy } from '@angular/core';
import {SearchService} from '../../../services/search.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnChanges, OnInit, OnDestroy {

  public searchText: string;
  public placeholderValue: string;
  public searchedInfoValue: string;
  public showInfoIcon: boolean;
  public showSearchIcon: boolean;
  private searchPlaceholderValueSub: Subscription;
  private resetSearchTextValueSub: Subscription;
  private searchedInfoValueSub: Subscription;
  @Input() resetSearchTextToSearchBox: any;
  constructor(private searchService: SearchService) {
    this.searchText = '';
    this.placeholderValue = 'Search';
    this.showInfoIcon = true;
    this.showSearchIcon = false;
  }

  ngOnInit() {
    this.searchPlaceholderValueSub = this.searchService.searchPlaceholderValue.subscribe(placeholderValue => {
      if (placeholderValue) {
        this.placeholderValue = placeholderValue;
      }
    });
    this.resetSearchTextValueSub = this.searchService.resetSearchTextValue.subscribe(value => {
      if (value !== 0) {
        this.searchText = '';
      }
    });
    this.searchedInfoValueSub = this.searchService.searchedInfoValue.subscribe(searchedInfoValue => {
      this.searchedInfoValue = searchedInfoValue;
    });
  }
  ngOnChanges() {
    if (this.resetSearchTextToSearchBox) {
      this.showInfoIcon = false;
      this.showSearchIcon = true;
      if (this.resetSearchTextToSearchBox === '/asset') {
        this.showInfoIcon = true;
        this.showSearchIcon = false;
      }
    }
  }
  ngOnDestroy() {
    this.searchPlaceholderValueSub.unsubscribe();
    this.resetSearchTextValueSub.unsubscribe();
    this.searchedInfoValueSub.unsubscribe();
  }
  searchForSelectedText(event) {
    this.searchService.sendSearchText(event);
  }

  clearSearchedText() {
    this.searchText = '';
    this.searchService.sendSearchText(this.searchText);
  }

}
