import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchForAssetComponent } from './search-for-asset.component';

describe('SearchForAssetComponent', () => {
  let component: SearchForAssetComponent;
  let fixture: ComponentFixture<SearchForAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchForAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchForAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
