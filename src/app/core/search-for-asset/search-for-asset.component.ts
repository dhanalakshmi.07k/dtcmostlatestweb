import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import {AssetService} from "../../../services/asset.service";
import {ConfigService} from '../../../services/config.service';

declare  var $: any;
import * as _ from 'lodash';

@Component({
  selector: 'app-search-for-asset',
  templateUrl: './search-for-asset.component.html',
  styleUrls: ['./search-for-asset.component.scss']
})
export class SearchForAssetComponent implements OnChanges, OnInit {
   @Input() allAssetTypes: any;
   @Input() resetSearchedAssetOnPlusClick: number;
   @Input() assetData: any;
   @Input() resetSearchedAssetOnBackBtn: number;
   @Output() searchedAssets: EventEmitter<any> = new EventEmitter();
  searchForAsset: string = '';
  searchedAssetForLinking: any = [];
  assetTypeForSearch: string = '';
  assetLabel: string = '';
  assetNotFound: boolean = false;
  linkableAssets: any;
  public linkableAssetsLabelWithType: any;
  constructor(public assetService: AssetService, public configService: ConfigService) {
    this.linkableAssetsLabelWithType = [];
    this.linkableAssets = [];
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'assetData') {
        if (this.assetData && this.assetData.assetType) {
          if (change.currentValue !== change.previousValue) {
            this.getLinkableAssetListForAsset();
          }
        } else if (this.assetData && this.assetData.serviceType) {
          if (change.currentValue !== change.previousValue) {
            this.getLinkableAssetListForServiceAsset();
          }
        }
      }
      if (propName === 'allAssetTypes') {
        if (this.assetData && this.assetData.assetType) {
          if (change.currentValue !== change.previousValue) {
            this.getLinkableAssetListForAsset();
          }
        } else if (this.assetData && this.assetData.serviceType) {
          if (change.currentValue !== change.previousValue) {
            this.getLinkableAssetListForServiceAsset();
          }
        }
      }
      if (propName === 'resetSearchedAssetOnPlusClick') {
        if (change.currentValue !== change.previousValue) {
          this.resetSearchedAssets(true);
        }
      }
      if (propName === 'resetSearchedAssetOnBackBtn') {
        if (change.currentValue !== change.previousValue) {
          this.resetSearchedAssets(false);
        }
      }
    }
  }

  getLinkableAssetListForAsset() {
    let linkableAssetObject: any = {};
    let selectedAsset: string = '';
    selectedAsset = this.assetData.assetType ? this.assetData.assetType : '';
    if (this.allAssetTypes && this.allAssetTypes.length > 0) {
      this.linkableAssetsLabelWithType = [];
      this.linkableAssets = [];
      linkableAssetObject = _.find(this.allAssetTypes, ['type', selectedAsset]);
      this.linkableAssets = linkableAssetObject['config']['assetsLinkable'];
      this.linkableAssetsLabelWithType = this.getAssetsLabelsWithTypes(this.linkableAssets, this.allAssetTypes);
      if (this.linkableAssetsLabelWithType.length > 0) {
        this.assetLabel = this.linkableAssetsLabelWithType[0]['label'];
      }
    }
  }
  getLinkableAssetListForServiceAsset() {
    let linkableAssets: any = [];
    if (this.allAssetTypes && this.allAssetTypes.length > 0) {
      this.linkableAssetsLabelWithType = [];
      this.linkableAssets = [];
      _.forEach(this.allAssetTypes, function(value) {
        if (value && value.config && value.config.isServiceLinkable === true) {
          linkableAssets.push(value.type);
        }
      });
      this.linkableAssets = linkableAssets;
      this.linkableAssetsLabelWithType = this.getAssetsLabelsWithTypes(linkableAssets, this.allAssetTypes);
      if (this.linkableAssetsLabelWithType.length > 0) {
        this.assetLabel = this.linkableAssetsLabelWithType[0]['label'];
      }
    }
  }
  getAssetsLabelsWithTypes(linkableAssets, allAssetTypes ) {
    let labelObj: any;
    let linkableAssetsLabelWithType: any = [];
    _.forEach(linkableAssets, function(value) {
      labelObj = {};
      labelObj = _.find(allAssetTypes, function(o) { return o.type === value; });
      if (labelObj) {
        let obj: any = {};
        obj = {
          'type': labelObj.type,
          'label': labelObj.label
        };
        linkableAssetsLabelWithType.push(obj);
      }
    });
    return linkableAssetsLabelWithType;
  }

  searchAsset(event) {
    this.assetNotFound = false;
    this.searchForAsset = event;
    let urlForSearch: string;
    urlForSearch = this.assetTypeForSearch + '/' + this.searchForAsset;
    if (this.searchForAsset) {
      this.assetService.searchForAssetFromassetType(this.searchForAsset, this.assetTypeForSearch, 0, 12)
        .subscribe(searchResult => {
          this.searchedAssetForLinking = searchResult;
          if (this.searchedAssetForLinking.length === 0) {
            this.assetNotFound = true;
          }
          this.searchedAssets.emit(this.searchedAssetForLinking);
        });
    } else {
      this.searchedAssetForLinking = [];
      this.getLastTenAssetForSelectedType(this.assetTypeForSearch);
    }
  }

  getLastTenAssetForSelectedType(assetType) {
    this.assetNotFound = false;
    this.assetService.getAllByType(assetType, 0, 10)
      .subscribe(lastTenAssetOfSpecificType => {
        this.searchedAssetForLinking = lastTenAssetOfSpecificType;
        if (this.searchedAssetForLinking.length === 0) {
          this.assetNotFound = true;
        }
        this.searchedAssets.emit(this.searchedAssetForLinking);
      });
  }

  setSelectedAssetType(asset) {
    this.assetTypeForSearch = asset.type;
    this.assetLabel = asset.label;
    this.searchedAssetForLinking = [];
    this.searchForAsset = '';
    this.getLastTenAssetForSelectedType(this.assetTypeForSearch);
  }

  resetSearchedAssets(getLastTenAssets: boolean) {
    this.searchedAssetForLinking = [];
    this.searchedAssets.emit(this.searchedAssetForLinking);
    this.searchForAsset = '';
    this.assetTypeForSearch = this.linkableAssets[0] ? this.linkableAssets[0] : '';
    if (this.linkableAssetsLabelWithType.length > 0) {
      this.assetLabel = this.linkableAssetsLabelWithType[0]['label'];
    }
    if (getLastTenAssets) {
      this.getLastTenAssetForSelectedType(this.assetTypeForSearch);
    }
  }

}
