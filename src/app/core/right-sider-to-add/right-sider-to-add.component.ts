import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import {log} from "util";
import {FormService} from '../../../services/form.service';
import {AssetService} from '../../../services/asset.service';
import {ServiceLinkableService} from '../../../services/serviceLinkable.service';
declare var $:any;

@Component({
  selector: 'app-right-sider-to-add',
  templateUrl: './right-sider-to-add.component.html',
  styleUrls: ['./right-sider-to-add.component.scss']
})
export class RightSiderToAddComponent implements OnChanges, OnInit {
@Input() assetSelectedType: any;
@Input() resetAddAssetFormValue: number;
@Output() savedAssetCardToAssetList: EventEmitter<any> = new EventEmitter();
@Output() assetRegistrationErrorMsgToAssetList: EventEmitter<string> = new EventEmitter();
  public output: any;
  public assetFromConfig: any;
  public showCircularLoader: boolean;
  constructor(public assetService: AssetService, public formService: FormService, private serviceLinkableService: ServiceLinkableService) { }


  ngOnInit() {}
  ngOnChanges(changes: SimpleChanges) {
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'resetAddAssetFormValue') {
        if (change.currentValue !== change.previousValue) {
          this.assetFromConfig = [];
          this.showCircularLoader = true;
          if (this.assetSelectedType && this.assetSelectedType === 'general') {
            this.fetchConfigBasedOnServiceAssetType();
          } else {
            this.fetchConfigBasedOnType();
          }
        }
      }
    }
  }

  closePopup() {
    $('html').css('overflow-y', 'auto');
  }
  fetchConfigBasedOnType() {
    this.assetService.getAssetConfig(this.assetSelectedType)
      .subscribe((data: any) => {
        this.assetFromConfig = this.formService.formatAssetConfig(data.configuration);
        this.showCircularLoader = false;
      });
  }

  fetchConfigBasedOnServiceAssetType() {
    this.serviceLinkableService.getServiceAssetConfig(this.assetSelectedType)
      .subscribe((config: any) => {
        this.assetFromConfig = this.formService.formatAssetConfig(config.configuration);
        this.showCircularLoader = false;
      });
  }

  savedAssetCard(assetValue) {
    this.savedAssetCardToAssetList.emit(assetValue);
  }

  assetRegistrationErrorMsg(errMsg: string) {
    this.assetRegistrationErrorMsgToAssetList.emit(errMsg);
  }


}
