import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightSiderToAddComponent } from './right-sider-to-add.component';

describe('RightSiderToAddComponent', () => {
  let component: RightSiderToAddComponent;
  let fixture: ComponentFixture<RightSiderToAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightSiderToAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightSiderToAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
