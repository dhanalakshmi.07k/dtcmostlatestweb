import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-asset-linked-count',
  templateUrl: './asset-linked-count.component.html',
  styleUrls: ['./asset-linked-count.component.scss']
})
export class AssetLinkedCountComponent implements OnChanges, OnInit {
  @Input() assetLinkedDetails: any;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
  }

}
