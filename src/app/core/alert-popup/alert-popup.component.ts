import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-alert-popup',
  templateUrl: './alert-popup.component.html',
  styleUrls: ['./alert-popup.component.scss']
})
export class AlertPopupComponent implements OnChanges, OnInit {
  @Input() selectedAssetToLinkCount: number;
  @Input() selectedExceptionAssetsCount: number;
  @Input() assetRegisterErrorMsg: any;
  @Input() serverAction: any;
  @Input() usernameToDelete: string;
  @Output() confirmedDelink: EventEmitter<any> = new EventEmitter();
  @Output() confirmedServiceDelink: EventEmitter<any> = new EventEmitter();
  @Output() confirmedlink: EventEmitter<any> = new EventEmitter();
  @Output() confirmedServicelink: EventEmitter<any> = new EventEmitter();
  @Output() confirmedDeregisteringAsset: EventEmitter<any> = new EventEmitter();
  @Output() confirmedDeletingUser: EventEmitter<any> = new EventEmitter();
  @Output() confirmedRegisteringExceptionBeacons: EventEmitter<any> = new EventEmitter();
  @Output() confirmedDeregisteringExceptionBeacons: EventEmitter<any> = new EventEmitter();
  @Output() confirmedServerAction: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.serverAction = null;
    this.usernameToDelete = '';
  }

  ngOnChanges() {
  }
  ngOnInit() {
  }

  confirmed() {
    this.confirmedDelink.emit('OK');
  }
  confirmServiceDelink() {
    this.confirmedServiceDelink.emit('OK');
  }
  linkingConfirmed() {
    this.confirmedlink.emit('OK');
  }

  linkingServiceConfirmed() {
    this.confirmedServicelink.emit('OK');
  }

  deregisteringAssetConfirmed() {
    this.confirmedDeregisteringAsset.emit('OK');
  }

  deleteUserConfirmed() {
    this.confirmedDeletingUser.emit('OK');
  }

  registerExceptionBeaconConfirmed() {
    this.confirmedRegisteringExceptionBeacons.emit('OK');
  }

  deregisterExceptionBeaconConfirmed() {
    this.confirmedDeregisteringExceptionBeacons.emit('OK');
  }

  serverActionConfirmed() {
    this.confirmedServerAction.emit('OK');
  }

}
