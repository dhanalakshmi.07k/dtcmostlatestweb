import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-modal-lg-report-details',
  templateUrl: './modal-lg-report-details.component.html',
  styleUrls: ['./modal-lg-report-details.component.scss']
})
export class ModalLgReportDetailsComponent implements OnChanges, OnInit {

  @Input() showEngineDiagnosticDetails: any;
  @Input() showUvisDetails: any;
  @Input() showAvddsDetails: any;
  @Input() showDriverFeedback: any;
  @Input() selectedInspectedCarIndividualComponentDetails: any;
  @Input() randomNumberToSetData: any;
  public individualComponentDetails: any;
  public resetComponetValue: number;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    for(const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'selectedInspectedCarIndividualComponentDetails') {
        if (change.currentValue !== change.previousValue) {
          if (this.selectedInspectedCarIndividualComponentDetails) {
            this.individualComponentDetails = this.selectedInspectedCarIndividualComponentDetails;
          }
        }
      }
    }
  }

  resetValues() {
    this.resetComponetValue = Math.random();
  }
}
