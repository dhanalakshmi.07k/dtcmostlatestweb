import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalLgReportDetailsComponent } from './modal-lg-report-details.component';

describe('ModalLgReportDetailsComponent', () => {
  let component: ModalLgReportDetailsComponent;
  let fixture: ComponentFixture<ModalLgReportDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalLgReportDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalLgReportDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
