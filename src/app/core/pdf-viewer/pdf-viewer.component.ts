import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import {ConfigService} from '../../../services/config.service';

import { Subject,Actions} from '../../../auth/rules';
declare var $: any;

@Component({
  selector: 'app-pdf-viewer',
  templateUrl: './pdf-viewer.component.html',
  styleUrls: ['./pdf-viewer.component.scss']
})
export class PdfViewerComponent implements OnChanges, OnInit {
  @Input() selectedReportData: any;
  @Output() inspectedCarDetailsForReportPage: EventEmitter<any> = new EventEmitter();
  public resetComponetValue: number;
  public showGeneratePdfBtn: boolean;
  public iotzenLogoSrcUrl: string;


  SUBJECT=Subject;
  ACTIONS=Actions;

  constructor(private configService: ConfigService) {
    this.showGeneratePdfBtn = false;
    this.iotzenLogoSrcUrl = this.configService.appConfig.iotzenLogoUrl;
  }

  ngOnInit() {}

  ngOnChanges() {
    this.showGeneratePdfBtn = true;
  }

  sendSelectedReportDataToReport() {
    this.inspectedCarDetailsForReportPage.emit(this.selectedReportData);
  }

  close() {
    $('html').css('overflow-y', 'auto');
    this.resetComponetValue = Math.random();
    this.selectedReportData.inspectionTime = null;
    this.showGeneratePdfBtn = false;
  }
}
