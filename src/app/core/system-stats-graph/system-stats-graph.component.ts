import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Chart } from 'chart.js';
import { GraphDataType } from './graph-data-type';
import { IotzenPillType } from '../iotzen-pills/iotzen-pill-type';
declare var $: any;

@Component({
  selector: 'app-system-stats-graph',
  templateUrl: './system-stats-graph.component.html',
  styleUrls: ['./system-stats-graph.component.scss']
})
export class SystemStatsGraphComponent implements OnChanges, OnInit  {
  private chartData: Chart;
  @Input() graphData: GraphDataType;
  @Input() graphRandomNumber: number;
  @Input() resetFormData: number;
  @Input() canvasId: string;
  @Input() showTabs: boolean;
  @Output() graphDataWithSeriesType: EventEmitter<any> = new EventEmitter();
  public arrayOfDurationPills: Array<IotzenPillType> = [];
  public isBoxShadowNeeded: boolean;
  constructor() {
    this.showTabs = false;
    this.isBoxShadowNeeded = false;
  }

  ngOnInit() {
    this.buildPillsArray();
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'graphRandomNumber') {
        if (change.currentValue !== change.previousValue) {
          this.isBoxShadowNeeded = true;
          this.statsGraphPlotting(this.graphData.graphType, this.graphData.labels, this.graphData.data,
            this.graphData.dataPropertyName, this.graphData.size, this.graphData.dataSetsLabel, this.graphData.dataSetsBackgroundColor);
        }
      }
      if (propName === 'resetFormData') {
        if (change.currentValue !== change.previousValue) {
          if (this.chartData) {
            this.chartData.destroy();
          }
          this.graphData.graphTitle = '';
          this.isBoxShadowNeeded = false;
        }
      }
    }
  }

  buildPillsArray1() {
    this.arrayOfDurationPills = [{id: 'oneHour', label: '1H', description: 'Last 1 Hour'},
      {id: 'oneDay', label: '1D', description: 'Last 1 Day'},
      {id: 'oneWeek', label: '1W', description: 'Last 1 Week'},
      {id: 'oneMonth', label: '1M', description: 'Last 1 Month'},
      {id: 'oneYear', label: '1Y', description: 'Last 1 Year'}];
  }

  buildPillsArray() {
    this.arrayOfDurationPills = [{id: 'oneHour', label: '1H', description: 'Last 1 Hour'},
      {id: 'sixHours', label: '6H', description: 'Last 6 Hours'},
      {id: 'twelveHours', label: '12H', description: 'Last 12 Hours'},
      {id: 'oneDay', label: '1D', description: 'Last 1 Day'}];
  }

  getActivePillValue(value: string) {
    let duration: string;
    duration = 'oneHour';
    switch (value) {
      case 'oneHour':
        duration = 'oneHour';
        break;
      case 'sixHours':
        duration = 'sixHours';
        break;
      case 'twelveHours':
        duration = 'twelveHours';
        break;
      case 'oneDay':
        duration = 'oneDay';
        break;
      default:
        break;
    }
    let obj: Object;
    obj = {};
    obj['field'] = this.graphData.hardwarePropertyType;
    obj['duration'] = duration;
    this.graphDataWithSeriesType.emit(obj);
  }

  statsGraphPlotting(graphType: string, labels: Array<string>, data: any, dataPropertyName: string, size: string, dataSetsLabel: string, dataSetsBackgroundColor: string ) {
    if (this.chartData) {
      this.chartData.destroy();
    }
    if (data) {
      let canvas: any = document.getElementById(this.canvasId);
      let ctx = canvas.getContext('2d');
      this.chartData = new Chart(ctx,
        {
          type: graphType,
          data: {
            labels: labels,
            datasets: [{
              label: dataSetsLabel,
              data: data[dataPropertyName],
              fill: false,
              borderColor: dataSetsBackgroundColor,
              borderWidth: 1.5,
            }],
          },
          options: {
            title: {
              display: true,
              position: 'left',
              fontSize: 14,
              text: size
            },
            tooltips: {
              titleFontSize: 15,
              titleMarginBottom: 10,
              titleFontColor: '#212529',
              bodyFontSize: 15,
              bodySpacing: 10,
              bodyFontColor: '#212529',
              footerFontSize: 15,
              footerMarginTop: 10,
              footerFontColor: '#212529',
              backgroundColor: 'rgba(255,255,255,0.9)',
              borderWidth: 1,
              borderColor: '#7F7F7F',
              mode: 'label',
            },
            responsive: true,
            legend: {
              position: 'right' // place legend on the right side of chart
            },
            scales: {
              yAxes: [{
                /*ticks: {
                  beginAtZero: true
                },*/
                stacked: false // this also..
              }]
            },
            elements: {  // to hide the circles
              point: {
                radius: 0
              }
            }
          },
        });
    }
  }

}
