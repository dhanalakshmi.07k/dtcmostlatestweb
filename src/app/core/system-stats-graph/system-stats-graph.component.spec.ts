import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemStatsGraphComponent } from './system-stats-graph.component';

describe('SystemStatsGraphComponent', () => {
  let component: SystemStatsGraphComponent;
  let fixture: ComponentFixture<SystemStatsGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemStatsGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemStatsGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
