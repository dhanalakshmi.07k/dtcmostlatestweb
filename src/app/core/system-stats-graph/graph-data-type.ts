/**
 * Created by chandru on 23/1/19.
 */


export interface GraphDataType {
  graphTitle: string;
  graphType: string;
  labels: Array<string>;
  data: any;
  dataPropertyName: string;
  size: string;
  dataSetsLabel: string;
  dataSetsBackgroundColor: string;
  hardwarePropertyType: string;
}
