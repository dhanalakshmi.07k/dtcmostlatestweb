import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchForServiceComponent } from './search-for-service.component';

describe('SearchForServiceComponent', () => {
  let component: SearchForServiceComponent;
  let fixture: ComponentFixture<SearchForServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchForServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchForServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
