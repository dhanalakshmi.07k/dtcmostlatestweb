import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-iotzen-pills',
  templateUrl: './iotzen-pills.component.html',
  styleUrls: ['./iotzen-pills.component.scss']
})
export class IotzenPillsComponent implements OnChanges, OnInit {

  public activePillColour: string;
  public defaultActivePillColour: string;
  public activePill: string;
  @Input() pillActiveColour: string;
  @Input() defaultActivePillIndex: number;
  @Input() arrayOfPills: string;
  @Output() activePillValue: EventEmitter<string> = new EventEmitter();
  constructor() {
    this.defaultActivePillColour = '#6D7FCC';
  }

  ngOnInit() {
    /*if (!this.defaultActivePillIndex) {
     this.defaultActivePillIndex = 0;
     }*/
    if ( this.defaultActivePillIndex && this.arrayOfPills
      && this.arrayOfPills.length > 0
      && this.defaultActivePillIndex < this.arrayOfPills.length) {
    } else {
      this.defaultActivePillIndex = 0;
    }
    this.activePill = this.arrayOfPills[this.defaultActivePillIndex]['id'];
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'pillActiveColour') {
        if (change.currentValue !== change.previousValue) {
          this.setPillActiveColour();
        }
      }
    }
  }

  setPillActiveColour() {
    if (this.pillActiveColour) {
      this.activePillColour = this.pillActiveColour;
    } else {
      this.activePillColour = this.defaultActivePillColour;
    }
  }

  onPillSelected(activePill) {
    this.activePill = activePill;
    this.activePillValue.emit(this.activePill);
  }

  setTextColor(value) {
    if (value === this.activePill) { return true;
    } else { return false; }
  }

  getBackGroundColour(value) {
    if (value === this.activePill) { return this.activePillColour;
    } else { return 'inherit'; }
  }

}
