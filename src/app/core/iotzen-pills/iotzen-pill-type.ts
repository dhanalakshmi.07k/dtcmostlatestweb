export interface IotzenPillType {
  id: string;
  label: string;
  description: string;
}
