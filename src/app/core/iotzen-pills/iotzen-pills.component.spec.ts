import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IotzenPillsComponent } from './iotzen-pills.component';

describe('IotzenPillsComponent', () => {
  let component: IotzenPillsComponent;
  let fixture: ComponentFixture<IotzenPillsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IotzenPillsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IotzenPillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
