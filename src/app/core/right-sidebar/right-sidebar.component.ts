import { Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import {AssetService} from "../../../services/asset.service";
import {FormService} from "../../../services/form.service";
import {AssetConfigService} from '../../../services/asset-config.service';
import {ConfigService} from '../../../services/config.service';
import {ServiceLinkableService} from '../../../services/serviceLinkable.service';
import {HealthCheckService} from '../../../services/healthCheck.service';
import { GraphDataType } from '../system-stats-graph/graph-data-type';
import { Chart } from 'chart.js';
import * as _ from 'lodash';
import * as moment from 'moment-timezone';
import { Subject,Actions} from '../../../auth/rules';
import {ability} from '../../../auth/ability';

declare var $: any;

@Component({
  selector: 'app-right-sidebar',
  templateUrl: './right-sidebar.component.html',
  styleUrls: ['./right-sidebar.component.scss']
})
export class RightSidebarComponent implements OnChanges, OnInit {
  @Input() assetData: any;
  @Input() assetLinkedDetails: any;
  @Input() assetLinkedToServiceDetails: any;
  @Input() assetConfigDetails: any;
  @Input() serviceAssetConfigDetails: any;
  @Input() allServersStats: any;
  @Input() allServicesStatus: any;
  @Input() showCircularLoaderValue: any;
  @Input() isAssetDetailsInReadMode: boolean;
  @Input() isAssetDeregisteredSuccess: boolean;
  @Input() isServerManageActionSuccess: boolean;
  @Input() resetFormData: number;
  @Output() delinkAssetDetailsToMainComponent: EventEmitter<any> = new EventEmitter();
  @Output() delinkServiceDetailsToMainComponent: EventEmitter<any> = new EventEmitter();
  @Input() isLinkDelinkAction: boolean;
  @Input() isServiceLinkDelinkAction: boolean;
  @Output() linkAssetDetailsToMainComponent: EventEmitter<any> = new EventEmitter();
  @Output() linkServiceDetailsToMainComponent: EventEmitter<any> = new EventEmitter();
  @Output() syncUpdateAssetCartEvent: EventEmitter<any> = new EventEmitter();
  @Output() deregisterAssetToMainComponent: EventEmitter<any> = new EventEmitter();
  @Output() manageServicesObjectToMainComponent: EventEmitter<any> = new EventEmitter();
  public assetUpdatedData: any;
  public resetSearchedAssetOnPlusClick: any;
  public resetSearchedServicesOnPlusClick: any;
  public searchForAsset: string = '';
  public infoLabel: string = '';
  public assetConfig: any;
  public individualAssetLinkedDetailsArray: Array<object> = [];
  public serviceActionsArray: Array<object> = [];
  public output: string;
  public assetFromConfig: Array<object> = [];
  public assetId;
  public  assetLabel: string;
  public  serviceLabel: string;
  public  linkedAssetDetails: any;
  public delinkAssetDetails: any;
  public isAssetLinkable: boolean;
  public showCircularLoader: boolean;
  public isEditOptionClicked: boolean;
  public showStatusOnAssetCard: boolean;
  public isServiceLinkable: boolean;
  public isServerAvailable: boolean;
  public isServiceManageable: boolean;
  public showStatus: boolean;
  public customCollapsedHeight: string;
  public customExpandedHeight: string;
  public statsDetails: any = null;
  public statsNotFound: boolean;
  public showStatsGraphTabs: boolean;
  public graphDiskRandomNumber: number;
  public graphCpuRandomNumber: number;
  public graphMemoryRandomNumber: number;
  SUBJECT=Subject;
  ACTIONS=Actions;
  public graphData: ServerStatsGraphDataType = {
    memory: {
      graphTitle: '',
      graphType: '',
      labels: [],
      data: null,
      dataPropertyName: '',
      size: '',
      dataSetsLabel: '',
      dataSetsBackgroundColor: '',
      hardwarePropertyType: ''},
    cpu: {
      graphTitle: '',
      graphType: '',
      labels: [],
      data: null,
      dataPropertyName: '',
      size: '',
      dataSetsLabel: '',
      dataSetsBackgroundColor: '',
      hardwarePropertyType: ''},
    disk: {
      graphTitle: '',
      graphType: '',
      labels: [],
      data: null,
      dataPropertyName: '',
      size: '',
      dataSetsLabel: '',
      dataSetsBackgroundColor: '',
      hardwarePropertyType: ''}
  };
  memoryValue: (value: number) => string;
  constructor(public assetService: AssetService, public formService: FormService, private assetConfigService: AssetConfigService,
              public configService: ConfigService, public healthCheckService: HealthCheckService, public serviceLinkableService: ServiceLinkableService) {
    this.isAssetLinkable = false;
    this.showCircularLoader = true;
    this.isEditOptionClicked = false;
    this.isAssetDetailsInReadMode = false;
    this.isAssetDeregisteredSuccess = false;
    this.isServerManageActionSuccess = false;
    this.isServiceLinkable = false;
    this.isServiceManageable = false;
    this.isServerAvailable = false;
    this.showStatusOnAssetCard = false;
    this.showStatus = false;
    this.statsNotFound = false;
    this.showStatsGraphTabs = false;
    this.serviceAssetConfigDetails = [];
    this.serviceActionsArray = [];
    this.assetLabel = '';
    this.serviceLabel = '';
    this.customCollapsedHeight = '70px';
    this.customExpandedHeight = '70px';
    this.memoryValue = function(value: number): string {
      return `${Math.round(value)} / ${this['max']}`;
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName of Object.keys(changes)) {
      const change = changes[propName];
      if (propName === 'assetData') {
        if (change.currentValue !== change.previousValue) {
          if (this.assetData && this.assetData.assetType) {
            this.infoLabel = 'Asset Info';
            this.getAssetsConfiguration();
            this.modify();
            this.assetData = this.checkServerStatus(this.assetData);
          }
          if (this.assetData && this.assetData.serviceType) {
            this.infoLabel = 'Service Info';
            this.showStatusOnAssetCard = true;
            this.getAssetsConfiguration();
            this.getServiceAssetConfiguration();
            this.getServiceAssetConfigByType();
          }
          /*if (!this.isLinkDelinkAction && !this.isServiceLinkDelinkAction && !this.isServerManageActionSuccess) {
            this.openTabContent('', 'assetInfo');
          }*/
          if (this.isServerManageActionSuccess) {
            this.openTabContent('', 'manageService');
          }
          if (this.assetData.assetType) {
            const groupByAssetType = _.groupBy(this.assetConfigDetails, 'type') ;
            this.isAssetLinkable = groupByAssetType && groupByAssetType[this.assetData.assetType] &&
              groupByAssetType[this.assetData.assetType][0] &&
              groupByAssetType[this.assetData.assetType][0].config &&
              groupByAssetType[this.assetData.assetType][0].config.assetsLinkable &&
              groupByAssetType[this.assetData.assetType][0].config.assetsLinkable.length > 0 ;
          } else if ( this.assetData.serviceType ) {
            const groupByServiceType = _.groupBy(this.serviceAssetConfigDetails, 'type') ;
            this.isAssetLinkable = groupByServiceType && groupByServiceType[this.assetData.serviceType] &&
              groupByServiceType[this.assetData.serviceType][0] &&
              groupByServiceType[this.assetData.serviceType][0].config &&
              groupByServiceType[this.assetData.serviceType][0].config.assetsLinkable;
          }
        }
      }
      if (propName === 'assetLinkedDetails') {
        if (change.currentValue !== change.previousValue) {
            if (this.assetData && this.assetData.assetsLinked && this.assetData.assetsLinked.length > 0 ) {
                this.getLinkedAssetDetails(this.assetData._id);
            } else if (this.assetData && this.assetData.serviceAssets && this.assetData.serviceAssets.length > 0) {
              this.getServicesLinkedToAssetDetails();
            } else {
                this.individualAssetLinkedDetailsArray = [];
            }
            if (this.isLinkDelinkAction) {
                this.openTabContent('', 'link');
            } else if (this.isServiceLinkDelinkAction) {
              this.openTabContent('', 'service');
            }
        }
      }
      if (propName === 'assetLinkedToServiceDetails') {
        if (change.currentValue !== change.previousValue) {
          this.modifyAssetLinkedToServiceData();
          /*if (this.assetLinkedToServiceDetails) {
            this.assetLinkedToServiceDetails(this.assetData._id);
          } else {
            this.individualAssetLinkedDetailsArray = [];
          }
          if (this.isLinkDelinkAction) {
            this.openTabContent('', 'link');
          }*/
        }
      }
      if (propName === 'showCircularLoaderValue') {
        if (change.currentValue !== change.previousValue) {
          this.showCircularLoader = true;
        }
      }
      if (propName === 'resetFormData') {
        if (change.currentValue !== change.previousValue) {
          this.showStatusOnAssetCard = false;
        }
      }
      if (propName === 'isAssetDeregisteredSuccess') {
        if (change.currentValue !== change.previousValue) {
          if (this.isAssetDeregisteredSuccess === true) {
            this.isEditOptionClicked = false;
          }
        }
      }
    }
  }

  ngOnInit() {}

  isAssetDelinkBtnReqired(): boolean {
    let isAssetDelinkBtnReqired: boolean;
    isAssetDelinkBtnReqired = false;
    if (this.assetData && this.assetData.assetType) {
      if ( ability.can( Actions.UPDATE, Subject.ASSET)) {
        isAssetDelinkBtnReqired = true;
      }
    } else if (this.assetData && this.assetData.serviceType) {
      if ( ability.can( Actions.UPDATE, Subject.ASSET)) {
        if (ability.can(Actions.UPDATE, Subject.SERVICE)) {
          isAssetDelinkBtnReqired = true;
        }
      }
    }
    return isAssetDelinkBtnReqired;
  }

  isServiceDelinkBtnReqired(): boolean {
    let isServiceDelinkBtnReqired: boolean;
    isServiceDelinkBtnReqired = false;
    if (this.assetData && this.assetData.assetType) {
      if ( ability.can( Actions.UPDATE, Subject.ASSET)) {
        if (ability.can(Actions.UPDATE, Subject.SERVICE)) {
          isServiceDelinkBtnReqired = true;
        }
      }
    }
    return isServiceDelinkBtnReqired;
  }

  checkServerStatus(assetData) {
    let serverStatusGroupById: any;
    serverStatusGroupById = _.groupBy(this.allServersStats, '_id');
    if (serverStatusGroupById[assetData._id]) {
      if ((serverStatusGroupById[assetData._id][0]._id) === assetData._id) {
        assetData.running = true;
      }
    }
    return assetData;
  }

  getServicesLinkedToAssetDetails() {
   if (this.assetData && this.assetData.serviceAssets) {
     // this.individualAssetLinkedDetailsArray = this.modifyAssetLinkedData(this.assetData.serviceAssets);
     let individualAssetLinkedDetailsArray: any = [];
     individualAssetLinkedDetailsArray = this.modifyAssetLinkedData(this.assetData.serviceAssets);
     this.individualAssetLinkedDetailsArray = this.checkServicesLinkedToAssetStatus(individualAssetLinkedDetailsArray);
   }
  }

  modifyAssetLinkedToServiceData() {
    if (this.assetLinkedToServiceDetails) {
      let array: any = [];
      let individualAssetLinkedDetailsArray: any = [];
      array.push(this.assetLinkedToServiceDetails);
      // this.individualAssetLinkedDetailsArray = this.modifyAssetLinkedData(array);
      individualAssetLinkedDetailsArray = this.modifyAssetLinkedData(array);
      this.individualAssetLinkedDetailsArray = this.checkAssetsLinkedToServiceStatus(individualAssetLinkedDetailsArray);
    } else {
      this.individualAssetLinkedDetailsArray = [];
    }
  }

  checkAssetsLinkedToServiceStatus(individualAssetLinkedDetailsArray) {
    let allServersStats: any = [];
    allServersStats = this.allServersStats;
    if (individualAssetLinkedDetailsArray.length > 0) {
      _.forEach(individualAssetLinkedDetailsArray[0].assetsLinked, function(value) {
        let serverStatusGroupById: any;
        serverStatusGroupById = _.groupBy(allServersStats, '_id');
        if (serverStatusGroupById[value._id]) {
          if ((serverStatusGroupById[value._id][0]._id) === value._id) {
            value.running = true;
          }
        }
      });
    }
    return individualAssetLinkedDetailsArray;
  }

  checkServicesLinkedToAssetStatus(servicesLinkedToAssetsArray) {
    let allServicesStatus: any = [];
    allServicesStatus = this.allServicesStatus;
    if (servicesLinkedToAssetsArray.length > 0) {
      _.forEach(servicesLinkedToAssetsArray[0].assetsLinked, function(value) {
        let serviceStatusGroupedbyId: any;
        serviceStatusGroupedbyId = _.groupBy(allServicesStatus, '_id');
        if (serviceStatusGroupedbyId[value._id]) {
          if (serviceStatusGroupedbyId[value._id][0].running && serviceStatusGroupedbyId[value._id][0].running === true) {
            value.running = true;
          }
        }
      });
    }
    return servicesLinkedToAssetsArray;
  }

  getAssetsConfiguration() {
    this.assetConfigService.getAssetsConfig()
      .then(assetConfig => {
        this.assetConfig = assetConfig;
       // this.getAssetLabel();
      });
  }

  getServiceAssetConfiguration() {
    this.assetConfigService.getServiceAssetConfig()
      .then(serviceAssetConfig => {
        this.serviceAssetConfigDetails = serviceAssetConfig;
       // this.getServiceAssetLabel();
      });
  }

  getServiceAssetLabel() {
    this.serviceLabel = '';
    if (this.assetData && this.assetData.serviceType) {
      let assetDetails =  _.find(this.serviceAssetConfigDetails, ['type', this.assetData.serviceType]);
      this.serviceLabel = assetDetails.label;
    }
  }

  getAssetLabel() {
    this.assetLabel = '';
    if (this.assetData && this.assetData.assetType) {
      let assetDetails =  _.find(this.assetConfig, ['type', this.assetData.assetType]);
      this.assetLabel = assetDetails.label;
    }
  }

  modify() {
    this.showCircularLoader = true;
    this.assetId = this.assetData._id;
      this.assetService.getAssetConfig(this.assetData.assetType)
        .subscribe((data: any) => {
        let selectedAssetConfig: any = {};
          selectedAssetConfig = data;
          if (selectedAssetConfig && selectedAssetConfig.assetType && selectedAssetConfig.assetType.config && selectedAssetConfig.assetType.config.isServiceLinkable) {
            this.isServiceLinkable = true;
            this.showStatusOnAssetCard = true;
          } else {
            this.isServiceLinkable = false;
            this.showStatusOnAssetCard = false;
          }
          if (selectedAssetConfig && selectedAssetConfig.assetType && selectedAssetConfig.assetType.config && selectedAssetConfig.assetType.config.showStats) {
            this.showStatus = true;
           // this.getServerAnalytics();
           // this.getAssetStatus();
          } else { this.showStatus = false; }
          this.assetFromConfig = this.formService.formatEditAssetConfig(selectedAssetConfig.configuration, this.assetData);
          this.showCircularLoader = false;
        });
  }

  getServiceAssetConfigByType() {
    this.showCircularLoader = true;
    this.assetId = this.assetData._id;
    this.serviceLinkableService.getServiceAssetConfig(this.assetData.serviceType)
      .subscribe((data: any) => {
        let selectedServiceConfig: any = {};
        selectedServiceConfig = data;
        if (selectedServiceConfig && selectedServiceConfig.serviceType && selectedServiceConfig.serviceType.config && selectedServiceConfig.serviceType.config.management) {
          this.isServiceManageable = true;
          if (selectedServiceConfig.serviceType.config.management.actions && selectedServiceConfig.serviceType.config.management.actions.length > 0) {
            this.serviceActionsArray = selectedServiceConfig.serviceType.config.management.actions;
            if (this.assetData && this.assetData.serverStatus) {
              this.isServerAvailable = true;
              if (this.assetData.running) {
                 _.remove(this.serviceActionsArray, function(n: any) {
                  return (n.value === 'start');
                });
              } else {
                _.remove(this.serviceActionsArray, function(n: any) {
                  return (n.value === 'stop' || n.value === 'restart');
                });
              }
            } else {
              this.isServerAvailable = false;
              this.serviceActionsArray = [];
            }
          }
        } else {
          this.isServiceManageable = false;
        }
        this.assetFromConfig = this.formService.formatEditAssetConfig(selectedServiceConfig.configuration, this.assetData);
        this.showCircularLoader = false;
      });
  }

  getAssetStatus() {
    this.statsDetails = null;
    this.showCircularLoader = true;
    let durationInMinutes: any = 5;
    let start= moment().subtract(durationInMinutes, 'minutes').utc();
    let end = moment().utc();
    this.healthCheckService.getAssetStatus(start, end, this.assetId)
      .subscribe(data => {
        this.showCircularLoader = false;
        /*let serverStatusDetails: any = [];
        serverStatusDetails = data;
        if (serverStatusDetails.length > 0) {
           serverStatusDetails[0].mem.free = this.formatBytes(serverStatusDetails[0].mem.free);
           serverStatusDetails[0].mem.total = this.formatBytes(serverStatusDetails[0].mem.total);
           this.statsDetails = serverStatusDetails[0];
        } else {
          this.statsDetails = null;
        }*/
      });
  }

  getServerAnalytics() {
    this.showCircularLoader = true;
    this.statsNotFound = false;
    let durationInMinutes: any = 1;
    let start= moment().subtract(durationInMinutes, 'hour').utc();
    let end = moment().utc();
    this.assetService.getServerAnalytics(start, end, this.assetId, 'all')
      .subscribe(data => {
        this.showCircularLoader = false;
        let historicalData: any = [];
        historicalData = data;
       // historicalData = _.orderBy(historicalData, 'dateInString', 'asc');
        if (historicalData && historicalData.length > 0) {
          this.statsNotFound = false;
          this.showStatsGraphTabs = true;
          let xAxis: any;
          let groupData: any;
          historicalData = _.sortBy(historicalData, [function(o) { return o.milliseconds; }]);
          groupData = _.groupBy(historicalData, 'dateInString');
          xAxis = _.keys(groupData);
          this.drawDiskStatsChart(xAxis, groupData);
          this.drawCpuStatsChart(xAxis, groupData);
          this.drawMemoryStatsChart(xAxis, groupData);
        } else {
          this.statsNotFound = true;
          this.showStatsGraphTabs = false;
        }
      });
  }


  getGraphDataForSeriesType(graphDataWithSeriesTypeObj) {
    let startTime: Date;
    let endTime: Date;
    switch (graphDataWithSeriesTypeObj.duration) {
      case 'oneHour':
        startTime = moment().subtract(1, 'hour').utc();
        endTime = moment().utc();
        break;
      case 'oneDay':
        startTime = moment().subtract(1, 'day').utc();
        endTime = moment().utc();
        break;
      case 'sixHours':
        startTime = moment().subtract(6, 'hour').utc();
        endTime = moment().utc();
        break;
      case 'twelveHours':
        startTime = moment().subtract(12, 'hour').utc();
        endTime = moment().utc();
        break;
      default:
        break;
    }
    this.showCircularLoader = true;
    this.assetService.getServerAnalytics(startTime, endTime, this.assetId, graphDataWithSeriesTypeObj.field)
      .subscribe((data) => {
        this.showCircularLoader = false;
        let historicalData: any = [];
        historicalData = data;
        if (historicalData && historicalData.length > 0) {
          let xAxis: any;
          let groupData: any;
          historicalData = _.sortBy(historicalData, [function(o) { return o.milliseconds; }]);
          groupData = _.groupBy(historicalData, 'dateInString');
          xAxis = _.keys(groupData);
          if (graphDataWithSeriesTypeObj.field === 'disk') {
            this.drawDiskStatsChart(xAxis, groupData);
          } else if (graphDataWithSeriesTypeObj.field === 'cpu') {
            this.drawCpuStatsChart(xAxis, groupData);
          } else if (graphDataWithSeriesTypeObj.field === 'memory') {
            this.drawMemoryStatsChart(xAxis, groupData);
          }
          //this.drawCpuStatsChart(xAxis, groupData);

        }
      });
  }

  drawDiskStatsChart(xAxis, groupData) {
    let yAxis = {
      usedDisk: []
    };
    let totalDiskSize: string='NA';
    yAxis['usedDisk'] = _.map(xAxis,function(xValue){
      if(groupData && groupData[xValue] && groupData[xValue][0] && groupData[xValue][0]['fsSize'] &&  groupData[xValue][0]['fsSize'].length>0 ) {
        let groupBySize = _.groupBy(groupData[xValue][0]['fsSize'],"size");

        let fileToEvaluated = groupBySize[_.max(_.map(_.keys(groupBySize),function(value){return parseFloat(value)})).toString()]
        if(fileToEvaluated && fileToEvaluated[0] && fileToEvaluated[0]['size']){
          totalDiskSize = 'Disk Size '+ ( fileToEvaluated[0]['size'] / (1024 * 1024 * 1024)).toFixed(2)+ ' GB';
          let used = fileToEvaluated[0]['used'];
          return  (used/ (1024 * 1024 * 1024)).toFixed(2)
        }else{
          return null
        }
      }else{
        return null
      }
    });
    this.setGraphData('Disk Stats', 'line', xAxis, yAxis, 'usedDisk', totalDiskSize, 'disk', 'Disk used in GB', 'rgba(41,182,246,1.9)');
    this.graphDiskRandomNumber = Math.random();
  }

  drawCpuStatsChart(xAxis, groupData) {
    let yAxis={cpuCurrentload:[]}
    yAxis['cpuCurrentload'] = _.map(xAxis,function(xValue){
        if(groupData && groupData[xValue] && groupData[xValue][0] && groupData[xValue][0]['cpu'] &&  groupData[xValue][0]['cpu']['currentload'] ||  groupData[xValue][0]['cpu']['currentload']===0) {
          return groupData[xValue][0]['cpu']['currentload'].toFixed(2)
        }else if(groupData && groupData[xValue] && groupData[xValue][0] && groupData[xValue][0]['cpu'] &&  groupData[xValue][0]['cpu']['currentload'] ||  groupData[xValue][0]['cpu']['currentload']===0){
          return 0
        }else{
          return null
        }
    });
    this.setGraphData('CPU Stats', 'line', xAxis, yAxis, 'cpuCurrentload', '', 'cpu', 'CPU Currentload', 'rgba(56, 142, 60,1.9)');
    this.graphCpuRandomNumber = Math.random();
  }

  drawMemoryStatsChart(xAxis, groupData) {
    let yAxis = {
        usedMemory: []
    };
    let totalMemory: string="NA";
    /*if (xAxis[0] && groupData[xAxis[0]] && groupData[xAxis[0]][0]['memory']['total']) {
      totalMemory ='Total memory ' +  (groupData[xAxis[0]][0]['memory']['total'] / (1024 * 1024 * 1024)).toFixed(2) + ' GB';
    }else{
      totalMemory="NA"
    }*/
    yAxis['usedMemory'] = _.map(xAxis,function(xValue){
      if(groupData && groupData[xValue] && groupData[xValue][0] && groupData[xValue][0]['memory'] &&  groupData[xValue][0]['memory']['used'] ) {
        let used = groupData[xValue][0]['memory']['used'];
        totalMemory ='Total memory ' +  (groupData[xAxis[0]][0]['memory']['total'] / (1024 * 1024 * 1024)).toFixed(2) + ' GB';

        return (used / (1024 * 1024 * 1024)).toFixed(2)
      }else{
        return null
      }
    });

    //totalMemory = 'Total memory ' + totalMemory + ' GB';
    this.setGraphData('Memory Stats', 'line', xAxis, yAxis, 'usedMemory', totalMemory, 'memory', 'Used Memory in GB', 'rgba(255, 160, 0,1.9)');
    this.graphMemoryRandomNumber = Math.random();
   // this.memoryStatsGraphPlotting(xAxis, yAxis, totalMemory);
  }

  setGraphData(graphTitle: string, graphType: string, labels: Array<string>, data: any,
               dataPropertyName: string, size: string, type: string, dataSetsLabel: string, dataSetsBackgroundColor: string) {
     this.graphData[type].graphTitle = graphTitle;
     this.graphData[type].graphType = graphType;
     this.graphData[type].labels = labels;
     this.graphData[type].data = data;
     this.graphData[type].dataPropertyName = dataPropertyName;
     this.graphData[type].size = size;
     this.graphData[type].dataSetsLabel = dataSetsLabel;
     this.graphData[type].dataSetsBackgroundColor = dataSetsBackgroundColor;
     this.graphData[type].hardwarePropertyType = type;
  }

  /*diskStatsGraphPlotting(label, data, totalDiskSize) {
    let canvas : any = document.getElementById('diskStatsChart');
    let ctx = canvas.getContext('2d');
    this.serverHistoricalData.diskChartData = new Chart(ctx,
      {
        type: 'line',
        data: {
          labels: label,
          datasets: [{
            label: 'Disk used in GB',
            data: data['usedDisk'],
            fill: false,
            borderColor: 'rgb(41,182,246,1.9)',
            borderWidth: 1.5,
          }],
        },
        options: {
          title: {
            display: true,
            position: 'left',
            fontSize: 14,
            text: 'Disk Size ' + totalDiskSize + ' GB'
          },
          tooltips: {
            titleFontSize: 15,
            titleMarginBottom: 10,
            titleFontColor: '#212529',
            bodyFontSize: 15,
            bodySpacing: 10,
            bodyFontColor: '#212529',
            footerFontSize: 15,
            footerMarginTop: 10,
            footerFontColor: '#212529',
            backgroundColor: 'rgba(255,255,255,0.9)',
            borderWidth: 1,
            borderColor: '#7F7F7F',
            mode: 'label',
          },
          responsive: true,
          legend: {
            position: 'right' // place legend on the right side of chart
          },
          scales: {
            /!*steppedLine: true,
             xAxes: [{
             stacked: false, // this should be set to make the bars stacked
             }],*!/
            yAxes: [{
              stacked: false // this also..
            }]
          }
        },
      });
  }

  cpuStatsGraphPlotting(label, data) {
    let canvas : any = document.getElementById('cpuStatsChart');
    let ctx = canvas.getContext('2d');
    this.serverHistoricalData.chartData = new Chart(ctx,
      {
        type: 'line',
        data: {
          labels: label,
          datasets: [{
            label: 'CPU Currentload',
            data: data['cpuCurrentload'],
            fill: false,
            borderColor: 'rgba(56, 142, 60,1.9)',
            borderWidth: 1.5,
          }/!*, {
            label: 'CPU FullLoad',
            data: data['cpuFullLoad'],
            fill: false,
            borderColor: 'rgba(255, 160, 0,1.9)',
            borderWidth: 1.5,
          }*!/],
        },
        options: {
          tooltips: {
            titleFontSize: 15,
            titleMarginBottom: 10,
            titleFontColor: '#212529',
            bodyFontSize: 15,
            bodySpacing: 10,
            bodyFontColor: '#212529',
            footerFontSize: 15,
            footerMarginTop: 10,
            footerFontColor: '#212529',
            backgroundColor: 'rgba(255,255,255,0.9)',
            borderWidth: 1,
            borderColor: '#7F7F7F',
            mode: 'label',
          },
          responsive: true,
          legend: {
            position: 'right' // place legend on the right side of chart
          },
          scales: {
            /!*steppedLine: true,
            xAxes: [{
              stacked: false, // this should be set to make the bars stacked
            }],*!/
            yAxes: [{
              stacked: false // this also..
            }]
          }
        },
      });
  }

  memoryStatsGraphPlotting(label, data, totalMemory) {
    let canvas : any = document.getElementById('memoryStatsChart');
    let ctx = canvas.getContext('2d');
    this.serverHistoricalData.memoryChartData = new Chart(ctx,
      {
        type: 'line',
        data: {
          labels: label,
          datasets: [/!*{
            label: 'Total memory',
            data: data['totalMemory'],
            fill: false,
            borderColor: 'rgba(56, 142, 60,1.9)',
            borderWidth: 1.5,
          },*!/ {
            label: 'Free Memory in GB',
            data: data['freeMemory'],
            fill: false,
            borderColor: 'rgba(255, 160, 0,1.9)',
            borderWidth: 1.5,
          }],
        },
        options: {
          title: {
            display: true,
            position: 'left',
            fontSize: 14,
            text: 'Total memory ' + totalMemory + ' GB'
          },
          tooltips: {
            titleFontSize: 15,
            titleMarginBottom: 10,
            titleFontColor: '#212529',
            bodyFontSize: 15,
            bodySpacing: 10,
            bodyFontColor: '#212529',
            footerFontSize: 15,
            footerMarginTop: 10,
            footerFontColor: '#212529',
            backgroundColor: 'rgba(255,255,255,0.9)',
            borderWidth: 1,
            borderColor: '#7F7F7F',
            mode: 'label',
          },
          responsive: true,
          legend: {
            position: 'right' // place legend on the right side of chart
          },
          scales: {
            /!*steppedLine: true,
             xAxes: [{
             stacked: false, // this should be set to make the bars stacked
             }],*!/
            yAxes: [{
              stacked: false // this also..
            }]
          }
        },
      });
  }*/

  /*formatBytes(bytes) {
    return parseInt((bytes / (1024 * 1024)).toFixed(1));
  }*/
  updateAssetCart(dataUpdated) {
    this.assetUpdatedData = dataUpdated;
    this.syncUpdateAssetCartEvent.emit(dataUpdated);
  }

  /*setDataFromConfig(data) {
    this.assetService.saveAssetDetails(this.formService.formatAssetSaveDetails(data))
      .subscribe(res => {
        console.log('ressss', res);
      });
  }*/

  getLinkedAssetDetails(assetId) {
    this.assetService.getLinkedAssets(assetId)
      .subscribe(linkedAssetDetails => {
        this.individualAssetLinkedDetailsArray = this.modifyAssetLinkedData(linkedAssetDetails);
      });
  }

  modifyAssetLinkedData(linkedAssetDetails) {
    let linkedAssetArray: any = [];
    linkedAssetArray = linkedAssetDetails;
    if ( linkedAssetArray &&  linkedAssetArray.length > 0) {
      let assetLinkedData: any;
      assetLinkedData = _.groupBy(linkedAssetDetails, function (asset) {
        if (asset.assetType) {
          return asset.assetType;
        } else if (asset.serviceType) {
          return asset.serviceType;
        }
      });

      let individualAssetLinkedCountArray: any = [];
      let obj;
      for(let key in assetLinkedData) {
        let value = assetLinkedData[key];
        obj = {
          'assetType': key,
          'count': value.length,
          'assetsLinked': value
        };
        individualAssetLinkedCountArray.push(obj);
      }
      return individualAssetLinkedCountArray;
    } else {
      return [];
    }
  }

  /*modifyServiceLinkedData(linkedSericeDetails) {
    let linkedServiceArray: any = [];
    linkedServiceArray = linkedSericeDetails;
    if ( linkedServiceArray &&  linkedServiceArray.length > 0) {
      let serviceLinkedData: any;
      serviceLinkedData = _.groupBy(linkedSericeDetails, function (asset) {
          return asset.serviceType;
      });

      let individualServiceLinkedCountArray: any = [];
      let obj;
      for(let key in serviceLinkedData) {
        let value = serviceLinkedData[key];
        obj = {
          'serviceType': key,
          'count': value.length,
          'serviceLinked': value
        };
        individualServiceLinkedCountArray.push(obj);
      }
      return individualServiceLinkedCountArray;
    } else {
      return [];
    }
  }*/

  assetDetailsToDelink(delinkAssetDetails) {
    //this.delinkAssetDetails = delinkAssetDetails;
    this.delinkAssetDetailsToMainComponent.emit(delinkAssetDetails);
  }

  serviceDetailsToDelink(delinkServiceDetails) {
    this.delinkServiceDetailsToMainComponent.emit(delinkServiceDetails);
  }

  /*selectedAssetTypeToLink(assetType) {
    this.assetTypeForSearch = assetType;
    $('#assetListingPopup').show();
  }*/


  assetDetailsToLink(linkingAssetDetails) {
    this.linkAssetDetailsToMainComponent.emit(linkingAssetDetails);
  }

  serviceDetailsToLinkAsset(linkingServiceDetails) {
    this.linkServiceDetailsToMainComponent.emit(linkingServiceDetails);
  }

  resetSearchedAssets() {
    this.resetSearchedAssetOnPlusClick = Math.random();
  }
  resetSearchedServices() {
    this.resetSearchedServicesOnPlusClick = Math.random();
  }

  deregisterAsset(assetId) {
    this.deregisterAssetToMainComponent.emit(assetId);
  }

  closeAssetPopup() {
    $('#assetListingPopup').hide(300);
    $('html').css('overflow-y', 'auto');
    this.assetFromConfig = [];
    this.individualAssetLinkedDetailsArray = [];
    this.serviceActionsArray = [];
    this.resetFormData = Math.random();
    this.assetLabel = '';
    this.serviceLabel = '';
    this.isEditOptionClicked = false;
    this.showStatusOnAssetCard = false;
    this.openTabContent('', 'assetInfo');
  }

  editOptionClicked() {
    this.isEditOptionClicked = true;
  }

  manageServices(actionDetails) {
    /*let serviceId: any = '';
    let action: string = '';
    action = actionDetails.value;
    serviceId = this.assetId;
    let obj = {};
    obj = {
      'id': serviceId,
      'action': action
    };*/
    this.manageServicesObjectToMainComponent.emit(actionDetails);
    /*this.serviceLinkableService.manageServices(obj)
      .subscribe(res => {});*/
  }

  openTabContent(evt, tabName) {
    let i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName('tabcontent');
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = 'none';
    }
    tablinks = document.getElementsByClassName('tablinks');
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(' active', '');
    }
    if (evt) {
      document.getElementById(tabName).style.display = 'block';
      evt.currentTarget.className += ' active';
    }
    if (!evt && tabName === 'assetInfo') {
      document.getElementById(tabName).style.display = 'block';
      $('.defaultTab').addClass('active');
    }
    if (!evt && tabName === 'link') {
      document.getElementById(tabName).style.display = 'block';
      $('.linkTab').addClass('active');
    }
    if (!evt && tabName === 'service') {
      document.getElementById(tabName).style.display = 'block';
      $('.serviceTab').addClass('active');
    }
    if (!evt && tabName === 'manageService') {
      document.getElementById(tabName).style.display = 'block';
      $('.serviceManageTab').addClass('active');
    }
    if (!evt && tabName === 'status') {
      document.getElementById(tabName).style.display = 'block';
      $('.statusTab').addClass('active');
    }
  }

 /* isAuthorized(action,subject){
    return ability.can(action,subject)
  }*/

}


export interface ServerStatsGraphDataType {
  memory: GraphDataType;
  cpu: GraphDataType;
  disk: GraphDataType;
}
