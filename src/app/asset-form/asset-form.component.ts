import {Component, Input, OnInit, Output ,EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {AssetService} from "../../services/asset.service";
import {AssetCountType} from "../asset-list/asset-count-type";
import {FormService} from "../../services/form.service";
import {ConfigService} from "../../services/config.service";
import {BeaconGatewayService} from "../../services/beacon-gateway.service";
import {RfidGatewayService} from "../../services/rfid-gateway.service";
import {ServiceLinkableService} from '../../services/serviceLinkable.service';
declare  var $: any;
import * as _ from 'lodash';

@Component({
  selector: 'app-asset-form',
  templateUrl: './asset-form.component.html',
  styleUrls: ['./asset-form.component.scss']
})


export class AssetFormComponent implements OnChanges, OnInit {
  Object = Object;
  @Input() formConfigData: any;
  @Input() assetSelectedType: any;
  @Output() savedAssetCard: EventEmitter<any> = new EventEmitter();
  @Output() assetRegistrationErrorMsg: EventEmitter<string> = new EventEmitter();
  constructor(public assetService: AssetService, public formService: FormService,
              public configService: ConfigService,
              public beaconGatewayService: BeaconGatewayService,
              public rfidGatewayService: RfidGatewayService,
              private serviceLinkableService: ServiceLinkableService) {
  }

  submitConfigDetails() {
    if (this.assetSelectedType && this.assetSelectedType === 'general') {
      this.saveServiceAssetDetails();
    } else {
      this.saveAssetDetails();
    }
  }

  saveServiceAssetDetails() {
    this.formConfigData.serviceType = this.assetSelectedType;
    this.serviceLinkableService.saveServiceAssetDetails(this.formService.formatAssetSaveDetails(this.formConfigData))
      .subscribe((savedServiceAssetValue: any) => {
        $('#sidebar-right-to-add').modal('hide');
        $('html').css('overflow-y', 'auto');
        this.savedAssetCard.emit(savedServiceAssetValue);
      },
        err => {
          if (err.status === 405 ) {
            this.assetRegistrationErrorMsg.emit(err['error']['message']);
          }
        });
  }

  saveAssetDetails() {
    this.formConfigData.assetType = this.assetSelectedType;
    this.assetService.saveAssetDetails(this.formService.formatAssetSaveDetails(this.formConfigData))
      .subscribe((savedAssetValue: any) => {
          $('#sidebar-right-to-add').modal('hide');
          $('html').css('overflow-y', 'auto');
          if (this.configService.appConfig.beaconGateway.BEACON_GATEWAY_ASSET_NAME === this.assetSelectedType) {
            let url = savedAssetValue.gatewayProtocol + '://' + savedAssetValue.gatewayIpAddress + ':' + savedAssetValue.gatewayPortNumber;
            this.beaconGatewayService.saveBeaconGatewaySetting(url, this.formService.formatAssetSaveDetails(this.formConfigData))
              .subscribe((res: any) => {
                  this.savedAssetCard.emit(res);
                },
                err => {
                  this.savedAssetCard.emit(err);
                });
          }
          if (this.configService.appConfig.rfidGateway.RFID_GATEWAY_ASSET_NAME === this.assetSelectedType) {
            let url = savedAssetValue.gatewayProtocol + '://' + savedAssetValue.gatewayIpAddress + ':' + savedAssetValue.gatewayPortNumber;
            this.rfidGatewayService.saveRFIDGatewaySetting(url, this.formService.formatAssetSaveDetails(this.formConfigData))
              .subscribe((res: any) => {
                  this.savedAssetCard.emit(res);
                },
                err => {
                  this.savedAssetCard.emit(err);
                });
          }
          this.savedAssetCard.emit(savedAssetValue);
        },
        err => {
          if (err.status === 405 ) {
            this.assetRegistrationErrorMsg.emit(err['error']['message']);
          }
        });
  }

  ngOnInit() {
    this.formConfigData = [];
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'formConfigData') {
        if (change.currentValue !== change.previousValue) {
          let formConfigLocalData: any = [];
          formConfigLocalData = this.formConfigData;
          _.forEach(this.formConfigData, function(value) {
            if (value.default) {
              formConfigLocalData[value.field] = value.default;
            }
          });
          this.formConfigData = formConfigLocalData;
          console.log('add this.formConfigData', this.formConfigData);
        }
      }
    }
  }

}
