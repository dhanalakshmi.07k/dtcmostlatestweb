import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AssetListComponent } from './asset-list/asset-list.component';
import {LoginComponent} from './login/login.component';
import {NearableComponentComponent} from './nearable-component/nearable-component.component';
import {ServicesComponent} from './services/services.component';
import {SettingsComponent} from './settings/settings.component';
import {UploadAssetDataComponent} from './upload-asset-data/upload-asset-data.component';
import {AssetExceptionInfoComponent} from './asset-exception-info/asset-exception-info.component';
import {ReportComponent} from './report/report.component';
import {InspectionBayComponent} from './inspection-bay/inspection-bay.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {AnalyticsComponent} from './analytics/analytics.component';
import {UserManagementComponent} from './user-management/user-management.component';
import {ForbiddenComponent} from './core/forbidden/forbidden.component';
import {AuthGuard} from '../auth/auth.guard';
import {Actions, Subject} from "../auth/rules";
const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
 /* { path: '**' , component : HomeComponent},*/
  { path: 'login', component: LoginComponent},
  { path: 'forbidden', component: ForbiddenComponent, canActivate: [AuthGuard]},
  { path: 'asset' , component : AssetListComponent, canActivate: [AuthGuard], data: {auth: {subject: Subject.ASSET, action: Actions.READ}}},
  { path: 'nearable', component: NearableComponentComponent, canActivate: [AuthGuard], data: {auth: {subject: Subject.ASSET, action: Actions.READ}}},
  { path: 'inspectionBay', component: InspectionBayComponent, canActivate: [AuthGuard], data: {auth: {subject: Subject.INSPECTION, action: Actions.LIVE}}},
  { path: 'report', component: ReportComponent, canActivate: [AuthGuard], data: {auth: {subject: Subject.INSPECTION, action: Actions.READ}}},
  { path: 'analytics', component: AnalyticsComponent, canActivate: [AuthGuard], data: {auth: {subject: Subject.INSPECTION, action: Actions.ANALYTICS}}},
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard], data: {auth: {subject: Subject.SETTINGS, action: Actions.READ}},
      children: [
        {
          path: '',
          redirectTo: 'userProfile',
          pathMatch: 'full'
        },
        {
          path: 'userProfile',
          component: UserProfileComponent, canActivate: [AuthGuard]
        },
        {
          path: 'upload',
          component: UploadAssetDataComponent, canActivate: [AuthGuard], data: {auth: {subject: Subject.ASSET, action: Actions.WRITE}}
        },
        {
          path: 'assetException',
          component: AssetExceptionInfoComponent, canActivate: [AuthGuard], data: {auth: {subject: Subject.ASSET, action: Actions.WRITE}}
        },
        {
          path: 'userManagement',
          component: UserManagementComponent, canActivate: [AuthGuard], data: {auth: { subject: Subject.USER, action: Actions.READ}}
        }
    ]
  },
  { path: 'services', component: ServicesComponent, canActivate: [AuthGuard], data: {auth: {subject: Subject.SERVICE, action: Actions.READ}}}
  /*{ path: 'userManagement', component: UserManagementComponent, canActivate: [AuthGuard], data: {roles : ['superadmin']}}*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
