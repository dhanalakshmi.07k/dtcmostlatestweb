import { Component, OnInit } from '@angular/core';
import {SearchService} from '../../services/search.service';
import {MyProfileManagementService} from '../../services/my-profile-management.service';
import {UserManagementService} from '../../services/userManagement.service';
import {UserProfile} from './user-profile-type';
import {Router} from '@angular/router';
import {FormControl, Validators, FormBuilder, FormGroup} from '@angular/forms';
import { MyErrorStateMatcher } from './my-error-state-matcher';
import { Subject,Actions} from '../../auth/rules';
declare var $ :any;

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  public userDetails: UserProfile;
  public userDetailsToEdit: UserProfile;
  showLoader: boolean;
  showChangePasswordGrid: boolean;
  showCircularLoaderInEditForm: boolean;
  isThemeChangeChecked: boolean;
  showEditModel: boolean;
  passwordHide: boolean;
  passwordChangeForm: FormGroup;
  matcher = new MyErrorStateMatcher();
  ACTIONS = Actions;
  SUBJECT = Subject;

  constructor(private searchService: SearchService, private fb: FormBuilder, private router: Router, private userManagementService: UserManagementService,
              private myProfileManagementService: MyProfileManagementService) {
    this.passwordHide = true;
    this.showEditModel = true;
  }

  ngOnInit() {
    this.searchService.showSearchBar(false);
    this.getUserDetails();
    this.checkForTheme();
  }

  getUserDetails() {
    this.showLoader = true;
    this.myProfileManagementService.getUserProfile()
      .subscribe((details: UserProfile) => {
        if (details) {
          this.showLoader = false;
          this.userDetails = details;
          this.myProfileManagementService.sendUserProfileDetails(details);
        }
      });
  }

  buildForm() {
    this.passwordChangeForm = this.fb.group({
      password: ['', [Validators.required,
        Validators.minLength(5)]],
      confirmPassword: ['']
    }, {validator: this.checkPasswords });
  }

  closeChangePasswordGrid() {
    this.showChangePasswordGrid = false;
  }

  changePassword() {
    this.showLoader = true;
    if (this.passwordChangeForm
      && this.passwordChangeForm.value
      && this.passwordChangeForm.value.password
      && this.passwordChangeForm.value.confirmPassword) {
      let obj: Object = {};
      obj['newPassword'] = this.passwordChangeForm.value.password;
      this.myProfileManagementService.setNewPassword(obj)
        .subscribe(result => {
          this.showLoader = false;
          $('#changePasswordModal').modal('hide');
          this.myProfileManagementService.passwordChanged(true);
          this.clearSession();
          this.router.navigate(['login']);
        });
    }
  }

  editProfileDetails() {
    this.showCircularLoaderInEditForm = true;
    this.myProfileManagementService.getUserProfile()
      .subscribe((details: UserProfile) => {
        if (details) {
          this.showCircularLoaderInEditForm = false;
          this.userDetailsToEdit = details;
        }
      });
  }

  updateUserDetails(userDetails) {
    this.myProfileManagementService.updateUserDetails(userDetails)
      .subscribe(res => {
        this.showMessagePopup('userUpdatedSuccessfully');
        this.getUserDetails();
        $('#user-edit-modal').modal('hide');
      }, err => {
        this.showMessagePopup('userUpdateFailed');
        $('#user-edit-modal').modal('hide');
    });
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    if (group) {
      let pass = group.controls.password.value;
      let confirmPass = group.controls.confirmPassword.value;
      return pass === confirmPass ? null : { notSame: true };
    }
  }

  private clearSession() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('userRoles');
    sessionStorage.removeItem('rules');
  }

  themeToggle(event) {
    if (event.checked) {
      sessionStorage.setItem('theme', 'dark-theme');
      this.isThemeChangeChecked = true;
      $('#body').removeClass('light-theme').addClass('dark-theme');
    } else {
      sessionStorage.setItem('theme', 'light-theme');
      this.isThemeChangeChecked = false;
      $('#body').removeClass('dark-theme').addClass('light-theme');
    }
  }

  checkForTheme() {
    let theme: string;
    theme = sessionStorage.getItem('theme');
    if (theme === 'dark-theme') {
      this.isThemeChangeChecked = true;
    } else if (theme === 'light-theme') {
      this.isThemeChangeChecked = false;
    }
  }

  showMessagePopup(popup) {
    $('.' + popup).fadeIn(300);
    setTimeout(() => {
      $('.' + popup).fadeOut(300);
    }, 3000);
  }

}
