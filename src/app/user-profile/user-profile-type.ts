/**
 * Created by chandru on 14/2/19.
 */

export interface UserProfile {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  roles: Array<string>;
  _id: string;
}
