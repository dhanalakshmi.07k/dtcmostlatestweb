import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import {FormControl, Validators, FormBuilder, FormGroup} from '@angular/forms';
import {UserManagementService} from '../../services/userManagement.service';
import { MyErrorStateMatcher } from '../user-profile/my-error-state-matcher';

@Component({
  selector: 'app-user-create-form',
  templateUrl: './user-create-form.component.html',
  styleUrls: ['./user-create-form.component.scss']
})
export class UserCreateFormComponent implements OnChanges, OnInit {
  userCreationForm: FormGroup;
  public hide: boolean;
  public isUsernameExist: boolean;
  usernameExistCheck = new MyErrorStateMatcher();
  @Input() roles: Array<string>;
  @Input() isBuildUserFormInitiated: boolean;
  @Output() userDetailsEvent: EventEmitter<object> = new EventEmitter();

  constructor(private fb: FormBuilder, private userManagementService: UserManagementService) {
    this.hide = true;
    this.isUsernameExist = false;
  }

  ngOnInit() {
    this.buildForm();
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName of Object.keys(changes)) {
      let change = changes[propName];
      if (propName === 'isBuildUserFormInitiated') {
        if (change.currentValue !== change.previousValue) {
          this.buildForm();
        }
      }
    }
  }

  buildForm() {
    this.userCreationForm = this.fb.group({
      usernameGroup: this.fb.group({
        username: ['', [Validators.required, Validators.minLength(5)]],
        isUsernameExist: ['', []],
    }, {validator: this.checkUsername }),
      password: ['', [Validators.required, Validators.minLength(5)]],
      firstName: ['', [Validators.required]],
      lastName: [''],
      email: ['', [Validators.required, Validators.email]],
      roles: ['', [Validators.required]],
    });
    this.setIsUsernameExistDefaultValue();
  }

  setIsUsernameExistDefaultValue() {
    this.userCreationForm.controls['usernameGroup']['controls']['isUsernameExist'].setValue(false);
  }

  emitUserDetails() {
    let rolesArray: any = [];
    let usernameValue: string;
    usernameValue = this.userCreationForm.value.usernameGroup.username;
    rolesArray.push(this.userCreationForm.value.roles);
    this.userCreationForm.value.roles = rolesArray;
    this.userCreationForm.value['username'] = usernameValue;
    this.userDetailsEvent.emit(this.userCreationForm.value);
  }

  onUserNameChange(userName) {
    let strLength: number;
    strLength = userName.length;
    if ( strLength > 4 ) {
      this.userManagementService.isUserNameExist(userName)
        .then((isUserNameAlereadyUsed: boolean) => {
          this.userCreationForm.controls['usernameGroup']['controls']['isUsernameExist'].setValue(isUserNameAlereadyUsed);
        });
    } else {
      this.setIsUsernameExistDefaultValue();
    }
  }

  checkUsername(group: FormGroup) { // here we have the 'usernameGroup' group
    let isUsernameExist = group.controls.isUsernameExist.value;
    return isUsernameExist === true ? { usernameExist: true } : null;
  }

}
