/**
 * Created by suhas on 14/2/19.
 */

export enum Subject {
  USER = 'User' ,
  ASSET = 'Asset' ,
  INSPECTION  = 'Inspection',
  SERVICE = 'Service',
  SETTINGS = 'Settings',
  READ_ROLE = 'read_role',
  WRITE_ROLE = 'write_role',
  DELETE_ROLE = 'delete_role',
  UPDATE_ROLE = 'update_role'
}
export enum Actions {
  READ = 'read',
  WRITE = 'write',
  UPDATE = 'update',
  DELETE = 'delete',
  MANAGE = 'manage',
  ANALYTICS = 'analytics',
  DOWNLOAD = 'download',
  LIVE = 'live'
}
/*export const SUBJECT = {
  USER:"User",
  ASSET:"Asset",
  INSPECTION:"Inspection",
  SERVICE:"Service",
  SETTINGS:"Settings",
};
export const ACTIONS = {
  READ:"read",
  WRITE:"write",
  UPDATE:"update",
  DELETE:"delete",
  MANAGE:"manage",
  ANALYTICS:"analytics",
  DOWNLOAD:"download",
  LIVE:"live",
};*/
