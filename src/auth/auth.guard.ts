import {Injectable, OnInit} from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import {Router} from "@angular/router";
import { LoginServiceService } from '../services/login-service.service';
import {ability} from './ability';
@Injectable()
export class AuthGuard implements CanActivate,OnInit {
  ngOnInit(): void {
  }

  constructor(private router: Router, private  loginServiceService: LoginServiceService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    if (sessionStorage.getItem('token') != null) {
      let auth = next.data['auth'];
      if(auth){
        let canRoute=ability.can(auth.action,auth.subject);
        if (canRoute) {
          return true;
        } else {
          this.router.navigate(['forbidden']);
          return false;
        }
      }else{
        return true;
      }
    }
    this.router.navigate(['login']);
    return false;
  }
}
